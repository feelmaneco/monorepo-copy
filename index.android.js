/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './packages/mobile/App';
import { name as appName } from './packages/mobile/app.json';

AppRegistry.registerComponent(appName, () => App);
console.log('loaded');
