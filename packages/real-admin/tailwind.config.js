module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      height: () => ({
        '10%': '10%',
        '20%': '20%',
        '30%': '30%',
        '40%': '40%',
        '50%': '50%',
        '60%': '60%',
        '70%': '70%',
        '80%': '80%',
        '90%': '90%',
        '100%': '100%',
        'screen-2': '50vh',
        'screen-3': 'calc(100vh / 3)',
        'screen-4': 'calc(100vh / 4)',
        'screen-5': 'calc(100vh / 5)',
        'screen-2/3': 'calc(100vh / 100 *75)',
      }),
      width: () => ({
        '10%': '10%',
        '20%': '20%',
        '25%': '25%',
        '30%': '30%',
        '40%': '40%',
        '50%': '50%',
        '60%': '60%',
        '70%': '70%',
        '80%': '80%',
        '90%': '90%',
        '100%': '100%',
      }),
    },
    animation: {
      'fade-from-right': `
        @keyframes fade-from-right {
          from {right: 0;}
          to {right: 200;}
        }
        animation-name: example;
        animation-duration: 4s;
        `,
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
