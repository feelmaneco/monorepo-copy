import { getAuth, signOut as signOutFirebase } from 'firebase/auth';

export const signOut = async (): Promise<boolean> => {
  try {
    const auth = getAuth();
    await signOutFirebase(auth);
    return true;
  } catch (e) {
    console.log('Errror login', e);
    return false;
  }
};
