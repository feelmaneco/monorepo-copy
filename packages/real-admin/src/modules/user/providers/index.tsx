import React from 'react';
import { RealStateProvider } from './RealState';

export const Providers: React.FC = ({ children }) => <RealStateProvider>{children}</RealStateProvider>;
