import React, { createContext, useState } from 'react';
import { CITY, REAL_STATE_GOVEMENT_STATUS } from '@dindom/common/build/interfaces/realState/realState';
import { FindRealStateInfo } from '@dindom/common/build/interfaces/realState/FindRealStateInfo.interface';
import { findRealStateInfo } from '../../../../services/firebase/findRealStateInfo';
import { updateRealStateInfoApi } from '../../../../services/firebase/updateRealStateInfo';
import { IRealStateContext } from './types';
export const RealStateContext = createContext<IRealStateContext>({
  isLoaded: false,
  isLoading: false,
  realStateInfo: {
    address: {
      address: '',
      city: CITY.BOGOTA,
    },
    administrator: {
      name: '',
      photoUrl: '',
    },
    aparmentsNumber: 0,
    email: '',
    name: '',
    phoneNumber: '',
    photoUrl: '',
    status: REAL_STATE_GOVEMENT_STATUS.four,
    uid: '',
  },
  errorMessage: '',
  fetchFindRealStateInfoApi: () => null,
  updateRealStateInfo: async () => false,
});

export const RealStateProvider: React.FC = ({ children }) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>('');
  const [realStateInfo, setRealStateInfo] = useState<FindRealStateInfo>({
    address: {
      address: '',
      city: CITY.BOGOTA,
    },
    administrator: {
      name: '',
      photoUrl: '',
    },
    aparmentsNumber: 0,
    email: '',
    name: '',
    phoneNumber: '',
    photoUrl: '',
    status: REAL_STATE_GOVEMENT_STATUS.four,
    uid: '',
  });

  const fetchFindRealStateInfoApi = async () => {
    setIsLoading(true);
    const response = await findRealStateInfo();

    if (response.error === true) {
      setErrorMessage(response.message);
    } else {
      setRealStateInfo(response.data);
    }
    setIsLoaded(true);
    setIsLoading(false);
  };

  const updateRealStateInfo = async (data: FindRealStateInfo): Promise<boolean> => {
    setIsLoading(true);
    const resposne = await updateRealStateInfoApi(data);
    if (resposne.error === true) {
      setIsLoading(false);
      setErrorMessage(resposne.message);
      return false;
    } else {
      setRealStateInfo(resposne.data);
      setIsLoading(false);
      return true;
    }
  };

  return (
    <RealStateContext.Provider
      value={{
        isLoading,
        realStateInfo,
        errorMessage,
        fetchFindRealStateInfoApi,
        isLoaded,
        updateRealStateInfo,
      }}
    >
      {children}
    </RealStateContext.Provider>
  );
};
