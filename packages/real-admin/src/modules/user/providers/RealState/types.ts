import { FindRealStateInfo } from '@dindom/common/build/interfaces/realState/FindRealStateInfo.interface';

export interface IRealStateContext {
  realStateInfo: FindRealStateInfo;
  isLoading: boolean;
  errorMessage: string;
  fetchFindRealStateInfoApi: () => void;
  isLoaded: boolean;
  updateRealStateInfo: (data: FindRealStateInfo) => Promise<boolean>;
}
