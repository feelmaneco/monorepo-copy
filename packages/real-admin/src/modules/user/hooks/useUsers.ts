import { useEffect, useState } from 'react';
import { RealSatateOwner } from '../../../interfaces/RealStateOwner.interface';
import { getRealStateOwners } from '../../../services/firebase/user/getRealStateOwners';

export function useUsers(realStateId: string) {
  const [users, setUsers] = useState<RealSatateOwner[]>([]);
  useEffect(() => {
    if (realStateId) {
      const foo = async () => {
        const resposne = await getRealStateOwners(realStateId);
        setUsers(resposne);
      };
      foo();
    }
  }, [realStateId]);
  return { users };
}
