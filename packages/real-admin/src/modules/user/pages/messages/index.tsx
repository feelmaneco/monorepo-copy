import React from 'react';
import { MessagesContainer } from '../../containers/MessagesContainer';

export const UserRealStateMessagesPage: React.FC = () => {
  return (
    <>
      <MessagesContainer />
    </>
  );
};
