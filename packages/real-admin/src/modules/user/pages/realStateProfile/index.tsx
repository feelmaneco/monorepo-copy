import React from 'react';
import { RealStateProfileContainer } from '../../containers/RealStateProfile';

export const UserRealStateProfilePage: React.FC = () => {
  return (
    <>
      <RealStateProfileContainer />
    </>
  );
};
