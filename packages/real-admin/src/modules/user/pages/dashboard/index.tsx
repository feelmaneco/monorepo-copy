import React from 'react';
import { DashBoardContainer } from '../../containers/Dashboard';

export const UserDashBoardPage: React.FC = () => {
  return (
    <>
      <DashBoardContainer />
    </>
  );
};
