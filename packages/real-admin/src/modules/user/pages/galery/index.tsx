import React from 'react';
import { GaleryContainer } from '../../containers/Galery';

export const UserRealStateGaleryPage: React.FC = () => {
  return (
    <>
      <GaleryContainer />
    </>
  );
};
