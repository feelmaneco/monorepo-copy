import React from 'react';
import { UsersContainer } from '../../containers/Users';

export const UserUsersPage: React.FC = () => {
  return (
    <>
      <UsersContainer />
    </>
  );
};
