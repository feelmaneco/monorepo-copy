import React from 'react';
import { UserSettingsContainer } from '../../containers/UserSettings';

// import { FindRealStateInfo } from "../../../../interfaces/FindRealStateInfo.interface";

export const UserRealStateUserSettingsPage: React.FC = () => {
  return (
    <div className='container'>
      <UserSettingsContainer />
    </div>
  );
};
