import React from 'react';
import { DocumentContainer } from '../../containers/DocumentsContainer';

export const UserRealStateDocumentsPage: React.FC = () => {
  return (
    <>
      <DocumentContainer />
    </>
  );
};
