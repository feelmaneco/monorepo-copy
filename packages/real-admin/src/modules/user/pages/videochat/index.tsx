import React from 'react';
import { VideoChatContainer } from '../../containers/VideoChat/VideoChatContainer';

export const UserVideoChatPage: React.FC = () => {
  return (
    <div>
      <VideoChatContainer />
    </div>
  );
};
