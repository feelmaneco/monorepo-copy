import React from 'react';
import { ViewPostContainer } from '../../containers/ViewPostContainer';

export const UserRealStateViewPostPage: React.FC = () => {
  return (
    <>
      <ViewPostContainer />
    </>
  );
};
