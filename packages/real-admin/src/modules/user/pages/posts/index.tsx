import React from 'react';
import { PostsContainer } from '../../containers/PostsContainer';

export const UserRealStatePostsPage: React.FC = () => {
  return (
    <>
      <PostsContainer />
    </>
  );
};
