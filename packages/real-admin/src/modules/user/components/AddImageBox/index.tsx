import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type AddImageBoxProps = {
  onClick?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  title: string;
  type?: 'pdf';
};
export const AddImageBox: React.FC<AddImageBoxProps> = ({ onClick, title, type }) => {
  return (
    <label>
      <div
        className='
    m-2
    flex
    h-56
    w-56
    cursor-pointer
    flex-col
    items-center
    justify-center
    rounded-2xl
    border-2
    border-dotted
    border-gray-400
    bg-gray-200
    text-gray-400
    shadow-md
    hover:border-gray-700
    hover:bg-gray-300
    hover:text-gray-500
    '
      >
        <FontAwesomeIcon icon={{ iconName: 'plus-circle', prefix: 'fas' }} size='3x' />
        <span className='mt-2'>{title}</span>
      </div>
      <input type='file' onChange={onClick} accept={type ? `application/pdf` : `image/*`} name='photo' className='hidden' />
    </label>
  );
};
