import React from 'react';
import { RealSatateOwner } from '../../../../interfaces/RealStateOwner.interface';
import { Microphone } from '../ControlButtons/MicrophoneUserList';
import { Video } from '../ControlButtons/VideoUserList';

type UsersListItemProps = {
  user: RealSatateOwner;
  microphoneClickHandler: (id: string) => void;
  videoClickHanlder: (id: string) => void;
};
export const UsersListItem: React.FC<UsersListItemProps> = ({ user, videoClickHanlder, microphoneClickHandler }) => {
  return (
    <div className='flex h-10% whitespace-nowrap  bg-white px-2 pb-1 pt-1 '>
      <div className='flex h-100% w-100% whitespace-nowrap rounded-xl px-2 hover:bg-green-500 '>
        <div className='align-center flex w-70% whitespace-nowrap rounded-l-lg '>
          <div className='flex items-center'>
            <div className='h-10 w-10 flex-shrink-0'>
              <img
                className='h-10 w-10 rounded-xl'
                src={
                  'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1374&q=80'
                }
                alt=''
              />
            </div>
            <div className='ml-2 '>
              <div className='text-sm font-medium text-gray-800'>
                {user.name} {user.lastname}
              </div>
              <div className='text-sm text-gray-600'>
                Torre: {user.entrance} Apt: {user.apartment}
              </div>
            </div>
          </div>
        </div>
        <div className='flex w-30%'>
          <div className='flex w-50%  flex-shrink-0 items-center justify-center'>
            <div className='h-7 w-7 '>
              <Microphone
                onClick={() => {
                  microphoneClickHandler(user.id);
                }}
                isActive={user.controls.microphoneActive}
              />
            </div>
          </div>
          <div className='flex w-50%  flex-shrink-0 items-center justify-center'>
            <div className='h-7 w-7 '>
              <Video
                onClick={() => {
                  videoClickHanlder(user.id);
                }}
                isActive={user.controls.videoActive}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
