import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useFormik } from 'formik';
import React from 'react';
import { BeatLoader } from 'react-spinners';
import { RealSatateOwner } from '../../../../interfaces/RealStateOwner.interface';
import Yup from '../../../../utils/formValidation';

type AddNewUserProps = {
  onCreateUser: (newUser: RealSatateOwner) => Promise<void>;
  isLoading: boolean;
};

export const AddNewUser: React.FC<AddNewUserProps> = ({ onCreateUser, isLoading }) => {
  const newUserSchema = Yup.object().shape({
    name: Yup.string().required(),
    lastname: Yup.string().required(),
    email: Yup.string().max(50).min(5).required().email(),
    entrance: Yup.string().required(),
    apartment: Yup.string().required(),
    status: Yup.string().required(),
  });

  const formik = useFormik<RealSatateOwner>({
    initialValues: {
      id: '',
      apartment: '',
      email: '',
      entrance: '',
      lastname: '',
      name: '',
      status: '',
      controls: {
        microphoneActive: false,
        videoActive: false,
      },
    },
    validationSchema: newUserSchema,
    onSubmit: async (value, { resetForm }) => {
      await onCreateUser(value);
      resetForm();
    },
  });

  const { handleSubmit, values, handleChange, errors } = formik;
  return (
    <>
      <div>
        <div className='hidden sm:block' aria-hidden='true'>
          <div className='py-5'>
            <div className='border-t border-gray-300' />
          </div>
        </div>

        <div className='mt-10 sm:mt-0'>
          <div className='md:grid md:grid-cols-3 md:gap-6'>
            <div className='md:col-span-1'>
              <div className='px-4 sm:px-0'>
                <h3 className='text-lg font-medium leading-6 text-gray-900'>Información Propietario</h3>
                <p className='mt-3 text-sm text-gray-600'>Mantén actualizados los datos de contacto para futuras notificaciones.</p>
              </div>
            </div>
            <div className='mt-5 md:col-span-2 md:mt-0'>
              <form onSubmit={handleSubmit}>
                <div className='overflow-hidden shadow sm:rounded-md'>
                  <div className='bg-white px-4 py-5 sm:p-6'>
                    <div className='grid grid-cols-6 gap-6'>
                      <div className='col-span-6 sm:col-span-3'>
                        <label htmlFor='name' className='block text-sm font-medium text-gray-700'>
                          Nombre
                        </label>
                        <input
                          type='text'
                          id='name'
                          value={values.name}
                          onChange={handleChange}
                          name='name'
                          placeholder='Nombre propietario'
                          // autoComplete="given-name"
                          className='mt-1 block h-9 w-full rounded-md  border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500  focus:outline-none  sm:text-sm'
                        />
                        {errors.name && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.name}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 sm:col-span-3'>
                        <label htmlFor='lastname' className='block text-sm font-medium text-gray-700'>
                          Apellido
                        </label>
                        <input
                          type='text'
                          id='lastname'
                          onChange={handleChange}
                          value={values.lastname}
                          name='lastname'
                          placeholder='Apellido propietario'
                          // autoComplete="family-name"
                          className='mt-1 block h-9 w-full rounded-md border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                        {errors.lastname && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.lastname}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 sm:col-span-4'>
                        <label htmlFor='email' className='block text-sm font-medium text-gray-700'>
                          Email
                        </label>
                        <input
                          type='text'
                          id='email'
                          onChange={handleChange}
                          value={values.email}
                          name='email'
                          placeholder='propietario@email.com'
                          // autoComplete="email"
                          className='mt-1 block h-9 w-full rounded-md border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                        {errors.email && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.email}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 mt-2'>
                        <span className='block text-sm font-bold  text-gray-700'>Propiedad</span>
                      </div>

                      <div className='col-span-6 sm:col-span-6 lg:col-span-2'>
                        <label htmlFor='entrance' className='block text-sm font-medium text-gray-700'>
                          Bloque
                        </label>
                        <input
                          type='string'
                          id='entrance'
                          onChange={handleChange}
                          value={values.entrance}
                          name='entrance'
                          placeholder='Numero de Bloque'
                          // autoComplete="address-level2"
                          className='block h-9 w-full rounded-md  border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                        {errors.entrance && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.entrance}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 sm:col-span-3 lg:col-span-2'>
                        <label htmlFor='apartment' className='block text-sm font-medium text-gray-700'>
                          Apartamento
                        </label>
                        <input
                          type='string'
                          id='apartment'
                          onChange={handleChange}
                          value={values.apartment}
                          name='apartment'
                          placeholder='Numero de Apartamento'
                          // autoComplete="address-level1"
                          className='block h-9 w-full rounded-md border-2 border-gray-200  pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                        {errors.apartment && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.apartment}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 sm:col-span-3 '>
                        <label htmlFor='status' className='block text-sm font-medium text-gray-700'>
                          Estado
                        </label>
                        <select
                          id='status'
                          name='status'
                          onChange={handleChange}
                          value={values.status}
                          className='mt-1 block w-full rounded-md border-2 border-gray-200 bg-white py-2 px-3 shadow-sm focus:border-green-500 focus:outline-none  sm:text-sm'
                        >
                          <option></option>
                          <option>Activo</option>
                          <option>Inactivo</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className='bg-gray-100 px-4 py-3 text-right sm:px-6'>
                    <button
                      type='submit'
                      className='inline-flex justify-center rounded-md border border-transparent bg-green-500 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2'
                    >
                      {isLoading ? <BeatLoader /> : 'Guardar'}
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div className='hidden sm:block' aria-hidden='true'>
          <div className='py-5'>
            <div className='border-t border-gray-200' />
          </div>
        </div>
      </div>
    </>
  );
};
