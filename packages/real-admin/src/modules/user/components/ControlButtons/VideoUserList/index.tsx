import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type VideoProps = {
  isActive: boolean;
  onClick: () => void;
};

export const Video: React.FC<VideoProps> = ({ isActive, onClick }) => {
  return (
    <button
      className={`mr-4
      h-7
      w-7
      rounded-full
      shadow-lg
      bg-${isActive ? 'white' : 'red-400'}
      `}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`text-${isActive ? 'green-500' : 'white'}`}
        size='xs'
        icon={{
          iconName: isActive ? 'video' : 'video-slash',
          prefix: 'fas',
        }}
      />
    </button>
  );
};
