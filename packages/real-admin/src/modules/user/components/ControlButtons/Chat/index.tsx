import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type ChatProps = {
  onClick: () => void;
};

export const Chat: React.FC<ChatProps> = ({ onClick }) => {
  return (
    <button
      className={`h-16
      w-16
      rounded-lg
      bg-white
      shadow-lg
      `}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`text-green-500`}
        size='2x'
        icon={{
          iconName: 'comment-alt',
          prefix: 'fas',
        }}
      />
    </button>
  );
};
