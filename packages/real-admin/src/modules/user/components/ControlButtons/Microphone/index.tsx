import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type MicrophoneProps = {
  isActive: boolean;
  onClick: () => void;
};

export const Microphone: React.FC<MicrophoneProps> = ({ isActive, onClick }) => {
  return (
    <button
      className={`mr-4
      h-16
      w-16
      rounded-lg
      shadow-lg
      bg-${isActive ? 'white' : 'red-400'}
      `}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`text-${isActive ? 'green-500' : 'white'}`}
        size='2x'
        icon={{
          iconName: isActive ? 'microphone' : 'microphone-slash',
          prefix: 'fas',
        }}
      />
    </button>
  );
};
