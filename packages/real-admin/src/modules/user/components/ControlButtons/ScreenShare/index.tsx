import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type ScreenShareProps = {
  onClick: () => void;
};

export const ScreenShare: React.FC<ScreenShareProps> = ({ onClick }) => {
  return (
    <button
      className={`h-16
      w-16
      rounded-lg
      bg-white
      shadow-lg
      `}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`text-green-500`}
        size='2x'
        icon={{
          iconName: 'desktop',
          prefix: 'fas',
        }}
      />
    </button>
  );
};
