import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type QuickQuestionProps = {
  onClick: () => void;
};

export const QuickQuestion: React.FC<QuickQuestionProps> = ({ onClick }) => {
  return (
    <button
      className={`h-16
      w-16
      rounded-lg
      bg-white
      shadow-lg
      `}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`text-green-500`}
        size='2x'
        icon={{
          iconName: 'list-alt',
          prefix: 'fas',
        }}
      />
    </button>
  );
};
