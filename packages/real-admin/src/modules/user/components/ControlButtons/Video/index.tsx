import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type VideoProps = {
  isActive: boolean;
  onClick: () => void;
};

export const Video: React.FC<VideoProps> = ({ isActive, onClick }) => {
  return (
    <button
      className={`mr-4
      h-16
      w-16
      rounded-lg
      shadow-lg
      bg-${isActive ? 'white' : 'red-400'}
      `}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`text-${isActive ? 'green-500' : 'white'}`}
        size='2x'
        icon={{
          iconName: isActive ? 'video' : 'video-slash',
          prefix: 'fas',
        }}
      />
    </button>
  );
};
