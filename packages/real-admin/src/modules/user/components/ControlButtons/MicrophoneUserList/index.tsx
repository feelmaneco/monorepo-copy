import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type MicrophoneProps = {
  isActive: boolean;
  onClick: () => void;
};

export const Microphone: React.FC<MicrophoneProps> = ({ isActive, onClick }) => {
  return (
    <button
      className={`mr-4
      h-7
      w-7
      rounded-full
      shadow-lg
      bg-${isActive ? 'white' : 'red-400'}
      `}
      onClick={onClick}
    >
      <FontAwesomeIcon
        className={`text-${isActive ? 'green-500' : 'white'}`}
        size='xs'
        icon={{
          iconName: isActive ? 'microphone' : 'microphone-slash',
          prefix: 'fas',
        }}
      />
    </button>
  );
};
