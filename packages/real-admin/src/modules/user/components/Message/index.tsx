import React from 'react';
import { IMessageEntity } from '../../../../interfaces/Message.interface';
import moment from 'moment';

type PostProps = {
  message: IMessageEntity;
};
export const Message: React.FC<PostProps> = ({ message }) => {
  //const [showPost, setShowPost] = useState(false);
  //const viewPostHandler = () => {setShowPost(!showPost);};
  return (
    <>
      <li className='mt-4 mb-4 w-fit overflow-hidden rounded-lg bg-white shadow-lg '>
        <div className='flex space-y-4'>
          <div className='flex w-full flex-col p-6'>
            {message.text && (
              <div className='flex justify-between'>
                <h3 className={`mb-4 text-gray-800 sm:text-2xl lg:text-3xl`}>{message.text}</h3>
              </div>
            )}
            <div className='mt-auto flex w-full text-lg font-medium leading-6'>
              <h3>
                {message.from} | {message.realStateOwnerId}
              </h3>
              <p className='ml-auto text-indigo-600'>{message.date && moment(message.date.toDate()).format('MM/DD/YYYY')}</p>
            </div>
          </div>
        </div>
      </li>
    </>
  );
};
