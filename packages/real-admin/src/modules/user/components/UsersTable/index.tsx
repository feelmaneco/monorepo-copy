import React from 'react';
import { RealSatateOwner } from '../../../../interfaces/RealStateOwner.interface';

type UsersTableProps = {
  users: RealSatateOwner[];
};

export const UsersTable: React.FC<UsersTableProps> = ({ users }) => {
  return (
    <table className='min-w-full leading-normal'>
      <thead>
        <tr>
          <th className='border-b-2 border-gray-200 bg-gray-100 px-5 py-3 text-left text-xs font-semibold uppercase tracking-wider text-gray-600'>
            Name
          </th>
          <th className='border-b-2 border-gray-200 bg-gray-100 px-5 py-3 text-left text-xs font-semibold uppercase tracking-wider text-gray-600'>
            Lastname
          </th>
          <th className='border-b-2 border-gray-200 bg-gray-100 px-5 py-3 text-left text-xs font-semibold uppercase tracking-wider text-gray-600'>
            Email
          </th>
          <th className='border-b-2 border-gray-200 bg-gray-100 px-5 py-3 text-left text-xs font-semibold uppercase tracking-wider text-gray-600'>
            Apartment
          </th>
          <th className='border-b-2 border-gray-200 bg-gray-100 px-5 py-3 text-left text-xs font-semibold uppercase tracking-wider text-gray-600'>
            Status
          </th>
        </tr>
      </thead>
      <tbody>
        {users.map(user => (
          <tr key={user.email}>
            <td className='border-b border-gray-200 bg-white px-5 py-5 text-sm'>
              <div className='flex items-center'>
                <div className='ml-3'>
                  <p className='whitespace-no-wrap text-gray-900'>{user.name}</p>
                </div>
              </div>
            </td>
            <td className='border-b border-gray-200 bg-white px-5 py-5 text-sm'>
              <p className='whitespace-no-wrap text-gray-900'>{user.lastname}</p>
            </td>
            <td className='border-b border-gray-200 bg-white px-5 py-5 text-sm'>
              <p className='whitespace-no-wrap text-gray-900'>{user.email}</p>
            </td>
            <td className='border-b border-gray-200 bg-white px-5 py-5 text-sm'>
              <p className='whitespace-no-wrap text-gray-900'>
                {user.entrance}-{user.apartment}
              </p>
            </td>
            <td className='border-b border-gray-200 bg-white px-5 py-5 text-sm'>
              <span
                className={`relative inline-block px-3 py-1 font-semibold text-${
                  user.status === 'Activo' ? 'green' : 'red'
                }-900 leading-tight`}
              >
                <span
                  aria-hidden
                  className={`absolute inset-0 rounded-full opacity-50 ${user.status === 'Activo' ? 'bg-green-200' : 'bg-red-200'}`}
                ></span>
                <span className='relative'>{user.status}</span>
              </span>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
