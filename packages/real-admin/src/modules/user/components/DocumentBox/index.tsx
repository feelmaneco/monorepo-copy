import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { IRealStateDocumentEntity } from '../../../../interfaces/Document.interface';

type DocumentBoxProps = {
  onClick: (document: IRealStateDocumentEntity) => void;
  document: IRealStateDocumentEntity;
  type?: 'pdf';
};

export const DocumentBox: React.FC<DocumentBoxProps> = ({ document, onClick }) => {
  return (
    <div
      onClick={() => {
        onClick(document);
      }}
      className='
            m-2
            mx-3
            flex
            h-56
            w-56
            cursor-pointer
            flex-col
            items-center
            justify-center
            rounded-2xl
            border-2
            border-dotted
            border-gray-400
            bg-white
            text-gray-400
            shadow-md
            hover:border-gray-700
            hover:bg-gray-50
            hover:text-gray-500
            '
    >
      <FontAwesomeIcon icon={{ iconName: 'file-alt', prefix: 'fas' }} size='3x' className='text-black' />
      <span className='mt-2 text-sm text-black'>{document.name}</span>
    </div>
  );
};
