import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useFormik } from 'formik';
import React, { useContext } from 'react';
import { FindRealStateInfo } from '@dindom/common/build/interfaces/realState/FindRealStateInfo.interface';
import Yup from '../../../../utils/formValidation';
import { RealStateContext } from '../../providers/RealState';

type ProfileInfoFormProps = {
  isLoading: boolean;
  initialValues: FindRealStateInfo;
  onSubmit: (data: FindRealStateInfo) => void;
};

export const ProfileInfoForm: React.FC<ProfileInfoFormProps> = ({ isLoading, initialValues, onSubmit }) => {
  const { realStateInfo } = useContext(RealStateContext);
  //const [selectedImage, setSelectedImage] = useState(undefined);
  const handleUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    //setSelectedImage(e.target.files[0])
    console.log(e.target.files);
  };
  const ProfileInfoSchema = Yup.object().shape({
    email: Yup.string().max(50).min(5).required().email(),
    name: Yup.string().required(),
    address: Yup.object().shape({
      address: Yup.string().min(5).max(511).required(),
    }),
    phoneNumber: Yup.string().required().min(8).max(20),
    aparmentsNumber: Yup.number().min(1).max(999).required(),
  });

  const formik = useFormik<FindRealStateInfo>({
    initialValues,
    enableReinitialize: true,
    onSubmit,
    validationSchema: ProfileInfoSchema,
  });
  const { handleSubmit, handleChange, values, errors } = formik;
  //const loading = useSelector(realStateLoadingStatus);

  return (
    <>
      <div>
        <div className='hidden sm:block' aria-hidden='true'>
          <div className='py-5'>
            <div className='border-t border-gray-300' />
          </div>
        </div>

        <div className='mt-10 sm:mt-0'>
          <div className='md:grid md:grid-cols-3 md:gap-6'>
            <div className='md:col-span-1'>
              <div className='px-4 sm:px-0'>
                <h3 className='text-lg font-medium leading-6 text-gray-900'>Información de Conjunto</h3>
                <p className='mt-3 text-sm text-gray-600'>Mantén actualizados los datos del conjunto para futuras actualizaciones.</p>
              </div>
            </div>

            <div className='mt-5 md:col-span-2 md:mt-0'>
              <form onSubmit={handleSubmit}>
                <div className='overflow-hidden shadow sm:rounded-md'>
                  <div className='bg-white px-4 py-5 sm:p-6'>
                    <div className=''>
                      <input onChange={handleUpload} className='hidden' name='file-upload' id='file-upload' accept='image/*' type='file' />
                      <div className='flex flex-col justify-center px-8 pb-10 align-middle'>
                        <label htmlFor='cover-photo' className='mb-4 block text-sm font-medium text-gray-700 '>
                          Foto de portada
                        </label>
                        <label
                          style={{
                            backgroundImage: `url(${realStateInfo.photoUrl})`,
                            backgroundRepeat: 'no-repeat',
                            backgroundSize: '100%',
                          }}
                          htmlFor='file-upload'
                          className='flex h-60 max-w-full cursor-pointer justify-end rounded-lg px-6 pt-5 pb-6 text-center'
                        >
                          <div className='h-8 w-8 rounded-lg bg-transparent bg-gray-700 bg-opacity-50 p-1 text-center'>
                            <svg
                              className='mx-auto h-6 w-6 text-white'
                              stroke='currentColor'
                              fill='none'
                              viewBox='0 0 48 48'
                              aria-hidden='true'
                            >
                              <path
                                d='M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02'
                                strokeWidth={2}
                                strokeLinecap='round'
                                strokeLinejoin='round'
                              />
                            </svg>
                          </div>
                        </label>
                      </div>
                    </div>
                    <div className='grid grid-cols-6 gap-6'>
                      <div className='col-span-6 sm:col-span-3'>
                        <label htmlFor='first-name' className='block text-sm font-medium text-gray-700'>
                          Correo
                        </label>
                        <input
                          placeholder={values.email}
                          onChange={handleChange}
                          value={values.email}
                          type='text'
                          name='email'
                          id='email'
                          className='mt-1 block h-9 w-full rounded-md  border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500  focus:outline-none  sm:text-sm'
                        />
                        {errors.email && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.email}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 sm:col-span-3'>
                        <label htmlFor='last-name' className='block text-sm font-medium text-gray-700'>
                          Nombre del conjunto
                        </label>
                        <input
                          type='text'
                          name='name'
                          id='email'
                          disabled
                          placeholder={values.name}
                          onChange={handleChange}
                          value={values.name}
                          className='mt-1 block h-9 w-full rounded-md border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                      </div>

                      <div className='col-span-6 sm:col-span-6'>
                        <label htmlFor='street-address' className='block text-sm font-medium text-gray-700'>
                          Dirección
                        </label>
                        <input
                          type='text'
                          name='address.address'
                          id='email'
                          placeholder={values.address.address}
                          onChange={handleChange}
                          value={values.address.address}
                          className='mt-1 block h-9 w-full rounded-md border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                        {errors.address?.address && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.address.address}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 sm:col-span-3'>
                        <label htmlFor='region' className='block text-sm font-medium text-gray-700'>
                          Numero telefónico
                        </label>
                        <input
                          type='text'
                          name='phoneNumber'
                          id='email'
                          placeholder={values.phoneNumber}
                          onChange={handleChange}
                          value={values.phoneNumber}
                          className='mt-1 block h-9 w-full rounded-md border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                        {errors.phoneNumber && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.phoneNumber}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>

                      <div className='col-span-6 sm:col-span-6 lg:col-span-3'>
                        <label htmlFor='postal-code' className='block text-sm font-medium text-gray-700'>
                          Numero de apartamentos
                        </label>
                        <input
                          type='number'
                          name='aparmentsNumber'
                          id='email'
                          placeholder={values.aparmentsNumber.toString()}
                          onChange={handleChange}
                          value={values.aparmentsNumber}
                          className='mt-1 block h-9 w-full rounded-md  border-2 border-gray-200 pl-3 shadow-sm focus:border-green-500 focus:outline-none sm:text-sm'
                        />
                        {errors.aparmentsNumber && (
                          <div className='m-2 flex items-center rounded bg-red-400 p-2 text-white'>
                            <span>{errors.aparmentsNumber}</span>
                            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className='bg-gray-50 px-4 py-3 text-center sm:px-6'>
                    {isLoading ? (
                      <button
                        type='submit'
                        disabled
                        className='inline-flex justify-center rounded-md border border-transparent bg-gray-600 py-2 px-4 text-sm font-medium text-white shadow-sm focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2'
                      >
                        Actualizando
                      </button>
                    ) : (
                      <button
                        type='submit'
                        className='inline-flex justify-center rounded-md border border-transparent bg-green-500 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2'
                      >
                        Actualizar
                      </button>
                    )}
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className='hidden sm:block' aria-hidden='true'>
          <div className='py-5'>
            <div className='border-t border-gray-200' />
          </div>
        </div>
      </div>
    </>
  );
};
