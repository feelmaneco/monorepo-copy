import React from 'react';

type ImageBoxProps = {
  onClick?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  image: {
    src: string;
  };
};

export const ImageBox: React.FC<ImageBoxProps> = ({ image }) => {
  return (
    <div
      className='
        m-2
        flex
        h-56
        w-56
        cursor-pointer
        flex-col
        items-center
        justify-center
        rounded-2xl
        border-2
        border-dotted
        border-gray-400
        shadow-md
        hover:border-gray-700
        '
    >
      <img
        src={image.src}
        className='h-56
                w-56
                rounded-2xl'
        alt={image.src}
      />
    </div>
  );
};
