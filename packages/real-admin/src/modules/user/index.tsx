import React, { useContext, useEffect } from 'react';
import { RealStateContext } from './providers/RealState';
import { Routes } from './routes';

export const UserModule: React.FC = () => {
  const { isLoaded, fetchFindRealStateInfoApi } = useContext(RealStateContext);
  useEffect(() => {
    if (!isLoaded) {
      fetchFindRealStateInfoApi();
    }
  }, [isLoaded, fetchFindRealStateInfoApi]);
  return <Routes />;
};
