import { IconProp } from '@fortawesome/fontawesome-svg-core';

export type NavigationItem = {
  name: string;
  path: string;
  icon: IconProp;
  current: boolean;
};

export const navigation: NavigationItem[] = [
  // {
  //   name: "Dashboard",
  //   path: "/dashboard",
  //   icon: { iconName: "home", prefix: "fas" },
  //   current: true,
  // },
  {
    name: 'Perfil del conjunto',
    path: '/realstateinfo',
    icon: { iconName: 'house-user', prefix: 'fas' },
    current: true,
  },
  {
    name: 'Galeria',
    path: '/galery',
    icon: { iconName: 'images', prefix: 'fas' },
    current: false,
  },
  {
    name: 'Publicaciones',
    path: '/posts',
    icon: { iconName: 'newspaper', prefix: 'fas' },
    current: false,
  },
  {
    name: 'Mensajes',
    path: '/messages',
    icon: { iconName: 'envelope', prefix: 'fas' },
    current: false,
  },
  {
    name: 'Documents',
    path: '/documents',
    icon: { iconName: 'file-alt', prefix: 'fas' },
    current: false,
  },
  {
    name: 'Usuarios',
    path: '/users',
    icon: { iconName: 'users', prefix: 'fas' },
    current: false,
  },
  {
    name: 'VideoChat',
    path: '/videochat',
    icon: { iconName: 'video', prefix: 'fas' },
    current: false,
  },
  //   { name: "Team", path: "#", icon: UsersIcon, current: false },
  //   { name: "Projects", path: "#", icon: FolderIcon, current: false },
  //   { name: "Calendar", path: "#", icon: CalendarIcon, current: false },
  //   { name: "Documents", path: "#", icon: InboxIcon, current: false },
  //   { name: "Reports", path: "#", icon: ChartBarIcon, current: false },
];
