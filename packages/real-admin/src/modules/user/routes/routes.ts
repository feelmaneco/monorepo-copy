export const ROUTES = {
  REALSTATE_INFO: '/realstateinfo',
  USERS: '/users',
  GALERY: '/galery',
  MESSAGES: '/messages',
  DOCUMENTS: '/documents',
  POSTS: '/posts',
  POSTS_VIEW: '/posts/view/:id',
  SETTINGS: '/settings',
  VIDEO_CHAT: '/videochat',
};
