import React from 'react';
import { BrowserRouter, Route, Routes as ReactRoutes, Navigate } from 'react-router-dom';
import { UserLayout } from '../layouts';
import { UserRealStateDocumentsPage } from '../pages/documents';
import { UserRealStateGaleryPage } from '../pages/galery';
import { UserRealStateMessagesPage } from '../pages/messages';
import { UserRealStatePostsPage } from '../pages/posts';
import { UserRealStateProfilePage } from '../pages/realStateProfile';
import { UserRealStateViewPostPage } from '../pages/viewPost';
import { UserRealStateUserSettingsPage } from '../pages/UserSettings';
// import { UserVideoChatPage } from "../pages/videochat";
import { ROUTES } from './routes';
import { UserUsersPage } from '../pages/users';
// import { UserUsersPage } from "../pages/users";

export const Routes: React.FC = () => {
  return (
    <BrowserRouter>
      <ReactRoutes>
        <Route path={'/'} element={<UserLayout />}>
          <Route path={ROUTES.REALSTATE_INFO} element={<UserRealStateProfilePage />} />
          <Route path={ROUTES.GALERY} element={<UserRealStateGaleryPage />} />
          <Route path={ROUTES.MESSAGES} element={<UserRealStateMessagesPage />} />
          <Route path={ROUTES.DOCUMENTS} element={<UserRealStateDocumentsPage />} />
          <Route path={ROUTES.POSTS} element={<UserRealStatePostsPage />} />
          <Route path={ROUTES.POSTS_VIEW} element={<UserRealStateViewPostPage />} />
          <Route path={ROUTES.SETTINGS} element={<UserRealStateUserSettingsPage />} />
          {/* 
            Used <Navigate> for redirect to /dashboard the /user path writed by the user in the url, 
            however, /dashboard doesn't exist yet so it send a warning in the console
          */}
          <Route path={ROUTES.USERS} element={<UserUsersPage />} />
          {/* <Route path={ROUTES.VIDEO_CHAT} element={<UserVideoChatPage />} /> */}
        </Route>

        {/* <Route path="*" element={<CustomRedirect />} /> */}
      </ReactRoutes>
    </BrowserRouter>
  );
};
