//import { time } from "console";
import { Unsubscribe } from 'firebase/auth';
import { collection, getFirestore, onSnapshot, query, where } from 'firebase/firestore';
import React, { useContext, useEffect, useState } from 'react';
import { ClipLoader } from 'react-spinners';

import { FirebaseCollections } from '../../../../interfaces/firebaseCollections';
import { IRealStateBlogPost } from '../../../../interfaces/Post.inerface';

import { Post } from '../../components/Post';
import { RealStateContext } from '../../providers/RealState';

export const PostsContainer: React.FC = () => {
  const [posts, setPosts] = useState<IRealStateBlogPost[]>([]);
  const [isLoadingPosts, setIsLoadingPosts] = useState<boolean>(true);
  const [isEmpty, setIsEmpty] = useState<boolean>(false);
  const { realStateInfo } = useContext(RealStateContext);
  const orderByDate = () => {
    return posts.sort((a, b) => (a.time < b.time ? 1 : -1));
  };
  useEffect(() => {
    let unsubscribe: Unsubscribe | undefined = undefined;
    if (realStateInfo.name) {
      const db = getFirestore();
      const q = query(collection(db, FirebaseCollections.POSTS), where('realStateName', '==', realStateInfo.name));

      unsubscribe = onSnapshot(q, querySnapshot => {
        if (querySnapshot.empty) {
          setIsLoadingPosts(false);
          setIsEmpty(true);
        }

        querySnapshot.docChanges().forEach(change => {
          setIsLoadingPosts(false);
          if (change.type === 'added') {
            setPosts(prev => [...prev, change.doc.data().post as IRealStateBlogPost]);
          }
        });
      });
    }
    return () => {
      if (typeof unsubscribe === 'function') {
        setPosts([]);
        console.log('unmounted');
        unsubscribe();
      }
    };
  }, [realStateInfo]);
  // const history = useHistory();
  // const newPostLinkHanlder = () => {
  //   history.push("/posts/new");
  // };
  // const posts = useSelector(getPostsSelector);
  // const postsAreLoaded = useSelector(postsAreLoadedSelector);
  // const dispatch = useDispatch();
  // const info = useSelector(realStateInfoSelector);

  // useEffect(() => {
  //   if (info && posts.length === 0 && !postsAreLoaded) {
  //     dispatch(getPosts({ data: { realStateName: info.name } }));
  //   }
  // }, [info, dispatch, posts, postsAreLoaded]);

  return (
    <>
      <ul>
        {isLoadingPosts && (
          <div className='m-auto mt-24 w-60'>
            <ClipLoader color={'#047857'} loading={isLoadingPosts} size={100} />
          </div>
        )}
        {isEmpty && <div>No tienes notificaciones</div>}
        {posts && (
          <div className='container'>
            {orderByDate().map((post, index) => (
              <Post post={post} key={index} />
            ))}
          </div>
        )}
      </ul>
    </>
  );
};
