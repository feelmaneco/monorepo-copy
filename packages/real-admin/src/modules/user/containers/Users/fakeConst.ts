import { RealSatateOwner } from '../../../../interfaces/RealStateOwner.interface';
import { faker } from '@faker-js/faker';

export const fakeUsers: () => RealSatateOwner[] = () => {
  const response = [];
  for (let i = 0; i < 2; i++) {
    response.push({
      id: faker.datatype.uuid(),
      name: faker.name.firstName(),
      lastname: faker.name.lastName(),
      apartment: faker.datatype.number({ min: 1, max: 100 }).toString(),
      entrance: faker.datatype.number({ min: 1, max: 100 }).toString(),
      status: 'Activo',
      email: faker.internet.email(),
      controls: {
        microphoneActive: false,
        videoActive: false,
      },
    });
  }
  return response;
};
