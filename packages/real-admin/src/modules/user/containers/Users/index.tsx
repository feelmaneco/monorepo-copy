import React, { useContext, useState } from 'react';
import { RealSatateOwner } from '../../../../interfaces/RealStateOwner.interface';
import { createRealStateOwner } from '../../../../services/firebase/user/createRealStateOwner';
import { notification } from '../../../../utils/showNotifiactions';
import { AddNewUser } from '../../components/AddNewUser';
import { UsersTable } from '../../components/UsersTable';
import { useUsers } from '../../hooks/useUsers';
import { RealStateContext } from '../../providers/RealState';

export const UsersContainer: React.FC = () => {
  // const [users] = useState(fakeUsers);
  const { realStateInfo } = useContext(RealStateContext);

  const [showAddNewUser, setShowAddNewUser] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const { users } = useUsers(realStateInfo.uid);

  const onCreateHandler = () => {
    setShowAddNewUser(!showAddNewUser);
  };
  const createNewUserHandler = async (user: RealSatateOwner) => {
    try {
      setIsLoading(true);
      const resposne = (await createRealStateOwner({
        realStateId: realStateInfo.uid,
        user,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
      })) as any;
      console.log('response', resposne);
      if (resposne.data.error === true) {
        throw new Error(resposne.data.message);
      }
      notification({
        text: `Usuario ${user.email} creado`,
        type: 'success',
      });
    } catch (error) {
      console.log(error);
      notification({
        text: 'Problemas con crear el usuario',
        type: 'error',
      });
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <>
      <div className='mt-4 w-full rounded-md bg-white p-8'>
        <div className='flex items-center justify-between pb-6'>
          <div>
            <h2 className='font-semibold text-gray-600'>Lista de Usuarios</h2>
          </div>
          <div className='flex items-center justify-between'>
            <div className='ml-10 space-x-8 lg:ml-40'>
              <button
                onClick={onCreateHandler}
                className={`bg-${
                  showAddNewUser ? 'red' : 'green'
                }-500 cursor-pointer rounded-md px-4 py-2 font-semibold tracking-wide text-white`}
              >
                {showAddNewUser ? 'Cerrar' : ' Crear'}
              </button>
            </div>
          </div>
        </div>

        {showAddNewUser && <AddNewUser onCreateUser={createNewUserHandler} isLoading={isLoading} />}
        <div>
          <div className='-mx-4 overflow-x-auto px-4 py-4 sm:-mx-8 sm:px-8'>
            <div className='inline-block min-w-full overflow-hidden rounded-lg shadow'>
              <UsersTable users={users} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
