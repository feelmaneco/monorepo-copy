import { collection, getFirestore, onSnapshot, query, Unsubscribe, where } from 'firebase/firestore';
import React, { useContext, useEffect, useState } from 'react';
import { ClipLoader } from 'react-spinners';
import { FirebaseCollections } from '../../../../interfaces/firebaseCollections';
import { IMessageEntity } from '../../../../interfaces/Message.interface';
import { Message } from '../../components/Message';
import { RealStateContext } from '../../providers/RealState';

export const MessagesContainer: React.FC = () => {
  const { realStateInfo } = useContext(RealStateContext);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isEmpty, setIsEmpty] = useState<boolean>(false);
  const [messages, setMessages] = useState<IMessageEntity[]>([]);

  useEffect(() => {
    let unsubscribe: Unsubscribe | undefined = undefined;
    if (realStateInfo.name) {
      const db = getFirestore();
      const q = query(collection(db, FirebaseCollections.MESSAGES), where('realStateName', '==', realStateInfo.name));
      unsubscribe = onSnapshot(q, querySnapshot => {
        if (querySnapshot.empty) {
          setIsEmpty(true);
          setIsLoading(false);
        }

        querySnapshot.docChanges().forEach(change => {
          setIsLoading(false);
          console.log('CHANGE', change);
          if (change.type === 'added') {
            console.log('add', change.doc.data());
            setMessages(prev => [...prev, change.doc.data() as IMessageEntity]);
          }
        });
      });
      return () => {
        if (typeof unsubscribe === 'function') {
          console.log('Unmount');
          unsubscribe();
        }
      };
    }
  }, [realStateInfo]);

  // const info = useSelector(realStateInfoSelector);
  // getMessages
  // useEffect(() => {
  //   if (messsages.length === 0 && info) {
  //     dispatch(getMessages({ data: { realStateName: info.name } }));
  //   }
  // }, [messsages, info, dispatch]);
  // useEffect(() => {
  //   const getMessageFunc = async (realStateName: string) => {
  //     const resposne = await getMessages({ realStateName });
  //     console.log("RESPONSE", resposne);
  //     if (resposne.error === false) {
  //       setMessages(resposne.data.messages);
  //     }
  //   };
  //   if (info) {
  //     getMessageFunc(info.name);
  //   }
  // }, [info]);
  return (
    <>
      {isEmpty && <div>No tienes mensajes</div>}
      {isLoading && (
        <div className='m-auto mt-24 w-60'>
          <ClipLoader color={'#047857'} loading={isLoading} size={100} />
        </div>
      )}
      {messages && messages.map((message, index) => <Message message={message} key={index} />)}
    </>
  );
};
