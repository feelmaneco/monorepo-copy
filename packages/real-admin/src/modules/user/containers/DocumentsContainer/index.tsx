import React, { useContext, useEffect, useState } from 'react';
import { IRealStateDocumentEntity } from '../../../../interfaces/Document.interface';
import { AddImageBox } from '../../components/AddImageBox';
import { DocumentBox } from '../../components/DocumentBox';
import { ClipLoader } from 'react-spinners';
import Modal from 'react-modal';
import PDFViewer from '../../../../components/PDFViewer';
import { updateDocumentsInDB, uploadDocumentFile } from '../../../../services/firebase/documents/uploadDocument';
import { RealStateContext } from '../../providers/RealState';
import { collection, getFirestore, onSnapshot, query, Unsubscribe, where } from 'firebase/firestore';
import { FirebaseCollections } from '../../../../interfaces/firebaseCollections';
import { notification } from '../../../../utils/showNotifiactions';

export const DocumentContainer: React.FC = () => {
  const { realStateInfo } = useContext(RealStateContext);
  const [isLoading, setIsLoading] = useState(true);

  const [activeDocument, setActiveDocument] = useState<IRealStateDocumentEntity | null>();
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [documents, setDocuments] = useState<IRealStateDocumentEntity[]>([]);

  const onUploadClick = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0] && realStateInfo.name) {
      try {
        setIsLoading(true);
        const response = await uploadDocumentFile(e.target.files[0]);
        notification({
          type: 'success',
          text: 'Se ha cargado el documento con éxito!',
        });
        console.log('REsponse', response, realStateInfo.name);
        await updateDocumentsInDB({
          url: response.url,
          name: response.name,
          realStateName: realStateInfo.name,
        });
      } catch (error) {
        notification({
          type: 'error',
          text: 'Se ha producido un error al cargar el documento',
        });
        console.log('ERROR', error);
      } finally {
        setIsLoading(false);
      }
    }
  };
  const onDocumentClick = (document: IRealStateDocumentEntity) => {
    setActiveDocument(document);
    toggleModal();
  };
  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  useEffect(() => {
    let unsubscribe: Unsubscribe | undefined = undefined;
    if (realStateInfo.name) {
      const db = getFirestore();
      const q = query(collection(db, FirebaseCollections.DOCUMENTS), where('realStateName', '==', realStateInfo.name));
      unsubscribe = onSnapshot(q, querySnapshot => {
        if (querySnapshot.empty) {
          setIsLoading(false);
        }

        querySnapshot.docChanges().forEach(change => {
          setIsLoading(false);
          console.log('CHANGE', change);
          if (change.type === 'added') {
            console.log('add', change.doc.data());
            setDocuments(prev => [...prev, change.doc.data() as IRealStateDocumentEntity]);
          }
        });
      });
      return () => {
        if (typeof unsubscribe === 'function') {
          console.log('Unmount');
          unsubscribe();
        }
      };
    }
  }, [realStateInfo]);

  return (
    <>
      <div className='container'>
        {documents.length <= 0 ? (
          <>
            {isLoading ? (
              <div className='m-auto mt-24 w-60'>
                <ClipLoader color={'#047857'} size={100} />
              </div>
            ) : (
              <div className='m-auto mt-24 w-60'>
                <AddImageBox title='Agregar documento' type='pdf' onClick={onUploadClick} />
              </div>
            )}
          </>
        ) : (
          <div className='flex flex-wrap'>
            {documents.map((document, index) => (
              <DocumentBox key={index} onClick={onDocumentClick} document={document} />
            ))}
            <AddImageBox title='Agregar documento' type='pdf' onClick={onUploadClick} />
          </div>
        )}
      </div>
      {activeDocument && (
        <Modal
          isOpen={isModalOpen}
          onRequestClose={() => {
            setIsModalOpen(!isModalOpen);
          }}
          className=''
          contentLabel={activeDocument.name}
        >
          {isLoading ? <ClipLoader color={'#047857'} size={100} /> : <PDFViewer url={activeDocument.url} onClose={toggleModal} />}
        </Modal>
      )}
    </>
  );
};
