import React, { useContext, useEffect, useState } from 'react';
import { ClipLoader } from 'react-spinners';
import { AddImageBox } from '../../components/AddImageBox';
import { ImageBox } from '../../components/ImageBox';
import { IRealStateGaleryEntity } from '../../../../interfaces/Galery.interface';
import { RealStateContext } from '../../providers/RealState';
import { collection, getFirestore, onSnapshot, query, Unsubscribe, where } from 'firebase/firestore';
import { FirebaseCollections } from '../../../../interfaces/firebaseCollections';
import { updateGaleryInDB, uploadFile } from '../../../../services/firebase/uploadGaleryImage';
import { notification } from '../../../../utils/showNotifiactions';
export const GaleryContainer: React.FC = () => {
  const { realStateInfo } = useContext(RealStateContext);
  const [isLoading, setIsLoading] = useState(true);
  const [isEmpty, setIsEmpty] = useState<boolean>(false);
  const [galery, setGalery] = useState<IRealStateGaleryEntity[]>([]);

  const onAddNewImageBoxClickHandler = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.files && e.target.files[0] && realStateInfo.name) {
      try {
        setIsLoading(true);
        const response = await uploadFile(e.target.files[0]);
        console.log('Respuesta: ', response, realStateInfo.name);
        await updateGaleryInDB({
          url: response.url,
          name: response.name,
          realStateName: realStateInfo.name,
        });
        setIsLoading(true);
        notification({
          type: 'success',
          text: 'Se ha cargado la imagen con éxito!',
        });
      } catch (error) {
        notification({
          type: 'error',
          text: 'Se ha producido un error al cargar la imagen',
        });
        console.log('ERROR: ', error);
      } finally {
        setIsLoading(false);
      }
    }
  };
  useEffect(() => {
    let unsubscribe: Unsubscribe | undefined = undefined;
    if (realStateInfo.name) {
      const db = getFirestore();
      const q = query(collection(db, FirebaseCollections.REAL_STATE_GALERY), where('realStateName', '==', realStateInfo.name));
      unsubscribe = onSnapshot(q, querySnapshot => {
        if (querySnapshot.empty) {
          setIsEmpty(true);
          setIsLoading(false);
        }

        querySnapshot.docChanges().forEach(change => {
          setIsLoading(false);
          console.log('CHANGE', change);
          if (change.type === 'added') {
            console.log('add', change.doc.data());
            setGalery(prev => [...prev, change.doc.data() as IRealStateGaleryEntity]);
          }
        });
      });
      return () => {
        if (typeof unsubscribe === 'function') {
          console.log('Unmount');
          unsubscribe();
        }
      };
    }
  }, [realStateInfo]);

  // useEffect(() => {
  //   const downloadImages = async (relaStateName: string) => {
  //     const resposne = await getImages(relaStateName);
  //     if (resposne.images) {
  //       setGalery(resposne.images);
  //     }
  //     setIsLoading(false);
  //   };
  //   if (info) {
  //     setIsLoading(true);
  //     downloadImages(info.name);
  //   }
  //   console.log("RUNNING", info);
  // }, [info]);

  return (
    <>
      {isEmpty && <span>No tienes fotos</span>}

      <div className='flex flex-wrap'>
        {galery.length > 0 ? (
          <>
            {galery.map(item => (
              <>
                <ImageBox
                  key={item.url}
                  image={{
                    src: item.url,
                  }}
                />
              </>
            ))}
            <AddImageBox title='Agregar Foto' onClick={onAddNewImageBoxClickHandler} />
          </>
        ) : (
          <>
            {isLoading ? (
              <div className='m-auto mt-24 w-60'>
                <ClipLoader color={'#047857'} size={100} />
              </div>
            ) : (
              <div className='m-auto mt-24 w-60'>
                <AddImageBox title='Agregar Foto' onClick={onAddNewImageBoxClickHandler} />
              </div>
            )}
          </>
        )}
      </div>
    </>
  );
};
