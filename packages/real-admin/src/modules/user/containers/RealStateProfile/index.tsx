import React, { useContext, useEffect, useState } from 'react';
import { ClipLoader } from 'react-spinners';

import { FindRealStateInfo } from '@dindom/common/build/interfaces/realState/FindRealStateInfo.interface';
import { notification } from '../../../../utils/showNotifiactions';

import { ProfileInfoForm } from '../../components/ProfileInfo/ProfileForm';
import { RealStateContext } from '../../providers/RealState';

export const RealStateProfileContainer: React.FC = () => {
  const { realStateInfo, isLoaded, fetchFindRealStateInfoApi, updateRealStateInfo } = useContext(RealStateContext);
  const [isLoading, setisLoading] = useState(false);
  useEffect(() => {
    if (!isLoaded) {
      fetchFindRealStateInfoApi();
    }
  }, [isLoaded, fetchFindRealStateInfoApi]);

  const handleSubmit = async (data: FindRealStateInfo) => {
    try {
      setisLoading(true);
      await updateRealStateInfo(data); // notification
      setisLoading(false);
      notification({
        type: 'success',
        text: 'se han ingresado los datos con exito!',
      });
    } catch (error) {
      setisLoading(false);
      notification({
        type: 'error',
        text: 'Se ha producido un error al ingresar los datos',
      });
    }
  };

  if (!realStateInfo.name) {
    return (
      <div className='m-auto mt-24 w-60'>
        <ClipLoader color={'#047857'} size={100} />
      </div>
    );
  }
  return (
    <div className='mt-4 w-full rounded-md bg-white p-8'>
      <ProfileInfoForm isLoading={isLoading} initialValues={realStateInfo} onSubmit={handleSubmit} />
    </div>
  );
};
