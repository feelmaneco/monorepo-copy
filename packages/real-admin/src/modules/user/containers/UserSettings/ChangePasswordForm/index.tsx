import { useFormik } from 'formik';
import React, { useContext } from 'react';
import { changeUserPassword } from '../../../../../services/firebase/user/changePassword';
import { UserContext } from '../../../../app/providers/User';
import { signOut } from '../../../services/firebase/signOut';

type ChangePasswordFormInitalValues = {
  password: string;
  newPassword: string;
  reNewPassword: string;
};

export const UserSettingsChangePasswordFormContainer: React.FC = () => {
  const { user } = useContext(UserContext);
  const formik = useFormik<ChangePasswordFormInitalValues>({
    initialValues: {
      newPassword: '',
      password: '',
      reNewPassword: '',
    },
    onSubmit: async ({ newPassword, password, reNewPassword }, { setErrors }) => {
      try {
        if (newPassword !== reNewPassword) {
          setErrors({
            reNewPassword: 'Password no coincide',
            newPassword: 'Password no coincide',
          });
        } else {
          setErrors({});
          if (user && user.email) {
            const changePasswordResponse = await changeUserPassword({
              newPassword,
              oldPassword: password,
              user,
              email: user.email,
            });
            if (changePasswordResponse) {
              signOut();
            }
          }
        }
      } catch (error) {
        console.log('Change password error');
      }
    },
  });
  const { values, handleChange, handleSubmit, errors } = formik;
  return (
    <>
      <form onSubmit={handleSubmit}>
        <label className='m-3 block text-center text-sm font-medium text-gray-700'>Cambiar contraseña</label>
        <div className='col-span-3 sm:col-span-2'>
          <div className='col-span-6 flex flex-col sm:col-span-3'>
            <div className='m-1'>
              <label className='block text-sm font-medium text-gray-700'>Contraseña Actual</label>
              <input
                type='password'
                name='password'
                id='password'
                onChange={handleChange}
                value={values.password}
                className='mt-1 block w-full rounded-md border-2 p-0.5 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
              />
            </div>
            <div className='m-1'>
              <label className='block text-sm font-medium text-gray-700'>Nueva Contraseña</label>
              <input
                type='password'
                id='newPassword'
                name='newPassword'
                onChange={handleChange}
                value={values.newPassword}
                className='mt-1 block w-full rounded-md border-2 p-0.5 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
              />
              <span>{errors.newPassword}</span>
            </div>
            <div className='m-1'>
              <label className='block text-sm font-medium text-gray-700'>Repetir Contraseña</label>
              <input
                type='password'
                name='reNewPassword'
                id='reNewPassword'
                onChange={handleChange}
                value={values.reNewPassword}
                className='mt-1 block w-full rounded-md border-2 p-0.5 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
              />
              <span>{errors.reNewPassword}</span>
            </div>
            <button
              type='submit'
              className='m-auto mt-4 mb-2 rounded-md border border-gray-300 bg-white py-2 px-3 text-center text-sm font-medium leading-4 text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
            >
              Cambiar Contraseña
            </button>
          </div>
        </div>
      </form>
    </>
  );
};
