import React from 'react';
// import { useDispatch, useSelector } from "react-redux";
// import { FindRealStateInfo } from "../../../../interfaces/FindRealStateInfo.interface";
// import { useFormik } from "formik";
// import {
//   realStateGaleryLoadedSelector,
//   realStateGalerySelector,
// } from "../../../../store/slices/RealStateGalery/selectors";
import { UserSettingsChangePasswordFormContainer } from './ChangePasswordForm';
import { UserSettingsChangeUserFormContainer } from './ChangeUserForm';
import { ChangeUserProfileImage } from './ChangeUserProfileImage';
export const UserSettingsContainer: React.FC = () => {
  return (
    <>
      <div className='space-y-6'>
        <div className='bg-white px-4 py-5 shadow sm:rounded-lg sm:p-6'>
          <div className='md:grid md:grid-cols-3 md:gap-6'>
            <div className='md:col-span-1'>
              <h3 className='text-lg font-medium leading-6 text-gray-900'>User Settings</h3>
              <p className='mt-1 text-sm text-gray-500'>This information will be displayed publicly so be careful what you share.</p>
            </div>
            <div className='mt-5 md:col-span-2 md:mt-0'>
              <UserSettingsChangePasswordFormContainer />
              <UserSettingsChangeUserFormContainer />
              <ChangeUserProfileImage />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
