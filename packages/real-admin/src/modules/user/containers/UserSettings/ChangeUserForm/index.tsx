import { useFormik } from 'formik';
import React, { useContext, useState } from 'react';
import { changeUserDisplayName } from '../../../../../services/firebase/user/changeUser';
import { notification } from '../../../../../utils/showNotifiactions';
import { UserContext } from '../../../../app/providers/User';
// import { signOut } from "../../../services/firebase/signOut";

type ChangeUserFormInitalValues = {
  displayName: string;
};

export const UserSettingsChangeUserFormContainer: React.FC = () => {
  const { user, setDisplayName } = useContext(UserContext);
  const [isUpdating, setIsUpdating] = useState(false);
  const { handleSubmit, values, handleChange } = useFormik<ChangeUserFormInitalValues>({
    initialValues: {
      displayName: (user && user.displayName && user.displayName) || '',
    },
    onSubmit: async ({ displayName }) => {
      if (user) {
        console.log('asdasd', user.uid);
      }
      try {
        setIsUpdating(true);
        if (user) {
          await changeUserDisplayName({ user, displayName });
          setDisplayName(displayName);
          notification({
            text: 'Nombre cambiado satisfactoriamente',
            type: 'success',
          });
        }
      } catch (error) {
        notification({
          text: 'Ocurrio un errro cambiando tu nombre',
          type: 'error',
        });
        console.log('Change User error', error);
      } finally {
        setIsUpdating(false);
      }
    },
  });

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className='mt-5 mb-5'>
          <label className='m-3 block text-center text-sm font-medium text-gray-700'>Cambiar Nombre de Usuario</label>
          <div className='mt-4 mb-4 flex'>
            <input
              type='text'
              name='displayName'
              value={values.displayName}
              onChange={handleChange}
              className=' block w-full rounded-md border-2 p-0.5 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
            />

            <button
              type='submit'
              disabled={isUpdating}
              className='m-auto min-w-max  rounded-md border border-gray-300 bg-white py-2 px-3 text-center text-sm font-medium leading-4 text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
            >
              {isUpdating ? 'Actualizando' : 'Cambiar Usuario'}
            </button>
          </div>
        </div>
      </form>
    </>
  );
};
