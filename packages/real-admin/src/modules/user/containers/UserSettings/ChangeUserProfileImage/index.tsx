import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext } from 'react';
import { updateProfileImageInDB } from '../../../../../services/firebase/user/changeUserProfileImage';
import { UserContext } from '../../../../app/providers/User';

export const ChangeUserProfileImage: React.FC = () => {
  const { user } = useContext(UserContext);

  if (!user) {
    return null;
  }
  const onChangeUserProfileImage = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files[0] && user) {
      updateProfileImageInDB({
        file: event.target.files[0],
        user,
      });
    }
  };
  return (
    <div>
      <label className='block text-sm font-medium text-gray-700'>Foto</label>
      <div className='mt-1 flex items-center space-x-5'>
        <span className='inline-block h-12 w-12 overflow-hidden rounded-full bg-gray-100'>
          <img
            src={
              user.photoURL ||
              'https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/images.png?alt=media&token=67241575-57e9-41fc-85e8-00db6fb6b842'
            }
            alt=''
          />
        </span>
        <label>
          <div
            className='
            flex
            cursor-pointer
            items-center
            rounded-md
            border
            border-gray-300
            bg-white
            py-2
            px-3
            text-sm
            font-medium
            leading-4
            text-gray-700
            shadow-sm
            hover:bg-gray-50
            focus:outline-none
            focus:ring-2
            focus:ring-indigo-500
            focus:ring-offset-2'
          >
            <FontAwesomeIcon icon={{ iconName: 'plus-circle', prefix: 'fas' }} size='2x' className='mr-2' />
            <span className='mt-2'>Cambiar imagen</span>
          </div>
          <input type='file' onChange={onChangeUserProfileImage} accept={`image/*`} name='photo' className='hidden' />
        </label>
      </div>
    </div>
  );
};
