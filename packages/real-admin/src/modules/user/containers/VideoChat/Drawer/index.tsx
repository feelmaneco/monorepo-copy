import React from 'react';

type DrawerProps = {
  isOpen: boolean;
  onClose: () => void;
  className?: string;
};

export const Drawer: React.FC<DrawerProps> = ({ isOpen, children, className }) => {
  if (!isOpen) {
    return null;
  }
  return (
    <div
      className={`${className} animate-fade-from-right
      absolute
      right-0
      top-0
      h-full
      rounded-tl-xl
      rounded-bl-xl
      bg-white `}
    >
      {children}
    </div>
  );
};
