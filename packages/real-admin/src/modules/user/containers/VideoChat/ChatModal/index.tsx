import React, { useState } from 'react';

export const ChatModal: React.FC = () => {
  const [messages, setMessages] = useState<string[]>([]);
  const [message, setMessage] = useState('');
  return (
    <>
      Chat Modal
      {console.log('message', messages)}
      <input type='text' value={message} onChange={e => setMessage(e.target.value)} />
      <button
        onClick={() => {
          setMessages([...messages, message]);
        }}
      >
        Enviar
      </button>
    </>
  );
};
