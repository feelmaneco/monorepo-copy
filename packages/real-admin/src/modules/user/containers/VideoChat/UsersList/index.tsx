import React, { useState, useEffect } from 'react';
import { fakeUsers } from '../../Users/fakeConst';
import { UsersListItem } from '../../../components/UsersListItem';

export const UsersList: React.FC = () => {
  const [users, setUsers] = useState(fakeUsers());

  //constante de estado Show list
  const [showListUser, setShowListUser] = useState<boolean>(true);
  const [showViewersListUser, setViewersShowListUser] = useState<boolean>(false);

  useEffect(() => {
    console.log('Impresion use effect', users);
  }, [users]);
  //Estados Handler
  const onParticipantsHandler = () => {
    setShowListUser(!showListUser);
    showViewersListUser && setViewersShowListUser(false);
  };

  const onViewersHandler = () => {
    setViewersShowListUser(!showViewersListUser);
    showListUser && setShowListUser(false);
  };

  const videoClickHanlder = (id: string) => {
    const result = users.findIndex(user => user.id === id);
    if (result !== -1) {
      const newUsers = users;
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      newUsers[result].controls!.videoActive = !newUsers[result].controls!.videoActive;
      setUsers([...newUsers]);
    }
  };
  const microphoneClickHandler = (id: string) => {
    const result = users.findIndex(user => user.id === id);
    if (result !== -1) {
      const newUsers = users;
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      newUsers[result].controls!.microphoneActive = !newUsers[result].controls!.microphoneActive;
      setUsers([...newUsers]);
    }
  };
  return (
    <>
      <div className='h-100% overflow-scroll bg-white'>
        <div className='mx-3 border-b-2 border-gray-200 py-5'>
          <div className='flex rounded-xl bg-gray-100 p-1'>
            <button
              onClick={onParticipantsHandler}
              className={`${
                showListUser ? 'bg-green-600 text-white ' : 'bg-gray-100 text-gray-700 '
              } "px-1 " w-50% rounded-lg  py-2 hover:bg-green-600 hover:text-white `}
            >
              <span>Anfitriones ({fakeUsers().length})</span>
            </button>
            <button
              onClick={onViewersHandler}
              className={`${
                showViewersListUser ? 'bg-green-600 text-white' : 'bg-gray-100 text-gray-700'
              } "px-1 " w-50% rounded-lg  py-2 hover:bg-green-600 hover:text-white `}
            >
              <span>Asistentes</span>
            </button>
          </div>
        </div>
        {showListUser &&
          users.map(user => (
            <UsersListItem
              user={user}
              key={user.id}
              microphoneClickHandler={microphoneClickHandler}
              videoClickHanlder={videoClickHanlder}
            />
          ))}
      </div>
    </>
  );
};
