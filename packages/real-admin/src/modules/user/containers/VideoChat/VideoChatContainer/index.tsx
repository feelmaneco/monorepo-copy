import React, { useState } from 'react';
import { ChatModal } from '../ChatModal';
import { ControlBar } from '../ControlBar';
import { Drawer } from '../Drawer';
import { UsersList } from '../UsersList';

export const VideoChatContainer: React.FC = () => {
  const [isUsersSectionOpen, setIsUsersSectionOpen] = useState(false);
  const [isChatSectionOpen, setIsChatsectionOpen] = useState(false);
  const toggleUsersSection = () => {
    setIsUsersSectionOpen(!isUsersSectionOpen);
    if (isChatSectionOpen) {
      setIsChatsectionOpen(false);
    }
  };
  const toggleChatSection = () => {
    setIsChatsectionOpen(!isChatSectionOpen);
    if (isUsersSectionOpen) {
      setIsUsersSectionOpen(false);
    }
  };
  return (
    <>
      <div
        className='relative
              mt-3
              h-screen-2/3
              rounded-md
              border-2
              border-green-400
              shadow-lg
                '
      >
        {/** Bacground video */}
        <div
          className='
          flex
          h-full
          justify-center
          border-2
  '
        >
          <img src='/testimages/background-videochat.png' className='max-h-full max-w-full bg-cover' alt='asdasd' />
        </div>
        {/** ControlBar */}
        <ControlBar showUsersList={toggleUsersSection} showChat={toggleChatSection} />
        <Drawer className='w-25%' onClose={toggleUsersSection} isOpen={isUsersSectionOpen}>
          <UsersList />
        </Drawer>
        <Drawer className='w-30%' onClose={toggleChatSection} isOpen={isChatSectionOpen}>
          <ChatModal />
        </Drawer>
      </div>
    </>
  );
};
