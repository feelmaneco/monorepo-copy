import React, { useState } from 'react';
import { Chat } from '../../../components/ControlButtons/Chat';
import { Microphone } from '../../../components/ControlButtons/Microphone';
import { QuickQuestion } from '../../../components/ControlButtons/QuickQuestion';
import { ScreenShare } from '../../../components/ControlButtons/ScreenShare';
import { Users } from '../../../components/ControlButtons/Users';
import { Video } from '../../../components/ControlButtons/Video';

type ControlBarProps = {
  showUsersList: () => void;
  showChat: () => void;
};

export const ControlBar: React.FC<ControlBarProps> = ({ showUsersList, showChat }) => {
  const [isMicrophoneActive, setIsMicrophoneActive] = useState<boolean>(true);
  const [isVideoActive, setIsVideoActive] = useState<boolean>(true);

  const videoClickHanlder = () => {
    setIsVideoActive(!isVideoActive);
  };
  const microphoneClickHandler = () => {
    setIsMicrophoneActive(!isMicrophoneActive);
  };
  const screenShareHandler = () => {
    console.log('Screenshare clicked');
  };
  const usersClickHandler = () => {
    console.log('usersClickHandler clicked');
    showUsersList();
  };
  const chatClickHandler = () => {
    showChat();
    console.log('chatClickHandler clicked');
  };
  const quickQuestionClickHandler = () => {
    console.log('quickQuestionClickHandler clicked');
  };

  return (
    <div
      className='absolute
                        bottom-0
                        left-0
                        flex
                        h-20
                        w-full
                        items-center
                        justify-between
                        '
    >
      <div className='justify-left ml-10 flex  flex-1'>
        <Microphone onClick={microphoneClickHandler} isActive={isMicrophoneActive} />
        <Video onClick={videoClickHanlder} isActive={isVideoActive} />
        <ScreenShare onClick={screenShareHandler} />
      </div>
      <div className='flex flex-1 justify-around'>
        <Users onClick={usersClickHandler} />
        <Chat onClick={chatClickHandler} />
      </div>
      <div className='mr-10 flex flex-1  justify-end'>
        <QuickQuestion onClick={quickQuestionClickHandler} />
      </div>
    </div>
  );
};
