import React from 'react';
import Providers from './providers';
import Modal from 'react-modal';
import { ToastContainer } from 'react-toastify';
Modal.setAppElement('#document-viewer');

export const AppModule: React.FC = () => {
  return (
    <>
      <ToastContainer />
      <Providers />
    </>
  );
};
