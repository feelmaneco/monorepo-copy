import React from 'react';
import { BarLoader } from 'react-spinners';
import { Logo } from '../../../../components/Logo';

export const FullPageLoader: React.FC = () => {
  return (
    <div
      className='absolute
        top-0
        left-0
        z-10
        flex
        h-screen
        w-screen
        flex-col
        items-center
        justify-center
        bg-gray-100'
    >
      <Logo isBig />
      <BarLoader color='#10B981' css='text-green-500' />
    </div>
  );
};
