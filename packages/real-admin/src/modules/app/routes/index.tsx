import React, { useContext } from 'react';
import { AuthModule } from '../../auth';
import { UserModule } from '../../user';
import { Providers } from '../../user/providers';
import { FullPageLoader } from '../components/FullPageLoader';
import { UserContext } from '../providers/User';

export const Routes: React.FC = () => {
  const { user } = useContext(UserContext);
  console.log('User', user);
  if (user === null) {
    return <FullPageLoader />;
  }
  if (user === false) {
    return <AuthModule />;
  }
  if (user) {
    return (
      <Providers>
        <UserModule />
      </Providers>
    );
  }
  return null;
};
