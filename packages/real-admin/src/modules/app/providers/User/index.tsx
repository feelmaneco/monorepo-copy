import React, { useState, createContext, useEffect, useContext } from 'react';
import { IUserContext } from './types';
import { User } from 'firebase/auth';
import { useSigninCheck } from 'reactfire';

export const UserContext = createContext<IUserContext>({
  user: null,
  setUser: () => null,
  setDisplayName: () => null,
});
export const UserProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<User | null | false>(null);
  const { data: signInCheckResult } = useSigninCheck();
  const setDisplayName = (name: string) => {
    if (user) {
      setUser({
        ...user,
        displayName: name,
      });
    }
  };
  useEffect(() => {
    console.log(signInCheckResult);
    if (signInCheckResult && !signInCheckResult.signedIn) {
      setUser(false);
    }
    if (signInCheckResult && signInCheckResult.signedIn) {
      setUser(signInCheckResult.user);
    }
  }, [signInCheckResult]);
  return <UserContext.Provider value={{ user, setUser, setDisplayName }}>{children}</UserContext.Provider>;
};
