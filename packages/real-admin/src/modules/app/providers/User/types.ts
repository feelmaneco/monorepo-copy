import { User } from 'firebase/auth';

export interface IUserContext {
  user: User | null | false;
  setUser: React.Dispatch<React.SetStateAction<false | User | null>>;
  setDisplayName: (name: string) => void;
}
