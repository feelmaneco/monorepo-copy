import React from 'react';
import { BrowserRouter, Route, Routes as ReactRoutes } from 'react-router-dom';
import { SignInPage } from '../pages/signIn';
import { SignUpPage } from '../pages/signUp';

export const Routes: React.FC = () => {
  return (
    <BrowserRouter>
      <ReactRoutes>
        <Route path='/' element={<SignInPage />} />
        <Route path='/signup' element={<SignUpPage />} />
        {/* <Route path="*" element={<Navigate to="/signin" />} /> */}
      </ReactRoutes>
    </BrowserRouter>
  );
};
