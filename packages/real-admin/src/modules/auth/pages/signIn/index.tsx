import { useFormik } from 'formik';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Logo } from '../../../../components/Logo';
import { login } from '../../services/firebase/login';
import { FirebaseError } from '@firebase/util';
import { notification } from '../../../../utils/showNotifiactions';
import { BeatLoader } from 'react-spinners';
import { deleteRememberMeFlag, setRememberMeFlag } from '../../../../services/localStorage';

interface FormValues {
  email: string;
  password: string;
}

export const SignInPage: React.FC = () => {
  const [isLoading, setIsloading] = useState(false);
  const [check, setCheck] = useState(true);

  const handleLogin = async (email: string, password: string) => {
    try {
      setIsloading(true);
      await login(email, password);
    } catch (error) {
      setIsloading(false);
      if (error instanceof FirebaseError) {
        console.log('error de firebase: ', error.code);
        if (error.code === 'auth/too-many-requests') {
          notification({
            type: 'error',
            text: 'se han ingresado hecho muchos intentos',
          });
        }

        if (error.code === 'auth/user-not-found') {
          notification({
            type: 'error',
            text: 'No existe ningún registro de usuario que corresponda al identificador proporcionado.',
          });
        }
      }
    }
  };
  const handleCheck = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCheck(e.target.checked);
    if (e.target.checked) {
      deleteRememberMeFlag();
    } else {
      setRememberMeFlag();
    }
  };
  const formik = useFormik<FormValues>({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: values => {
      handleLogin(values.email, values.password);
    },
  });
  const { handleSubmit, handleChange, values, errors } = formik;
  return (
    <div className='flex min-h-screen flex-col justify-center bg-green-700 py-12 sm:px-6 lg:px-8'>
      <div className='sm:mx-auto sm:w-full sm:max-w-md'>
        <Logo isBig color='white' />
      </div>

      <div className='mt-8 sm:mx-auto sm:w-full sm:max-w-md'>
        <div className='bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10'>
          <form className='space-y-6' onSubmit={handleSubmit}>
            <div>
              <label htmlFor='email' className='block text-sm font-medium text-gray-700'>
                Email address
              </label>
              <div className='mt-1'>
                <input
                  id='email'
                  name='email'
                  type='email'
                  onChange={handleChange}
                  value={values.email}
                  autoComplete='email'
                  required
                  className='block
                    w-full
                    appearance-none
                    rounded-md
                    border
                    border-gray-300
                    px-3
                    py-2
                    placeholder-gray-400
                    shadow-sm
                    focus:border-indigo-500
                    focus:outline-none
                    focus:ring-indigo-500
                    sm:text-sm'
                />
                {errors.email && <span>{errors.email}</span>}
              </div>
            </div>

            <div>
              <label htmlFor='password' className='block text-sm font-medium text-gray-700'>
                Password
              </label>
              <div className='mt-1'>
                <input
                  id='password'
                  name='password'
                  type='password'
                  onChange={handleChange}
                  value={values.password}
                  autoComplete='current-password'
                  required
                  className='block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm'
                />
              </div>
            </div>

            <div className='flex items-center justify-between'>
              <div className='flex items-center'>
                <input
                  id='remember-me'
                  name='remember-me'
                  type='checkbox'
                  checked={check}
                  onChange={handleCheck}
                  className='h-4 w-4 rounded border-gray-300 text-green-600 focus:ring-green-500'
                />

                <label htmlFor='remember-me' className='ml-2 block text-sm text-gray-900'>
                  Recordarme
                </label>
              </div>

              <div className='text-sm'>
                <Link to='/courses?sort=name' className='font-medium text-indigo-600 hover:text-indigo-500'>
                  Olvidaste tu contraseña?
                </Link>
              </div>
            </div>

            <div>
              <button
                type='submit'
                className='flex
                    w-full
                    justify-center
                    rounded-md
                    border
                    border-transparent
                    bg-green-600
                    py-2
                    px-4
                    text-sm
                    font-medium
                    text-white
                    shadow-sm
                    hover:bg-indigo-700
                    focus:outline-none
                    focus:ring-2
                    focus:ring-indigo-500
                    focus:ring-offset-2'
              >
                {isLoading ? (
                  <>
                    <div className='grid h-5 place-items-center'>
                      <BeatLoader color={'#047857'} loading={isLoading} size={10} />
                    </div>
                  </>
                ) : (
                  <>Entrar</>
                )}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
