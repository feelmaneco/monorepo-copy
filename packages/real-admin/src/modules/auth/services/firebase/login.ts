import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';

export const login = async (email: string, password: string): Promise<boolean> => {
  const auth = getAuth();
  await signInWithEmailAndPassword(auth, email, password);
  return true;
};
