export enum FirebaseCollections {
  REAL_STATE = 'real-state',
  POSTS = 'real-state-blog-posts',
  REAL_STATE_GALERY = 'real-state-galery',
  MESSAGES = 'real-state-messages',
  DOCUMENTS = 'real-state-documents',
  CONFERENCE_CHAT = 'real-state-conference-chat',
  CONFERENCE = 'real-state-conference',
  USERS = 'real-state-users',
}
