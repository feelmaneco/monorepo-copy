import { Timestamp } from 'firebase/firestore';

export type MessageType = 'string' | 'emotion';

export interface ConferenceMessageItem {
  from: {
    name: string;
    id: string;
    entrance?: string;
    aparment?: string;
  };
  realStateId: string;
  conferenceId: string;
  time: Timestamp;
  messageType: MessageType;
  metadata?: null;
  message: string;
}
