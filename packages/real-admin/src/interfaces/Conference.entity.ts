import { Timestamp } from 'firebase/firestore';

export type RealStateConferenceStatus = 'active' | 'scheduled' | 'ended' | 'canceled';

export interface RealStateConference {
  name: string;
  startTime: Timestamp;
  endTime: Timestamp;
  status: RealStateConferenceStatus;
  realStateId: string;
  creatorId: string;
  createdAt: string;
  updatedAt: string;
}
