export interface RealSatateOwner {
  name: string;
  id: string;
  lastname: string;
  apartment: string;
  entrance: string;
  status: string;
  email: string;
  controls: {
    microphoneActive: boolean;
    videoActive: boolean;
  };
}
