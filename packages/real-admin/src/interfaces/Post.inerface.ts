import { Timestamp } from '@firebase/firestore';

export interface IRealStateBlogPost {
  title?: string;
  subTitle?: string;
  text?: string;
  thubnailPreviewImgUrl?: string;
  url?: string;
  time: Timestamp;
}
