import { FindRealStateInfo } from '@dindom/common/build/interfaces/realState/FindRealStateInfo.interface';
import { IMessageEntity } from './Message.interface';
import { IRealStateBlogPost } from './Post.inerface';

export interface CreatePostResponse {
  isPublished: true;
}
export interface GetPostsResponse {
  posts: IRealStateBlogPost[];
}

export interface GetMessageResponse {
  messages: IMessageEntity[];
}

interface IHttpResponseSuccessList {
  FindRealStateInfo: FindRealStateInfo;
  UpdateRealStateInfo: FindRealStateInfo;
  CreatePost: CreatePostResponse;
  GetPosts: GetPostsResponse;
  GetMessages: GetMessageResponse;
}
export interface IHttpResponseSuccess<
  T extends keyof IHttpResponseSuccessList,
  // extends keyof IHttpResponseSuccessList
> {
  error: false;
  data: IHttpResponseSuccessList[T];
}

export interface IHttpResponseError {
  error: true;
  message: string;
}
