export interface IRealStateDocumentEntity {
  realStateName: string;
  url: string;
  name: string;
}
