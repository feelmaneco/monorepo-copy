import { IconName } from '@fortawesome/fontawesome-common-types';
import { IconPrefix, SizeProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

type IconButton = {
  type: 'icon' | 'default';
  center: boolean;
  icon: {
    prefix: IconPrefix;
    iconName: IconName;
  };
  text?: string;
  iconSize?: SizeProp;
  srLabel?: string;
};

type ButtonProps = {
  onClick: (e?: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  data: IconButton;
};

export const Button: React.FC<ButtonProps> = ({ data, children, onClick }) => {
  if (data.type === 'icon') {
    return (
      <button
        onClick={onClick}
        className='inline-flex w-full justify-center rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-500 shadow-sm hover:bg-gray-50'
      >
        {data.srLabel && <span className='sr-only'>{data.srLabel}</span>}
        <FontAwesomeIcon icon={data.icon} size={data.iconSize} />
      </button>
    );
  }
  return (
    <button
      type='button'
      className='inline-flex items-center rounded border border-transparent bg-indigo-600 px-2.5 py-1.5 text-xs font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
    >
      {children}
    </button>
  );
};
