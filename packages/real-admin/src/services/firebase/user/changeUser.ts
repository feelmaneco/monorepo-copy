import { User, updateProfile } from 'firebase/auth';
import { getFunctions, httpsCallable } from 'firebase/functions';
import { FirebaseOnCallFunction } from '@dindom/common/build/interfaces/firebase/firebaseFunctions.enum';

type changeUserPasswordProps = {
  displayName: string;

  user: User;
};

export const changeUserDisplayName = async ({ displayName, user }: changeUserPasswordProps) => {
  // const credential = EmailAuthProvider.credential(email, oldPassword);
  console.log('USER', user);
  await updateProfile(user, {
    displayName,
  });
  const functions = getFunctions();
  const functionRef = httpsCallable(functions, FirebaseOnCallFunction.CHANGE_USRNAME);
  await functionRef({
    id: user.uid,
    name: displayName,
  });
  console.log('Response');
  return true;
};
