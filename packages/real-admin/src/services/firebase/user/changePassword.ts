import { updatePassword, User, EmailAuthProvider, reauthenticateWithCredential } from 'firebase/auth';

type changeUserPasswordProps = {
  oldPassword: string;
  newPassword: string;
  user: User;
  email: string;
};

export const changeUserPassword = async ({ newPassword, oldPassword, user, email }: changeUserPasswordProps) => {
  const credential = EmailAuthProvider.credential(email, oldPassword);
  await reauthenticateWithCredential(user, credential);
  await updatePassword(user, newPassword);
  return true;
};
