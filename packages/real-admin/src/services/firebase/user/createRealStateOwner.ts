import { getFunctions, httpsCallable } from 'firebase/functions';

type createRealStateOwnerProps = {
  user: {
    name: string;
    lastname: string;
    apartment: string;
    entrance: string;
    status: string;
    email: string;
  };

  realStateId: string;
};

export const createRealStateOwner = async (data: createRealStateOwnerProps) => {
  const fireFunctions = getFunctions();
  const createUserRef = httpsCallable(fireFunctions, 'onCreateNewUserByRealAdmin');
  return await createUserRef(data);
};
