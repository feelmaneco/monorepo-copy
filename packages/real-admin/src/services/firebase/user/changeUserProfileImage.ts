import { User } from 'firebase/auth';
import { getFunctions, httpsCallable } from 'firebase/functions';
import { getDownloadURL, getStorage, ref, uploadBytes } from 'firebase/storage';
import { makeid } from '../../../utils/generateRandomString';

const fileType = (fileType: string): '.jpg' | '.png' => {
  if (fileType.includes('jpeg') || fileType.includes('jpg')) {
    return '.jpg';
  }
  return '.png';
};

export const uploadFile = async (file: File) => {
  const storage = getStorage();
  const storageRef = ref(storage, `/profileImages/${makeid()}${fileType(file.type)}`);
  await uploadBytes(storageRef, file);
  return {
    name: file.name,
    url: await getDownloadURL(storageRef),
  };
};

export const updateProfileImageInDB = async (data: { user: User; file: File }) => {
  const uploadedFile = await uploadFile(data.file);
  const fireFunctions = getFunctions();
  const req = httpsCallable(fireFunctions, 'changeProfileImage');
  console.log('upladedFile', uploadedFile);
  const resposne = await req({
    filePath: uploadedFile.url,
  });
  console.log('Response, updateProfileImageInDB', resposne);
};
