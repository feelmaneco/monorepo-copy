import { RealSatateOwner } from '../../../interfaces/RealStateOwner.interface';
import { FirebaseCollections } from '../../../interfaces/firebaseCollections';
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';

export const getRealStateOwners = async (realStateId: string): Promise<RealSatateOwner[]> => {
  const db = getFirestore();
  const q = query(collection(db, FirebaseCollections.USERS), where('realStateInfo.realstateID', '==', realStateId));
  const resposne = await getDocs(q);
  if (resposne.empty) {
    return [];
  }
  console.log('resposne', resposne);
  //   rs
  return resposne.docs.map(realStateOwner => realStateOwner.data() as RealSatateOwner);
};
