import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { FirebaseCollections } from '../../../interfaces/firebaseCollections';
import { IHttpResponseError, IHttpResponseSuccess } from '../../../interfaces/HttpResponses.interface';
import { IMessageEntity } from '../../../interfaces/Message.interface';

export const getMessages = async (data: { realStateName: string }): Promise<IHttpResponseSuccess<'GetMessages'> | IHttpResponseError> => {
  try {
    const db = getFirestore();
    const q = query(collection(db, FirebaseCollections.MESSAGES), where('realStateName', '==', data.realStateName));
    const updateRealStateInfoReq = await getDocs(q);
    if (updateRealStateInfoReq.empty) {
      return {
        error: false,
        data: {
          messages: [],
        },
      };
    }

    return {
      error: false,
      data: {
        messages: updateRealStateInfoReq.docs.map(item => item.data() as IMessageEntity),
      },
    };
  } catch (error) {
    console.log('Error, getPosts', error);
    return {
      error: true,
      message: '',
    };
  }
};
