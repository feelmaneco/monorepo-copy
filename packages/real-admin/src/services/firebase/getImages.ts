import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { FirebaseCollections } from '../../interfaces/firebaseCollections';
import { IRealStateGaleryEntity } from '../../interfaces/Galery.interface';

export const getImages = async (realStateName: string): Promise<{ images: IRealStateGaleryEntity[] }> => {
  try {
    const db = getFirestore();
    const q = query(collection(db, FirebaseCollections.REAL_STATE_GALERY), where('realStateName', '==', realStateName));
    const response = await getDocs(q);

    if (response.empty) {
      return {
        images: [],
      };
    }
    return {
      images: response.docs.map(item => item.data() as IRealStateGaleryEntity),
    };
  } catch (error) {
    console.log('ERROR', error);
    throw new Error(error instanceof Error ? error.message : 'unhanled message');
  }
};
