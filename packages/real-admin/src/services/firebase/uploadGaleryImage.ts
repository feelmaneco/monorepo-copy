import { getFunctions, httpsCallable } from 'firebase/functions';
import { getDownloadURL, getStorage, ref, uploadBytes } from 'firebase/storage';
import { makeid } from '../../utils/generateRandomString';

const fileType = (fileType: string): '.jpg' | '.png' => {
  if (fileType.includes('jpeg') || fileType.includes('jpg')) {
    return '.jpg';
  }
  return '.png';
};

export const uploadFile = async (file: File) => {
  const storage = getStorage();
  const storageRef = ref(storage, `/realStateGalery/${makeid()}${fileType(file.type)}`);
  await uploadBytes(storageRef, file);
  return {
    name: file.name,
    url: await getDownloadURL(storageRef),
  };
};

export const updateGaleryInDB = async (data: { name: string; url: string; realStateName: string }) => {
  const fireFunctions = getFunctions();
  const updateRealStateGaleryReq = httpsCallable(fireFunctions, 'onRealStateGalery');
  const resposne = await updateRealStateGaleryReq({
    image: {
      name: data.name,
      url: data.url,
    },
    realStateName: data.realStateName,
    type: 'ADD',
  });
  console.log('Response, updateGaleryInDB', resposne);
};
