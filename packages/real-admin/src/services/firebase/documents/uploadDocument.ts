import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { getFunctions, httpsCallable } from 'firebase/functions';
import { getDownloadURL, getStorage, ref, uploadBytes } from 'firebase/storage';
import { IRealStateDocumentEntity } from '../../../interfaces/Document.interface';
import { FirebaseCollections } from '../../../interfaces/firebaseCollections';
import { makeid } from '../../../utils/generateRandomString';

// const fileType = (fileType: string): ".jpg" | ".png" => {
//   if (fileType.includes("jpeg") || fileType.includes("jpg")) {
//     return ".jpg";
//   }
//   return ".png";
// };

export const uploadDocumentFile = async (file: File) => {
  const storage = getStorage();
  const storageRef = ref(storage, `/realStateDocuments/${makeid()}.pdf`);
  await uploadBytes(storageRef, file);
  return {
    name: file.name,
    url: await getDownloadURL(storageRef),
  };
};

export const updateDocumentsInDB = async (data: { name: string; url: string; realStateName: string }) => {
  const fireFunctions = getFunctions();
  const updateRealStateGaleryReq = httpsCallable(fireFunctions, 'onUploadRealStateDocuments');
  const resposne = await updateRealStateGaleryReq({
    image: {
      name: data.name,
      url: data.url,
    },
    realStateName: data.realStateName,
    type: 'ADD',
  });
  console.log('Response, updateGaleryInDB', data.realStateName, resposne);
};

export const getDocuments = async (realStateName: string): Promise<{ documents: IRealStateDocumentEntity[] }> => {
  try {
    const db = getFirestore();
    const q = query(collection(db, FirebaseCollections.DOCUMENTS), where('realStateName', '==', realStateName));
    const response = await getDocs(q);

    if (response.empty) {
      return {
        documents: [],
      };
    }
    return {
      documents: response.docs.map(item => item.data() as IRealStateDocumentEntity),
    };
  } catch (error) {
    console.log('ERROR', error);
    throw new Error(error instanceof Error ? error.message : 'unhanled message');
  }
};
