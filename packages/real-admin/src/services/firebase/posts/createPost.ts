import { getFunctions, httpsCallable } from 'firebase/functions';

import { IHttpResponseError, IHttpResponseSuccess } from '../../../interfaces/HttpResponses.interface';
import { IRealStateBlogPost } from '../../../interfaces/Post.inerface';

export const createPost = async (data: {
  realStateName: string;
  post: IRealStateBlogPost;
}): Promise<IHttpResponseSuccess<'CreatePost'> | IHttpResponseError> => {
  try {
    const fireFunctions = getFunctions();
    const updateRealStateInfoReq = httpsCallable(fireFunctions, 'onCreateNewPost');
    console.log('CALLL', data);
    const fetchRealStateInfo = (
      await updateRealStateInfoReq({
        realStateName: data.realStateName,
        data: data.post,
      })
    ).data as IHttpResponseSuccess<'CreatePost'> | IHttpResponseError;
    if (fetchRealStateInfo.error) {
      throw new Error(fetchRealStateInfo.message);
    }
    console.log('Runnnign');
    return {
      error: false,
      data: fetchRealStateInfo.data,
    };
  } catch (error) {
    console.log('Error, createPost', error);
    return {
      error: true,
      message: '',
    };
  }
};
