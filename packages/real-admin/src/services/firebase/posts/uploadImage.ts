import { getDownloadURL, getStorage, ref, uploadBytes } from 'firebase/storage';
import { makeid } from '../../../utils/generateRandomString';

const fileType = (fileType: string): '.jpg' | '.png' => {
  if (fileType.includes('jpeg') || fileType.includes('jpg')) {
    return '.jpg';
  }
  return '.png';
};
export const uploadPostImageFile = async (file: File) => {
  const storage = getStorage();
  const storageRef = ref(storage, `/postImages/${makeid()}${fileType(file.type)}`);
  await uploadBytes(storageRef, file);
  return {
    name: file.name,
    url: await getDownloadURL(storageRef),
  };
};
