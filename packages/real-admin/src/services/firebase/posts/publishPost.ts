import { doc, getFirestore, setDoc } from 'firebase/firestore';
import { FirebaseCollections } from '../../../interfaces/firebaseCollections';
import { IRealStateBlogPost } from '../../../interfaces/Post.inerface';

export const publishPost = async (realStateName: string, post: IRealStateBlogPost) => {
  try {
    const db = getFirestore();
    await setDoc(doc(db, FirebaseCollections.POSTS), { realStateName, post }, { merge: true });
  } catch (error) {
    console.log('ERROR', error);
    throw new Error(error instanceof Error ? error.message : 'unhanled message');
  }
};
