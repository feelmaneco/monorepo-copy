import { collection, getDocs, getFirestore, orderBy, query, where } from 'firebase/firestore';

import { FirebaseCollections } from '../../../interfaces/firebaseCollections';
import { IHttpResponseError, IHttpResponseSuccess } from '../../../interfaces/HttpResponses.interface';
import { IRealStateBlogPost } from '../../../interfaces/Post.inerface';

export const getPosts = async (data: { realStateName: string }): Promise<IHttpResponseSuccess<'GetPosts'> | IHttpResponseError> => {
  try {
    const db = getFirestore();
    const q = query(collection(db, FirebaseCollections.POSTS), where('realStateName', '==', data.realStateName), orderBy('post.time'));
    const updateRealStateInfoReq = await getDocs(q);
    if (updateRealStateInfoReq.empty) {
      return {
        error: false,
        data: {
          posts: [],
        },
      };
    }

    return {
      error: false,
      data: {
        posts: updateRealStateInfoReq.docs.map(item => item.data().post as IRealStateBlogPost),
      },
    };
  } catch (error) {
    console.log('Error, getPosts', error);
    return {
      error: true,
      message: '',
    };
  }
};
