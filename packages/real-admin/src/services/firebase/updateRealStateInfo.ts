import { getFunctions, httpsCallable } from 'firebase/functions';
import { IHttpResponseError, IHttpResponseSuccess } from '../../interfaces/HttpResponses.interface';
import { FindRealStateInfo } from '@dindom/common/build/interfaces/realState/FindRealStateInfo.interface';

export const updateRealStateInfoApi = async (
  data: FindRealStateInfo,
): Promise<IHttpResponseSuccess<'UpdateRealStateInfo'> | IHttpResponseError> => {
  try {
    const fireFunctions = getFunctions();
    const updateRealStateInfoReq = httpsCallable(fireFunctions, 'updateRealStateInfo');
    const fetchRealStateInfo = (await updateRealStateInfoReq({ data })).data as
      | IHttpResponseSuccess<'UpdateRealStateInfo'>
      | IHttpResponseError;
    if (fetchRealStateInfo.error) {
      throw new Error(fetchRealStateInfo.message);
    }

    return {
      error: false,
      data: fetchRealStateInfo.data,
    };
  } catch (error) {
    return {
      error: true,
      message: '',
    };
  }
};
