import { getFunctions, httpsCallable } from 'firebase/functions';
import { IHttpResponseError, IHttpResponseSuccess } from '../../interfaces/HttpResponses.interface';

export const findRealStateInfo = async (): Promise<IHttpResponseSuccess<'FindRealStateInfo'> | IHttpResponseError> => {
  try {
    const fireFunctions = getFunctions();
    const fetchRealStateInfoReq = httpsCallable(fireFunctions, 'findRealStateInfo');
    const fetchRealStateInfo = await fetchRealStateInfoReq();
    console.log('fetchRealStateInfo.data', fetchRealStateInfo);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    if (fetchRealStateInfo.data && (fetchRealStateInfo.data as any).error) {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      throw new Error((fetchRealStateInfo.data as any).message);
    }

    return {
      error: false,
      data: (fetchRealStateInfo.data as IHttpResponseSuccess<'FindRealStateInfo'>).data,
    };
  } catch (error) {
    console.log('Error', error);
    return {
      error: true,
      message: '',
    };
  }
};
