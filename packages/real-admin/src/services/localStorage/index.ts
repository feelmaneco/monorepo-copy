const REMEBERME_FLAG = 'REMEMBERME_FLAG';

export function getRememberMeFlag() {
  return localStorage.getItem(REMEBERME_FLAG);
}

export function setRememberMeFlag() {
  localStorage.setItem(REMEBERME_FLAG, 'false');
}
export function deleteRememberMeFlag() {
  localStorage.removeItem(REMEBERME_FLAG);
}
