import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { AppModule } from './modules/app';
import reportWebVitals from './reportWebVitals';
import './config/fontawesome';
import 'react-toastify/dist/ReactToastify.css';
import 'tailwindcss/tailwind.css';
import { FirebaseAppProvider } from 'reactfire';
import { getFirebaseConfig } from './config/variables';
import { Suspense } from 'react';
import { FullPageLoader } from './modules/app/components/FullPageLoader';

console.log(getFirebaseConfig());
ReactDOM.render(
  <Suspense fallback={<FullPageLoader />}>
    <FirebaseAppProvider
      suspense={true}
      firebaseConfig={{
        apiKey: 'AIzaSyA4NM8G2Mj_bn6n_i9bX7hQJxvj2SOJS4A',
        authDomain: 'donde-vivo-dev.firebaseapp.com',
        databaseURL: 'https://donde-vivo-dev-default-rtdb.firebaseio.com',
        projectId: 'donde-vivo-dev',
        storageBucket: 'donde-vivo-dev.appspot.com',
        messagingSenderId: '425582762536',
        appId: '1:425582762536:web:d9b8adc7a4b7657a3ab31d',
        measurementId: 'G-EG5V4WXT4G',
      }}
    >
      <AppModule />
    </FirebaseAppProvider>
  </Suspense>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
