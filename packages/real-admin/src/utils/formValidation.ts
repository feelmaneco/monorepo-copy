import { es } from 'yup-locales';
import * as Yup from 'yup';
//setLocale(es);
Yup.setLocale(es);
export default Yup;
