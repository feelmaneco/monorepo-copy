import { toast, ToastPosition } from 'react-toastify';

type notificationProps = {
  position?: ToastPosition;
  autoClose?: number;
  text: string;
  type?: 'info' | 'warning' | 'success' | 'error';
};
export const notification = ({ type = 'info', text, autoClose = 2000, position = 'top-right' }: notificationProps) =>
  toast[type](text, {
    position,
    autoClose,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
  });
