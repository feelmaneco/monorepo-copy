module.exports = {
    project: {
      ios: {},
      android: {}, // grouped into "project"
    },
    config:{
      getProjectRoots() {
          return [
              // Keep your project directory.
              path.resolve(__dirname),
  
              // Include arc/root directory as a new root.
              path.resolve(__dirname, "../.."),
              path.resolve(__dirname, "../")
          ];
      }
    },
    assets: ["./resources/fonts/"], // stays the same
  };