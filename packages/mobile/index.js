/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
console.log('loaded');
AppRegistry.registerComponent(appName, () => App);
