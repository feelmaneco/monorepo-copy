/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import { AppModule } from './src/modules/app';
import './src/config/firebaseLocal';
const App = () => <AppModule />;
console.log('loaded');
export default App;
