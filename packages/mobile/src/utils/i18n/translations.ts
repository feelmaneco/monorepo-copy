const es = {
  authScreen: {
    login: 'Iniciar sesión',
    writeYourEmail: 'Escribe tu email',
    writeYourPassword: 'Escribe tu contraseña',
  },
};

export const getTranslations = () => {
  return es;
};
