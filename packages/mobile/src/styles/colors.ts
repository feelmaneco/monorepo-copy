const 
  White = '#ffffff',
  WhiteSmoke = '#F5F5F5',
  Gray_0 = 'rgba(175, 175, 175, 0.15)',
  Gray_1 = '#ADB5BD',
  Gray_2 = '#949BA5',
  Gray_3 = '#6D6D6D',
  Gray_4 = '#2B2B2B',
  Gray_5 = '#212226',
  Emerald = '#48D8BE',
  SoftPurple = '#EEE6FF',
  Purple = '#D170FF',
  LightOrange = '#EBB43F'
;

export const colorsLogin = {
  background: WhiteSmoke,
  titles: Gray_4,
  text: Gray_3,
  strongText: Gray_5,
};

export const colorsBtn = {
  background: Gray_5,
  text: White,
};

export const placeholderTextColor = Gray_1;
export const colorsInput = {
  background: White,
  border: Gray_0,
};

export const colorsHomeDashboard = {
  containerBackground: Gray_5,
  paginationDotBackground: Gray_2,
};

export const colorsAdvertising = {
  headerTexts: White,
  avatarBackground: Emerald,
  infoBtnBackground: SoftPurple,
  infoBtnText: Gray_5,
  descriptionText: White,
  contactBtnBackground: Emerald,
  contactBtnText: White,
};

export const colorsHomeHeader = {
  avatarBackground: Emerald,
  textTitle: Gray_5,
  textSubtitle: Gray_2,
};

export const colorsHomeLocker = {
  textTitle: Gray_5,
  postbagBackground: LightOrange,
  mailBackground: Emerald,
  amountText: White,
  text: White,
};

export const colorsHomeService = {
  text: Gray_5,
};

export const colorsServiceCard = {
  background: Purple,
  text: White,
};
