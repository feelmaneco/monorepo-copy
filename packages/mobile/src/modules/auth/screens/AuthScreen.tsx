import React from 'react';
import { SafeAreaView } from 'react-native';
import AuthScreenContainer from '../containers/AuthScreenContainer';

export const AuthScreen: React.FC = () => {
  return (
    <SafeAreaView>
      <AuthScreenContainer />
    </SafeAreaView>
  );
};
