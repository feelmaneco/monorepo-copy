import { StyleSheet } from 'react-native';
import { colorsLogin } from '../../../../styles/colors';
import { fontsLogin } from '../../../../styles/fonts';

export const styles = StyleSheet.create({
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colorsLogin.background,
    fontFamily: fontsLogin.family,
  },
  subtitleText: {
    marginTop: 8,
    marginBottom: 72,
    color: colorsLogin.titles,
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '800',
  },
  containerCheck: {
    display: 'flex',
    height: 25,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 17,
    marginBottom: 39
  },
  iconCheckbox: {
    borderColor: colorsLogin.text,
    borderRadius: 0,
  },
  textCheckbox: {
    color: colorsLogin.text,
    fontSize: 12,
    fontWeight: '600',
    textDecorationLine: 'none',
  },
  registerText: {
    fontFamily: fontsLogin.family,
    fontWeight: '600',
    fontSize: 12,
    textAlign: 'center',
    color: colorsLogin.strongText,
  },
  registerTextStrong: {
    fontWeight: '800',
  },
});
