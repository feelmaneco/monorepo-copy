import React, { useState } from 'react';
import { Text, View } from 'react-native';
import Button from '../../../../components/Button';
import Input from '../../../../components/Input';
import { getTranslations } from '../../../../utils/i18n/translations';
import { fireLogin } from '../../services/firebase/login';
import { styles } from './styles';
import Logo from '../../../../assets/Logo.svg';
import BouncyCheckbox from 'react-native-bouncy-checkbox';

const AuthScreenContainer: React.FC = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const handleLogin = async () => {
    try {
      await fireLogin({ email, password });
      console.log('Success');
    } catch (error) {
      console.log('Error', (error as Error).message);
    }
  };
  const handleEmail = (text: string) => {
    setEmail(text);
  };
  const handlePassword = (text: string) => {
    setPassword(text);
  };
  return (
    <View style={styles.container}>
      <View>
        <Logo />
        <Text style={styles.subtitleText}>Vecinos en conexión</Text>
      </View>

      <View>
        <Input value={email} placeholder={getTranslations().authScreen.writeYourEmail} onChangeText={handleEmail} />

        <Input
          value={password}
          secureTextEntry={true}
          placeholder={getTranslations().authScreen.writeYourPassword}
          onChangeText={handlePassword}
        />
        <View style={styles.containerCheck}>
          <BouncyCheckbox size={18} text="Mantener la sesión iniciada" textStyle={styles.textCheckbox} iconStyle={styles.iconCheckbox} fillColor="#48D8BE" />
        </View>
      </View>

      <Button text={getTranslations().authScreen.login} onPress={handleLogin} />

      <Text style={styles.registerText}>
        Aún no tengo cuenta, <Text style={styles.registerTextStrong}>Registrarme</Text>
      </Text>
    </View>
  );
};

export default AuthScreenContainer;
