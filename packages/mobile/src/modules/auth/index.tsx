import React from 'react';
import { SafeAreaView } from 'react-native';
import { AuthScreen } from './screens/AuthScreen';

export const AuthModule: React.FC = () => {
  return (
    <SafeAreaView>
      <AuthScreen />
    </SafeAreaView>
  );
};
