import React from 'react';
import { View, Image, Text } from 'react-native';
import { styles } from './styles';
import Button from '../../../../components/Button';
import Input from '../../../../components/Input';

type AuthLoginProps = {
  hardCodeBtn: {
    text: string;
    handlePressable(): void;
  };
  inputs: {
    placeHolderEmail: string,
    placeHolderPassword: string,
    handlerEmail(): void,
    handlerPassword(): void;
  };
}

const AuthLogin: React.FC<AuthLoginProps> = ({ hardCodeBtn, inputs }) => {
  return (
    <View style={styles.container}>
      <View>
        <Image source={require('../../../../../images/Logo.jpeg')} />
        <Text style={styles.subtitleText}>Vecinos en conexión</Text>
      </View>

      <View>
        <Input placeholder={inputs.placeHolderEmail} onChangeText={inputs.handlerEmail} />
        <Input placeholder={inputs.placeHolderPassword} onChangeText={inputs.handlerPassword} />
        <Text style={styles.textCheckbox}>Mantener la sesión iniciada</Text>
      </View>

      <Button text={hardCodeBtn.text} onPress={hardCodeBtn.handlePressable} />

      <Text style={styles.registerText}>
        Aún no tengo cuenta, <Text style={styles.registerTextStrong}>Registrarme</Text>
      </Text>
    </View>
  );
};

export default AuthLogin;
