import auth from '@react-native-firebase/auth';

type fireLoginProps = {
  email: string;
  password: string;
};

export const fireLogin = async ({ email, password }: fireLoginProps) => {
  await auth().signInWithEmailAndPassword(email, password);
};
