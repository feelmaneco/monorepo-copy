import React from 'react';
import { SafeAreaView } from 'react-native';
import { Providers } from './providers';
import { AppRoutes } from './routes';

export const AppModule: React.FC = () => {
  return (
    <Providers>
      <AppRoutes />
    </Providers>
  );
};
