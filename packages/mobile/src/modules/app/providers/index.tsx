import React from 'react';
import { AuthProvider } from './auth';

export const Providers: React.FC = ({ children }) => <AuthProvider>{children}</AuthProvider>;
