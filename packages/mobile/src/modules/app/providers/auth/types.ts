import { FirebaseAuthTypes } from '@react-native-firebase/auth';
import { USER_ROLE } from '@dindom/common/build/interfaces/user/roles.enum';
export type AuthUser = FirebaseAuthTypes.User | null | false;

export type AuthContextType = {
  user: AuthUser;
  userRole: USER_ROLE | null;
};
