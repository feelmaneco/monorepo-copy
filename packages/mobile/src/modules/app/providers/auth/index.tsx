import React, { createContext, useEffect, useState } from 'react';
import { AuthContextType, AuthUser } from './types';
import auth from '@react-native-firebase/auth';
import { USER_ROLE } from '@dindom/common/build/interfaces/user/roles.enum';

export const AuthContext = createContext<AuthContextType>({
  user: null,
  userRole: null,
});

export const AuthProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<AuthUser>(null);
  const [userRole, setUserRole] = useState<USER_ROLE | null>(null);
  const onAuthStateChanged = async (fireUser: AuthUser) => {
    if (fireUser) {
      setUser(fireUser);
    } else {
      setUser(false);
    }
  };

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);
  return <AuthContext.Provider value={{ user, userRole }}>{children}</AuthContext.Provider>;
};
