import React, { useContext } from 'react';
import { AuthModule } from '../../auth';
import { UserModule } from '../../user';
import { AuthContext } from '../providers/auth';
import { SplashScreen } from '../screens/SplashScreen';

export const AppRoutes: React.FC = () => {
  const { user } = useContext(AuthContext);

  if (user) {
    return <UserModule />;
  }
  if (user === false) {
    return <AuthModule />;
  }
  return <SplashScreen />;
};
