import React, { createContext, useState } from 'react';
import { Channel as ChannelType } from 'stream-chat';
import { ThreadContextValue } from 'stream-chat-react-native';
import { ChatContextType, StreamChatGenerics } from './types';

export const ChatContext = createContext<ChatContextType>({
  channel: undefined,
  setChannel: () => null,
  setThread: () => null,
  thread: undefined,
});

export const ChatProvider: React.FC = ({ children }) => {
  const [channel, setChannel] = useState<ChannelType<StreamChatGenerics>>();
  const [thread, setThread] = useState<ThreadContextValue<StreamChatGenerics>['thread']>();
  return <ChatContext.Provider value={{ channel, setChannel, setThread, thread }}>{children}</ChatContext.Provider>;
};
