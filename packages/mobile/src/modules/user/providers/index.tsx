import React from 'react';
import { ChatProvider } from './chat';

export const UserProviders: React.FC = ({ children }) => {
  return <ChatProvider>{children}</ChatProvider>;
};
