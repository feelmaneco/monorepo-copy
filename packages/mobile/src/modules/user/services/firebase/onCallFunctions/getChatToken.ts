import functions from '@react-native-firebase/functions';
// import { FirebaseOnCallFunction } from '@dindom/common/build/interfaces/firebase/firebaseFunctions.enum';

export const getChatToken = async (): Promise<string> => {
  const onCallRef = functions().httpsCallable('generateUserToken');
  const result = await onCallRef();
  if (result.data.error === true) {
    throw new Error(result.data.message || 'unknown error');
  }
  if (!result.data.data) {
    throw new Error('no token provided');
  }
  return result.data.data;
};
