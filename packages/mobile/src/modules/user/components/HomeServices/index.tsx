import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import ServiceCard from '../ServiceCard';
import { styles } from './styles';

type HomeServicesProps = {
  serviceCardInfo: {
    subtitleText: string;
    titleText: string;
  };
};

const HomeServices: React.FC<HomeServicesProps> = ({ serviceCardInfo }) => {
  return (
    <View style={styles.homeServicesContainer}>
      <Text style={styles.homeServiceTitle}>Servicios</Text>
      <View style={styles.homeServicesList}>
        {/* TODO: generate the others services; create a map for the others: Canchas, Gimansio, Piscina Sauna */}
        <ServiceCard subtitleText={serviceCardInfo.subtitleText} titleText={serviceCardInfo.titleText} />
      </View>
    </View>
  );
};

export default HomeServices;
