import { StyleSheet, Dimensions } from 'react-native';
import { colorsHomeService } from '../../../../styles/colors';
import { fontsHomeService } from '../../../../styles/fonts';
const { width: windowWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  homeServicesContainer: {
    marginLeft: windowWidth * 0.06,
    marginTop: 38,
    fontFamily: fontsHomeService.family,
    color: colorsHomeService.text,
  },
  homeServiceTitle: {
    fontWeight: '600',
    fontSize: 14,
    letterSpacing: -0.015,
    marginBottom: 7,
  },
  homeServicesList: {
    display: 'flex',
    flexDirection: 'row',
  },
});
