import React from 'react';
import { Pressable, View, Text } from 'react-native';
import { Image } from 'react-native-svg';
import ChatContact from '../ChatContact';

const ChatList: React.FC = () => {
  return (
    <View>
      {/* Bot */}
      <View>
        <Image />
        <Text>Dom</Text>
        <Text>Icon + Hour</Text>
        {/* <Pressable>Enviar mensaje a Dom</Pressable> */}
      </View>

      {/* Contact list  */}
      <View>
        <Text> List of Contacts: Mi_Conjunto just for now</Text>
        <ChatContact name={'Condominio los Cerezos'} />
        <Text>It'd Link to a thread</Text>
      </View>
    </View>
  );
};

export default ChatList;
