import { StyleSheet, Dimensions } from 'react-native';
import { colorsServiceCard } from '../../../../styles/colors';
import { fontsServiceCard } from '../../../../styles/fonts';
const { width: windowWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  serviceCard: {
    width: windowWidth * (0.9 - 0.1),
    height: 100,
    paddingHorizontal: 13,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colorsServiceCard.background,
    borderRadius: 10,
    fontFamily: fontsServiceCard.family,
    color: colorsServiceCard.text,
    fontWeight: '600',
  },
  serviceCardDescription: {
    display: 'flex',
    flexDirection: 'column',
  },
  serviceSubTitle: {
    fontSize: 14,
    letterSpacing: -0.015,
  },
  serviceTitle: {
    fontSize: 21,
    letterSpacing: -0.015,
  },
  serviceImg: {
    width: 94,
    height: 94,
  },
});
