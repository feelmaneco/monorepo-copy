import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import { styles } from './styles';

type ServiceCardProps = {
  subtitleText: string;
  titleText: string;
};

const ServiceCard: React.FC<ServiceCardProps> = ({ subtitleText, titleText }) => {
  return (
    <View style={styles.serviceCard}>
      <View style={styles.serviceCardDescription}>
        <Text style={styles.serviceSubTitle}>{subtitleText}</Text>
        <Text style={styles.serviceTitle}>{titleText}</Text>
      </View>
      <ImageBackground style={styles.serviceImg} source={require('../../../../../images/Calandar.png')} />
    </View>
  );
};

export default ServiceCard;
