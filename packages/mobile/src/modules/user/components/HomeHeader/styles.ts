import { StyleSheet, Dimensions } from 'react-native';
import { colorsHomeHeader } from '../../../../styles/colors';
import { fontsHomeHeader } from '../../../../styles/fonts';
const { width: windowWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  headerContainer: {
    marginTop: 53,
    marginLeft: windowWidth * 0.06,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    fontFamily: fontsHomeHeader.family,
  },
  headerContainerAvatar: {
    width: 48,
    height: 48,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    backgroundColor: colorsHomeHeader.avatarBackground,
    marginRight: windowWidth * 0.03,
  },
  avatarImg: {
    height: 38,
  },
  headerTitle: {
    fontSize: 17,
    fontWeight: '700',
    lineHeight: 24,
    letterSpacing: -0.015,
    color: colorsHomeHeader.textTitle,
  },
  headerSubTitle: {
    fontSize: 13,
    fontWeight: '600',
    lineHeight: 16,
    letterSpacing: -0.015,
    color: colorsHomeHeader.textSubtitle,
  },
});
