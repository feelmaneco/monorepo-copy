import React from 'react';
import { View, Image, Text } from 'react-native';
import { styles } from './styles';

type HomeHeaderProps = {
  user: string;
  residential: string;
};

const HomeHeader: React.FC<HomeHeaderProps> = ({ user, residential }) => {
  return (
    <View style={styles.headerContainer}>
      <View style={styles.headerContainerAvatar}>
        <Image style={styles.avatarImg} source={require('../../../../../images/MainAvatar.png')} />
      </View>
      <View>
        <Text style={styles.headerTitle}>¡Hola {user}!</Text>
        <Text style={styles.headerSubTitle}>{residential}</Text>
      </View>
    </View>
  );
};

export default HomeHeader;
