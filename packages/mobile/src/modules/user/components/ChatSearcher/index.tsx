import React from 'react';
import { Text, View } from 'react-native';
import { styles } from './styles';
import IconEntypo from 'react-native-vector-icons/Entypo';
import Input from '../../../../components/Input';

const handleOnChange = (e: any) => {
  console.log(e);
};
const iconSize = 24,
  iconColor = 'white',
  placeholderTextColor = '#ADB5BD',
  placeholder = 'Buscar contacto';

const ChatSearcher: React.FC = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>Chat</Text>
        <IconEntypo name='plus' size={iconSize} color={iconColor} />
      </View>
      <Input
        inputStyles={styles.input}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        onChangeText={handleOnChange}
      />
    </View>
  );
};

export default ChatSearcher;
