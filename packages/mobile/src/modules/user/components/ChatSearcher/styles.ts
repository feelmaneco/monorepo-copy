import { StyleSheet, Dimensions } from 'react-native';
import { colorsHomeDashboard } from '../../../../styles/colors';
const { width: windowWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  container: {
    height: 135,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#212226',
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 19,
    marginHorizontal: 24,
    marginTop: 37,
  },
  title: {
    // fontFamily: 'Roboto1',
    fontWeight: '600',
    fontSize: 20,
    lineHeight: 25,
    letterSpacing: -0.015,
    color: '#FFFFFF',
  },
  input: {
    width: windowWidth * 0.9,
    height: 36,
    backgroundColor: 'white',
    borderRadius: 4,
    paddingVertical: 6,
    paddingHorizontal: 8,
    marginHorizontal: 16,
    color: 'black',
  },
});
