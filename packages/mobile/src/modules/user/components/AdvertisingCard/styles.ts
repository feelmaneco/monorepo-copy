import { StyleSheet, Dimensions } from 'react-native';
import { colorsAdvertising } from '../../../../styles/colors';
import { fontsAdvertising } from '../../../../styles/fonts';
const { width: windowWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  headerAdvertising: {
    width: windowWidth * (0.9 - 0.1),
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
    fontFamily: fontsAdvertising.headerFamily,
    color: colorsAdvertising.headerTexts,
  },
  headerAdvertisingTitle: {
    fontWeight: '600',
    fontSize: 17,
    letterSpacing: -0.015,
  },
  headerAdvertisingSubTitle: {
    fontWeight: '600',
    fontSize: 10,
  },
  advertisingAvatar: {
    width: 44,
    height: 44,
    backgroundColor: colorsAdvertising.avatarBackground,
    borderRadius: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  advertisingAvatarImg: {
    height: 29,
  },
  advertisingInfoBtn: {
    width: windowWidth * (0.9 - 0.6),
    height: 20,
    backgroundColor: colorsAdvertising.infoBtnBackground,
    borderRadius: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  advertisingInfoBtnText: {
    fontFamily: fontsAdvertising.infoBtnFamily,
    fontWeight: '700',
    fontSize: 12,
    letterSpacing: -0.015,
    color: colorsAdvertising.infoBtnText,
  },
  advertisingDescription: {
    width: windowWidth * 0.78,
    fontFamily: fontsAdvertising.descriptionFamily,
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 18,
    letterSpacing: -0.015,
    color: colorsAdvertising.descriptionText,
    marginBottom: 6,
  },
  advertisingContactBtn: {
    width: windowWidth * 0.8,
    height: 30,
    backgroundColor: colorsAdvertising.contactBtnBackground,
    borderRadius: 10,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  advertisingContactBtnText: {
    fontFamily: fontsAdvertising.contactBtnFamily,
    fontWeight: '600',
    fontSize: 14,
    letterSpacing: -0.015,
    color: colorsAdvertising.contactBtnText,
  },
});
