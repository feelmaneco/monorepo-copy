import React from 'react';
import { Image, Pressable, Text, View } from 'react-native';
import { styles } from './styles';

type AdvertisingProps = {
  dashboardInfo: {
    title: string;
    author: string;
    location: string;
    price: string;
    description: string;
  };
};

const AdvertisingCard: React.FC<AdvertisingProps> = ({ dashboardInfo }) => {
  return (
    <>
      <View style={styles.headerAdvertising}>
        <View>
          <Text style={styles.headerAdvertisingTitle}>{dashboardInfo.title}</Text>
          <Text style={styles.headerAdvertisingSubTitle}>
            {dashboardInfo.author} | {dashboardInfo.location}
          </Text>
        </View>
        <View style={styles.advertisingAvatar}>
          <Image style={styles.advertisingAvatarImg} source={require('../../../../../images/Avatar.png')} />
        </View>
      </View>

      <View style={styles.advertisingInfoBtn}>
        <Text style={styles.advertisingInfoBtnText}>{dashboardInfo.price}</Text>
      </View>

      <Text style={styles.advertisingDescription}>{dashboardInfo.description}</Text>

      <Pressable style={styles.advertisingContactBtn}>
        <Text style={styles.advertisingContactBtnText}>Contactar</Text>
      </Pressable>
    </>
  );
};

export default AdvertisingCard;
