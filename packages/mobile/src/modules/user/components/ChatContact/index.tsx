import React from 'react';
import { View, Text } from 'react-native';
import { Image } from 'react-native-svg';

type ChatContactProps = {
  name: string;
};

const ChatContact: React.FC<ChatContactProps> = ({ name }) => {
  return (
    <View>
      <Image />
      <Text>{name}</Text>
      <Text>Last message</Text>
      <Text>Icon + Hour</Text>
      <Text>Badge</Text>
    </View>
  );
};

export default ChatContact;
