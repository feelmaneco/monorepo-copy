import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import { styles } from './styles';

type HomeLockerProps = {
  amountPostbag: number;
  amountMail: number;
};

const HomeLocker: React.FC<HomeLockerProps> = ({ amountPostbag, amountMail }) => {
  return (
    <View style={styles.locker}>
      <Text style={styles.lockerTitle}>Mi casillero</Text>

      <View style={styles.lockerContainer}>
        <View style={styles.lockerPostbag}>
          <ImageBackground style={styles.postbagImg} source={require('../../../../../images/Postbag.png')} />
          <Text style={styles.amount}>{amountPostbag}</Text>
          <Text style={styles.lockerText}>Correspondencia</Text>
        </View>

        <View style={styles.lockerMail}>
          <ImageBackground style={styles.mailLetterImg} source={require('../../../../../images/MailLetter.png')} />
          <Text style={styles.amount}>{amountMail}</Text>
          <Text style={styles.lockerText}>Correo</Text>
        </View>
      </View>
    </View>
  );
};

export default HomeLocker;
