import { StyleSheet, Dimensions } from 'react-native';
import { colorsHomeLocker } from '../../../../styles/colors';
import { fontsHomeLocker } from '../../../../styles/fonts';
const { width: windowWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  locker: {
    marginLeft: windowWidth * 0.06,
    marginTop: 20,
    fontFamily: fontsHomeLocker.family,
  },
  lockerTitle: {
    fontWeight: '600',
    fontSize: 14,
    letterSpacing: -0.015,
    color: colorsHomeLocker.textTitle,
    marginBottom: 6,
  },
  lockerContainer: {
    width: windowWidth * 0.9,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  lockerPostbag: {
    width: windowWidth * (0.9 - 0.5),
    height: 100,
    backgroundColor: colorsHomeLocker.postbagBackground,
    borderRadius: 10,
    paddingLeft: 13,
  },
  postbagImg: {
    width: 75,
    height: 79,
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  lockerMail: {
    width: windowWidth * (0.9 - 0.5),
    height: 100,
    backgroundColor: colorsHomeLocker.mailBackground,
    borderRadius: 10,
    paddingLeft: 13,
  },
  mailLetterImg: {
    width: 96,
    height: 79,
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  amount: {
    fontWeight: '600',
    fontSize: 40,
    color: colorsHomeLocker.amountText,
    marginTop: 22,
  },
  lockerText: {
    fontWeight: '600',
    fontSize: 14,
    letterSpacing: -0.015,
    color: colorsHomeLocker.text,
  },
});
