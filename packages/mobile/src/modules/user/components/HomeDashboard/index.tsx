import React from 'react';
import { View } from 'react-native';
import AdvertisingCard from '../AdvertisingCard';
import { styles } from './styles';

type DashboardInfoProps = {
  dashboardInfo: {
    title: string,
    author: string,
    location: string,
    price: string,
    description: string,
  };
};

const HomeDashboard: React.FC<DashboardInfoProps> = ({ dashboardInfo }) => {
  return (
    <>
      <View style={styles.dashboardContainer}>
        {/* TODO: generate more Advertisings and link to Pagination */}
        <AdvertisingCard dashboardInfo={dashboardInfo}/>
      </View>

      {/* TODO: create Pagination */}
      <View style={styles.dashboardPagination}>
        <View style={styles.dashboardPaginationDot}></View>
        <View style={styles.dashboardPaginationDot}></View>
        <View style={styles.dashboardPaginationDot}></View>
        <View style={styles.dashboardPaginationDot}></View>
      </View>
    </>
  );
};

export default HomeDashboard;
