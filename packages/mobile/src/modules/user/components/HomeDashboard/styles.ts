import { StyleSheet, Dimensions } from 'react-native';
import { colorsHomeDashboard } from '../../../../styles/colors';
const { width: windowWidth } = Dimensions.get('window');

export const styles = StyleSheet.create({
  dashboardContainer: {
    width: windowWidth * 0.9,
    height: 183,
    marginTop: 25,
    marginLeft: windowWidth * 0.06,
    paddingLeft: windowWidth * 0.05,
    paddingRight: windowWidth * 0.05,
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: colorsHomeDashboard.containerBackground,
    borderRadius: 16,
  },
  dashboardPagination: {
    width: windowWidth * 0.2,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: windowWidth * 0.4,
    marginTop: 13,
  },
  dashboardPaginationDot: {
    width: windowWidth * 0.02,
    height: windowWidth * 0.02,
    backgroundColor: colorsHomeDashboard.paginationDotBackground,
    borderRadius: 50,
  },
});
