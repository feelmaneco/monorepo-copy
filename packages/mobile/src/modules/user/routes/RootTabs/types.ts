import { BottomTabNavigationProp } from '@react-navigation/bottom-tabs';

export type RootTabsList = {
  ChatScreen: undefined;
  HomeScreen: undefined;
  SettingsScreen: undefined;
  RealStateScreen: undefined;
  PaymentsScreen: undefined;
};
export interface IUserRootTabs<T extends keyof RootTabsList> {
  navigation: BottomTabNavigationProp<RootTabsList, T>;
}
