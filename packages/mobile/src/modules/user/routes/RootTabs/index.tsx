import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { UserHomeScreen } from '../../screens/HomeScreen';
import { UserPaymentsScreen } from '../../screens/PaymentsScreen';
import { UserSettingsScreen } from '../../screens/SettingsScreen';
import { UserChatScreen } from '../../screens/ChatScreen';
import { UserRealStateScreen } from '../../screens/RealStateScreen';
import { RootTabsList } from './types';
import HomeIcon from '../../../../assets/icons/homeIcon.svg';
import IconFeather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator<RootTabsList>();

export const UserRootTabs: React.FC = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          header: () => null,
          tabBarIcon: ({ focused, color, size }) => {
            const iconColor = focused ? 'black' : '#949BA5';
            if (route.name === 'HomeScreen') {
              return <HomeIcon size={size} />;
            } else if (route.name === 'PaymentsScreen') {
              return <IconFeather name='credit-card' size={size} color={iconColor} />;
            } else if (route.name === 'SettingsScreen') {
              return <Ionicons name='settings-outline' size={size} color={iconColor} />;
            } else if (route.name === 'ChatScreen') {
              return <Ionicons name='chatbubble-ellipses-outline' size={size} color={iconColor} />;
            } else if (route.name === 'RealStateScreen') {
              return <MaterialCommunityIcons name='account-group-outline' size={size} color={iconColor} />;
            }
          },
          tabBarActiveTintColor: '#212226',
          tabBarInactiveTintColor: '#949BA5',
          tabBarLabelStyle: { fontSize: 12, fontWeight: 'bold' },
        })}
        initialRouteName='HomeScreen'>
        <Tab.Screen
          name='RealStateScreen'
          component={UserRealStateScreen}
          options={{
            title: 'Mi conjunto',
          }}
        />
        <Tab.Screen
          name='PaymentsScreen'
          component={UserPaymentsScreen}
          options={{
            title: 'Pagos',
          }}
        />
        <Tab.Screen
          name='HomeScreen'
          component={UserHomeScreen}
          options={{
            title: 'Inicio',
          }}
        />
        <Tab.Screen
          name='ChatScreen'
          component={UserChatScreen}
          options={{
            title: 'Chat',
          }}
        />
        <Tab.Screen
          name='SettingsScreen'
          component={UserSettingsScreen}
          options={{
            title: 'Ajustes',
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};
