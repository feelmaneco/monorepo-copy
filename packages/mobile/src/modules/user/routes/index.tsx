import React from 'react';
import { UserRootTabs } from './RootTabs';

export const UserRoutes: React.FC = () => {
  return <UserRootTabs />;
};
