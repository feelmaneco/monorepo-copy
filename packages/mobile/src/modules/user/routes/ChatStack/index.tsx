import React, { useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { ChannelScreen } from '../../screens/Chat/ChannelScreen';
import { ChannelListScreen } from '../../screens/Chat/ChannelListScreen';
import { ThreadScreen } from '../../screens/Chat/ThreadScreen';
import { ChatContext } from '../../providers/chat';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export const ChatStack = createStackNavigator<ChatNavigationParamsList>();

export const ChatRoutes: React.FC = () => {
  const { channel } = useContext(ChatContext);
  const safeAreaParams = useSafeAreaInsets();
  console.log('safeAreaParams.top', safeAreaParams.top);
  return (
    <ChatStack.Navigator
      initialRouteName='ChannelList'
      screenOptions={{
        headerShown: false,
        headerTitleStyle: { alignSelf: 'center', fontWeight: 'bold' },
      }}>
      <ChatStack.Screen
        component={ChannelScreen}
        name='Channel'
        options={() => ({
          headerBackTitle: 'Back',
          headerRight: () => <></>,
          headerTitle: channel?.data?.name,
        })}
      />
      <ChatStack.Screen component={ChannelListScreen} name='ChannelList' options={{ headerTitle: 'Channel List' }} />
      <ChatStack.Screen component={ThreadScreen} name='Thread' options={() => ({ headerLeft: () => <></> })} />
    </ChatStack.Navigator>
  );
};
