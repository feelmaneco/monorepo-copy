type ChannelRoute = { Channel: undefined };
type ChannelListRoute = { ChannelList: undefined };
type ThreadRoute = { Thread: undefined };
type ChatNavigationParamsList = ChannelRoute & ChannelListRoute & ThreadRoute;
