import { StreamChat } from 'stream-chat';
import { Streami18n } from 'stream-chat-react-native';
import { StreamChatGenerics } from '../providers/chat/types';
export const chatClient = StreamChat.getInstance<StreamChatGenerics>('t7kw7pb5a899');
export const streami18n = new Streami18n({
  translationsForLanguage: require('./translations.es.json'),
});
