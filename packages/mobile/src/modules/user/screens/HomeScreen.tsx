import React from 'react';
import { SafeAreaView } from 'react-native';
import { UserHomeScreenContainer } from '../containers/HomeScreenContainer';

export const UserHomeScreen: React.FC = () => {
  return (
    <SafeAreaView>
      <UserHomeScreenContainer />
    </SafeAreaView>
  );
};
