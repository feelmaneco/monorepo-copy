export const homeHeaderInfo = {
  user: 'Joan',
  residential: 'Los Gatitos',
};
export const dashboardInfo = {
  title: 'Paseador de perros',
  author: 'Joaquin Rivas',
  location: 'Torre 1 Apto 303',
  price: '$15.000 x5 días',
  description: 'Servicio de paseador de perros todas los días de 10:00 am a 12:00 pm.',
};
export const homeLockerInfo = {
  amountPostbag: 5,
  amountMail: 2,
};
export const serviceCardInfo = {
  subtitleText: 'Reservar',
  titleText: 'Salón Social',
};
