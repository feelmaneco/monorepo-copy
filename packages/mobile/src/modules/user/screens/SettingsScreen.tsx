import React from 'react';
import { UserSettingsScreenContainer } from '../containers/SettingsScreenContainer';

export const UserSettingsScreen: React.FC = () => {
  return <UserSettingsScreenContainer />;
};
