import React from 'react';
import { SafeAreaView } from 'react-native';
import { UserRealStateScreenContainer } from '../containers/RealStateScreenContainer';

export const UserRealStateScreen: React.FC = () => {
  return (
    <SafeAreaView>
      <UserRealStateScreenContainer />
    </SafeAreaView>
  );
};

