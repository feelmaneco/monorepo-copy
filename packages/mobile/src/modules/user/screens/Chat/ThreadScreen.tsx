import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useContext, useEffect } from 'react';
import { Platform, SafeAreaView, Text, View } from 'react-native';
import { Channel, Chat, MessageInput, MessageList, Thread, useAttachmentPickerContext, useOverlayContext } from 'stream-chat-react-native';
import { chatClient, streami18n } from '../../config/chatConfig';
import { ChatContext } from '../../providers/chat';
import { StreamChatGenerics } from '../../providers/chat/types';
import {} from '@react-navigation/stack';
type ThreadScreenProps = {
  navigation: StackNavigationProp<ChatNavigationParamsList, 'Thread'>;
  route: RouteProp<ChatNavigationParamsList, 'Thread'>;
};

export const ThreadScreen: React.FC<ThreadScreenProps> = ({ navigation }) => {
  const { channel, setThread, thread } = useContext(ChatContext);
  //   const headerHeight = useHeaderHeight();
  const { overlay } = useOverlayContext();

  useEffect(() => {
    navigation.setOptions({
      gestureEnabled: Platform.OS === 'ios' && overlay === 'none',
    });
  }, [overlay]);

  return (
    <SafeAreaView>
      <View>
        <Text
          onPress={() => {
            navigation.navigate('Channel');
          }}>
          Back
        </Text>
      </View>
      <Chat client={chatClient} i18nInstance={streami18n}>
        <Channel channel={channel as any} keyboardVerticalOffset={10} thread={thread} threadList>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
            }}>
            <Thread<StreamChatGenerics> onThreadDismount={() => setThread(null)} />
          </View>
        </Channel>
      </Chat>
    </SafeAreaView>
  );
};
