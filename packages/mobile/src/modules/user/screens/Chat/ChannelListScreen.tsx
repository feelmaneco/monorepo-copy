import { StackNavigationProp } from '@react-navigation/stack';
import React, { useContext, useMemo } from 'react';
import { Text, View } from 'react-native';
import { ChannelList, Chat } from 'stream-chat-react-native';
import { chatClient, streami18n } from '../../config/chatConfig';
import { ChatContext } from '../../providers/chat';
import { StreamChatGenerics } from '../../providers/chat/types';
import { ChannelSort } from 'stream-chat';
import { SafeAreaView } from 'react-native-safe-area-context';
type ChannelListScreenProps = {
  navigation: StackNavigationProp<ChatNavigationParamsList, 'ChannelList'>;
};
const filters = {};
const sort: ChannelSort<StreamChatGenerics> = { last_message_at: -1 };
const options = {
  presence: true,
  state: true,
  watch: true,
  limit: 30,
};

export const ChannelListScreen: React.FC<ChannelListScreenProps> = ({ navigation }) => {
  const { setChannel } = useContext(ChatContext);

  const memoizedFilters = useMemo(() => filters, []);

  return (
    <SafeAreaView>
      <Chat client={chatClient} i18nInstance={streami18n}>
        <View style={{ height: '100%' }}>
          <ChannelList<StreamChatGenerics>
            filters={memoizedFilters}
            onSelect={channel => {
              setChannel(channel);
              navigation.navigate('Channel');
            }}
            options={options}
            sort={sort}
          />
        </View>
      </Chat>
    </SafeAreaView>
  );
};
