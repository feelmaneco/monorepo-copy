import { StackNavigationProp } from '@react-navigation/stack';
import React, { useContext, useEffect } from 'react';
import { Platform, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Channel, Chat, MessageInput, MessageList, useAttachmentPickerContext, useOverlayContext } from 'stream-chat-react-native';
import { chatClient, streami18n } from '../../config/chatConfig';
import { ChatContext } from '../../providers/chat';
import { StreamChatGenerics } from '../../providers/chat/types';

type ChannelScreenProps = {
  navigation: StackNavigationProp<ChatNavigationParamsList, 'Channel'>;
};

export const ChannelScreen: React.FC<ChannelScreenProps> = ({ navigation }) => {
  const { channel, setThread, thread } = useContext(ChatContext);
  const headerHeight = 10;
  const { setTopInset } = useAttachmentPickerContext();
  const { overlay } = useOverlayContext();

  useEffect(() => {
    navigation.setOptions({
      gestureEnabled: Platform.OS === 'ios' && overlay === 'none',
    });
  }, [overlay]);

  useEffect(() => {
    setTopInset(headerHeight);
  }, [headerHeight]);

  return (
    <SafeAreaView>
      <View>
        <Text
          onPress={() => {
            navigation.navigate('ChannelList');
          }}>
          Back
        </Text>
      </View>
      <Chat client={chatClient} i18nInstance={streami18n}>
        <Channel channel={channel as any} keyboardVerticalOffset={headerHeight} thread={thread}>
          <View style={{ flex: 1 }}>
            <MessageList<StreamChatGenerics>
              onThreadSelect={thread => {
                setThread(thread);
                if (channel?.id) {
                  navigation.navigate('Thread');
                }
              }}
            />
            <MessageInput />
          </View>
        </Channel>
      </Chat>
    </SafeAreaView>
  );
};
