import React from 'react';
import { SafeAreaView, Text } from 'react-native';
import { UserChatScreenContainer } from '../containers/ChatScreenContainer';

export const UserChatScreen: React.FC = () => {
  return <UserChatScreenContainer />;
};
