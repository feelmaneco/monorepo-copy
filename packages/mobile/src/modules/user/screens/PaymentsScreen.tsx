import React from 'react';
import { UserPaymentsScreenContainer } from '../containers/UserPaymentScreenContainer';

export const UserPaymentsScreen: React.FC = () => {
  return <UserPaymentsScreenContainer />;
};
