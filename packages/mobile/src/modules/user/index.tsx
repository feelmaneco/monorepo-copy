import React from 'react';
import { UserProviders } from './providers';
import { UserRoutes } from './routes';

export const UserModule: React.FC = () => {
  return (
    <UserProviders>
      <UserRoutes />
    </UserProviders>
  );
};
