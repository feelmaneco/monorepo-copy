import React from 'react';
import { Button, Text, View } from 'react-native';
import auth from '@react-native-firebase/auth';
import { SafeAreaView } from 'react-native-safe-area-context';

export const UserSettingsScreenContainer: React.FC = () => {
  return (
    <SafeAreaView>
      <Text>Settings </Text>
      <Button
        title='Logout'
        onPress={() => {
          auth().signOut();
        }}
      />
    </SafeAreaView>
  );
};
