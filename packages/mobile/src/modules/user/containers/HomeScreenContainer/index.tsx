import React from 'react';
import { SafeAreaView, View } from 'react-native';
import HomeDashboard from '../../components/HomeDashboard';
import HomeHeader from '../../components/HomeHeader';
import HomeLocker from '../../components/HomeLocker';
import HomeServices from '../../components/HomeServices';
import { dashboardInfo, homeHeaderInfo, homeLockerInfo, serviceCardInfo } from '../../screens/constants';

import { styles } from './styles';
export const UserHomeScreenContainer: React.FC = () => {
  return (
    <SafeAreaView style={styles.homeContainer}>
      <HomeHeader user={homeHeaderInfo.user} residential={homeHeaderInfo.residential} />
      <HomeDashboard dashboardInfo={dashboardInfo} />
      <HomeLocker amountPostbag={homeLockerInfo.amountPostbag} amountMail={homeLockerInfo.amountMail} />
      <HomeServices serviceCardInfo={serviceCardInfo} />
    </SafeAreaView>
  );
};
