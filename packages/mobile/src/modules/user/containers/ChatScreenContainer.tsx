import React, { useContext, useEffect, useState } from 'react';
import { useColorScheme } from 'react-native';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { OverlayProvider, Streami18n } from 'stream-chat-react-native';
import { useStreamChatTheme } from '../utils/useStreamChatTheme';
import { StreamChatGenerics } from '../providers/chat/types';
import { chatClient, streami18n } from '../config/chatConfig';
import { ChatRoutes } from '../routes/ChatStack';
import { getChatToken } from '../services/firebase/onCallFunctions/getChatToken';
import { AuthContext } from '../../app/providers/auth';
import { FirebaseAuthTypes } from '@react-native-firebase/auth';

export const UserChatScreenContainer: React.FC = () => {
  const [clientReady, setClientReady] = useState(false);
  const [client, setClient] = useState(null);
  const colorScheme = useColorScheme();
  const { bottom } = useSafeAreaInsets();
  const theme = useStreamChatTheme();
  const { user } = useContext(AuthContext);

  // const userToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoicm9uIn0.eRVjxLvd4aqCEHY_JRa97g6k7WpHEhxL7Z4K4yTot1c';

  const setupClient = async () => {
    try {
      if (!user) {
        throw new Error('no user provided');
      }
      const chatUser = {
        id: user.uid,
      };
      const token = await getChatToken();
      if (!chatClient.user) await chatClient.connectUser(chatUser, token);

      return setClientReady(true);
    } catch (error) {
      console.log('Error connecting on chat', error);
    }
  };
  const findChats = async (fireUser: FirebaseAuthTypes.User) => {
    try {
      const { realStateID } = await (await fireUser.getIdTokenResult()).claims;
      const response = await chatClient.queryChannels({ id: realStateID });
      console.log('CHATS', response);
    } catch (error) {
      console.log('ERROR findChats', error);
    }
  };
  useEffect(() => {
    setupClient();
  }, []);
  useEffect(() => {
    if (chatClient.user && clientReady && user) {
      findChats(user);
    }
    console.log('CLIENT', chatClient.user);
  }, [clientReady, user]);
  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <OverlayProvider<StreamChatGenerics> bottomInset={bottom} i18nInstance={streami18n} value={{ style: theme }}>
        {clientReady && <ChatRoutes />}
      </OverlayProvider>
    </GestureHandlerRootView>
  );
};
