import '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import functions from '@react-native-firebase/functions';

if (__DEV__) {
  console.log('RUNNING LOCALLY');
  auth().useEmulator('http://localhost:9099');
  functions().useEmulator('localhost', 5001);
  //   firestore().useEmulator('localhost', 8089);
}
