import React from 'react';
import { Pressable, Text } from 'react-native';
import { styles } from './styles';

type ButtonProps = {
  text: string;
  onPress: () => void;
};

const Button: React.FC<ButtonProps> = ({ text, onPress }) => {
  return (
    <Pressable style={styles.btn} onPress={onPress}>
      <Text style={styles.btnText}>{text}</Text>
    </Pressable>
  );
};

export default Button;
