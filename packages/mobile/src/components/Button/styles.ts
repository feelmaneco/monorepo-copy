import { StyleSheet } from 'react-native';
import { colorsBtn } from '../../styles/colors'
import { fontsBtn } from '../../styles/fonts';

export const styles = StyleSheet.create({
  btn: {
    width: 163,
    height: 56,
    backgroundColor: colorsBtn.background,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 14,
    marginBottom: 17,
  },
  btnText: {
    fontFamily: fontsBtn.family,
    fontWeight: '600',
    fontSize: 17,
    letterSpacing: -0.015,
    color: colorsBtn.text,
  },
});
