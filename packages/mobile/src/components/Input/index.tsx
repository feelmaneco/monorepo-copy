import React from 'react';
import { ImageStyle, TextInput, TextStyle, ViewStyle } from 'react-native';
import { placeholderTextColor as defaultPlaceholderTextColor } from '../../styles/colors';
import { styles } from './styles';

type TextInputProps = {
  onChangeText: (text: string) => void;
  inputStyles?: {
    [key: string]: ViewStyle | TextStyle | ImageStyle | number | string;
  };
  placeholder?: string;
  placeholderTextColor?: string;
  value?: string;
  secureTextEntry?: boolean;
};

const Input: React.FC<TextInputProps> = ({
  inputStyles = styles.inputDesign,
  placeholder,
  onChangeText,
  value,
  placeholderTextColor,
  secureTextEntry = false,
}) => {
  return (
    <TextInput
      style={inputStyles}
      placeholder={placeholder}
      placeholderTextColor={placeholderTextColor ? placeholderTextColor : defaultPlaceholderTextColor}
      onChangeText={onChangeText}
      secureTextEntry={secureTextEntry}
      value={value}
    />
  );
};

export default Input;
