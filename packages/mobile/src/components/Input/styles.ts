import { StyleSheet } from 'react-native';
import { colorsInput } from '../../styles/colors';
import { fontsInput } from '../../styles/fonts';

export const styles = StyleSheet.create({
  inputDesign: {
    width: 296,
    height: 36,
    color: '#212226',
    backgroundColor: colorsInput.background,
    borderWidth: 1,
    borderColor: colorsInput.border,
    borderRadius: 8,
    padding: 3,
    marginBottom: 10,
    paddingLeft: 8,
    fontFamily: fontsInput.family,
  },
});
