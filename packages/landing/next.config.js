const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
const getFunctionsURL = () => {
  if (process.env.ENV === 'production') {
    return 'https://us-central1-donde-vivo-prod.cloudfunctions.net';
  }

  if (process.env.ENV === 'development') {
    return 'https://us-central1-donde-vivo-dev.cloudfunctions.net';
  }

  return 'http://localhost:5001/donde-vivo-dev/us-central1';
};
module.exports = {
  future: {
    webpack5: true,
  },
  images: {
    domains: ['firebasestorage.googleapis.com'],
  },
  env: {
    enviroment: process.env.ENV,
    functionsURL: getFunctionsURL(),

    firebaseconfig:
      process.env.ENV === 'production'
        ? {
            apiKey: 'AIzaSyDNTjH8JmKZZa5dsvZwTJK6Wo6vBo8kLH4',
            authDomain: 'donde-vivo-prod.firebaseapp.com',
            databaseURL: 'https://donde-vivo-prod-default-rtdb.firebaseio.com',
            projectId: 'donde-vivo-prod',
            storageBucket: 'donde-vivo-prod.appspot.com',
            messagingSenderId: '956769231426',
            appId: '1:956769231426:web:9e7057c9981dce427d43db',
            measurementId: 'G-PB96GYNJR2',
          }
        : {
            apiKey: 'AIzaSyA4NM8G2Mj_bn6n_i9bX7hQJxvj2SOJS4A',
            authDomain: 'donde-vivo-dev.firebaseapp.com',
            databaseURL: 'https://donde-vivo-dev-default-rtdb.firebaseio.com',
            projectId: 'donde-vivo-dev',
            storageBucket: 'donde-vivo-dev.appspot.com',
            messagingSenderId: '425582762536',
            appId: '1:425582762536:web:6ee02a12b32644703ab31d',
            measurementId: 'G-03CVWDDFD8',
          },
  },
  //  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
  webpack: config => {
    // load worker files as a urls with `file-loader`
    config.resolve.alias.canvas = false;
    config.resolve.alias.encoding = false;
    config.resolve.alias['~'] = path.resolve(__dirname);
    // config.module.rules.push({
    //   test: /\.(woff|woff2|eot|ttf|svg)$/,
    //   loader: 'url-loader?limit=100000',
    // });
    config.plugins.push(
      new CopyPlugin({
        patterns: [
          {
            from: path.join(__dirname, '../../node_modules/pdfjs-dist/build/pdf.worker.min.js'),
            to: path.join(__dirname, 'public'),
          },
        ],
      }),
    );
    // config.module.rules.unshift({
    //   test: /pdf\.worker\.(min\.)?js/,
    //   use: [
    //     {
    //       loader: "file-loader",
    //       options: {
    //         name: "[contenthash].[ext]",
    //         publicPath: "_next/static/worker",
    //         outputPath: "static/worker",
    //       },
    //     },
    //   ],
    // });

    return config;
  },
};
