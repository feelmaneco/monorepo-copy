import React from 'react';
import { HeroDescription } from '@components/RealState/HeroSection';

type RealStatePageLayoutProps = {
  name: string;
  photoUrl: string;
  address: string;
};

export const RealStatePageLayout: React.FC<RealStatePageLayoutProps> = ({ children, address, name, photoUrl }) => {
  console.log('address', address, name, photoUrl);
  return (
    <main className='container mx-auto mt-4'>
      <HeroDescription name={name} photoUrl={photoUrl} address={address} />
      <>{children}</>
    </main>
  );
};
