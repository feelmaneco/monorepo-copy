import { getFunctions, httpsCallable } from 'firebase/functions';

type sendMessageProps = {
  text: string;
  from: string;
  realStateOwnerId: string;
  realStateUrl: string;
  fromLastname: string;
};
export const sendMessage = async (data: sendMessageProps) => {
  const fireFunctions = getFunctions();
  const senMessageRef = httpsCallable(fireFunctions, 'onCreateMessage');
  const response = await senMessageRef(data);
  console.log('Response', response);
};
