import { ApiInstance } from '@services/api/axios';
import { FirebaseApiEnpoints } from '@dindom/common/build/interfaces/firebase/firebaseApiEnpoints.enum';

export const findAllPosts = async () => {
  const resposne = await ApiInstance.get(FirebaseApiEnpoints.FIND_ALL_POSTS);
  console.log(resposne.data.data);
  return resposne;
};
