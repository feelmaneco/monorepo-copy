import { ApiInstance } from '@services/api/axios';
import { FirebaseApiEnpoints } from '@dindom/common/build/interfaces/firebase/firebaseApiEnpoints.enum';

export const findPostByUrl = async (postUrl: string) => {
  return await ApiInstance.post(FirebaseApiEnpoints.FIND_DINDOM_POST_BY_URL, {
    postUrl,
  });
};
