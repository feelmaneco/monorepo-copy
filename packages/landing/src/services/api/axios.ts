import axios from 'axios';
console.log(process.env.functionsURL);
export const ApiInstance = axios.create({
  baseURL: process.env.functionsURL || 'http://localhost',
});
