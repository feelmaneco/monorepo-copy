import { IRealStateItem } from '@interfaces/getallrealstate';
import { getFunctions, httpsCallable } from 'firebase/functions';

type ResposnseSuccess = {
  error: false;
  data: IRealStateItem[];
};
type ResponseError = {
  error: true;
  message: string;
};

export const getRealStateList = async (): Promise<ResponseError | ResposnseSuccess> => {
  try {
    const fireFunctions = getFunctions();
    const resultRef = httpsCallable(fireFunctions, 'onGetRealStateList');
    const result = await resultRef();
    const preparedResponse = (result.data as { data: IRealStateItem[] }).data.map(
      item =>
        ({
          ...item,
          logoUrl:
            (item.logoUrl && item.logoUrl) ||
            'https://image.shutterstock.com/image-photo/belo-horizonte-minas-gerais-brazil-600w-2087075641.jpg',
        } as IRealStateItem),
    );
    return {
      error: false,
      data: preparedResponse,
    };
  } catch (error) {
    if (error instanceof Error) {
      return {
        error: true,
        message: error.message,
      };
    }
    return {
      error: true,
      message: 'unhandled error',
    };
  }
};
