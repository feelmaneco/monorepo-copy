import { useEffect, useState } from 'react';
import { IBlog } from '@dindom/common/build/interfaces/blog/blog.public.interface';
import { findAllPosts } from '@services/api/dindom-blog/findAllPosts';

export const useBlog = () => {
  const [posts, setPosts] = useState<IBlog[]>([]);
  useEffect(() => {
    const getBlogFunc = async () => {
      const resposne = await findAllPosts();
      if (!resposne.data.error === true) {
        const postsFromDb = resposne.data.data;
        console.log('postsFromDb', postsFromDb);
        setPosts(postsFromDb);
      }
    };
    getBlogFunc();
  }, []);
  return { posts };
};
