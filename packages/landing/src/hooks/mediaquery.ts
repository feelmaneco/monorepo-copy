import { useEffect, useState } from 'react';

export function useMedia() {
  const [size, setSize] = useState<'xs' | 'sm' | 'md' | 'lg' | 'xl' | '2xl'>('xs');
  const [width, setWidth] = useState(0);
  useEffect(() => {
    if (window) {
      setWidth(window.innerWidth);
    }
  });
  useEffect(() => {
    if (window) {
      const width = window.innerWidth;

      if (width > 680 && width < 768) {
        setSize('sm');
      }
      if (width > 768 && width < 1024) {
        setSize('md');
      }
      if (width > 1024 && width < 1280) {
        setSize('lg');
      }
      if (width > 1280 && width < 1534) {
        setSize('xl');
      }
      if (width > 1534) {
        setSize('2xl');
      }
    }
  }, [width]);
  return {
    size,
  };
}
