import { RealStateInfo } from '@dindom/common/build/interfaces/realState/realState';
import React, { createContext, useState } from 'react';

type RealStateContextProps = {
  realState: RealStateInfo | null;
  setRealState: React.Dispatch<React.SetStateAction<RealStateInfo>>;
};

export const RealStateContext = createContext<RealStateContextProps>({
  realState: null,
  setRealState: () => null,
});

export const RealStateProvider: React.FC = ({ children }) => {
  const [realState, setRealState] = useState<RealStateInfo | null>(null);
  return <RealStateContext.Provider value={{ setRealState, realState }}>{children}</RealStateContext.Provider>;
};
