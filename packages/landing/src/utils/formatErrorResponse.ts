export const formatResponseError = (fun: string, error: unknown) => {
  if (error instanceof Error) {
    return {
      error: true,
      message: error.message,
    };
  }
  return {
    error: true,
    message: `${fun}: unknown error`,
  };
};
