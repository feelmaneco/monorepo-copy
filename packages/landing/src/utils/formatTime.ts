import moment from 'moment';

export const formatTime = (time: { _seconds: number; _nanoseconds: number }) => {
  console.log('time._seconds', time._seconds);
  return moment.unix(time._seconds).format('MMMM Do YYYY');
  //   return 'ok';
};
