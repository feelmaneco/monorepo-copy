import React from 'react';
import { toast, ToastPosition } from 'react-toastify';

type notificationProps = {
  position?: ToastPosition;
  autoClose?: number;
  text: string;
  type?: 'info' | 'warning' | 'success' | 'error';
  title?: string;
};
type ToastyDesignComponentProps = {
  title?: string;
  message: string;
};

const ToastyDesignComponent: React.FC<ToastyDesignComponentProps> = ({ title, message }) => {
  return (
    <div className='container-text-toasty'>
      {title && <span className='title-toasty'>{title}</span>}
      <span className='msg-toasty'>{message}</span>
    </div>
  );
};

export const notification = ({ type = 'info', title, text, autoClose = 2000, position = 'top-right' }: notificationProps) =>
  toast[type](<ToastyDesignComponent title={title} message={text} />, {
    position,
    autoClose,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
  });
