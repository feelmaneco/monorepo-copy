import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUser,
  faHome,
  faHouseUser,
  faImages,
  faPlusCircle,
  faNewspaper,
  faFileAlt,
  faArrowRight,
  faArrowCircleDown,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faArrowCircleRight,
  faEnvelope,
  faTimesCircle,
  faPen,
  faCamera,
  faUserCheck,
  faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faTwitter, faGoogle, faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faWindowMaximize } from '@fortawesome/free-regular-svg-icons';

library.add(faUser);
library.add(faFileAlt);
library.add(faWindowMaximize);
library.add(faUserCheck);
library.add(faCheckCircle);
library.add(faHome);
library.add(faHouseUser);
library.add(faFacebook);
library.add(faFacebookF);
library.add(faTwitter);
library.add(faGoogle);
library.add(faImages);
library.add(faPlusCircle);
library.add(faNewspaper);
library.add(faArrowRight);
library.add(faArrowCircleDown);
library.add(faArrowCircleRight);
library.add(faEnvelope);
library.add(faArrowAltCircleLeft);
library.add(faArrowAltCircleRight);
library.add(faTimesCircle);
library.add(faPen);
library.add(faCamera);
