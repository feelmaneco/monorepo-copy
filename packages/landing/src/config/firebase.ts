import { getApps, initializeApp } from 'firebase/app';
getApps().length === 0 &&
  initializeApp(
    (process.env.firebaseconfig && (process.env.firebaseconfig as unknown)) || {
      apiKey: 'AIzaSyA4NM8G2Mj_bn6n_i9bX7hQJxvj2SOJS4A',
      authDomain: 'donde-vivo-dev.firebaseapp.com',
      databaseURL: 'https://donde-vivo-dev-default-rtdb.firebaseio.com',
      projectId: 'donde-vivo-dev',
      storageBucket: 'donde-vivo-dev.appspot.com',
      messagingSenderId: '425582762536',
      appId: '1:425582762536:web:6ee02a12b32644703ab31d',
      measurementId: 'G-03CVWDDFD8',
    },
  );

// const authInstance = getAuth();
// const fireInstance = getFirestore();
// const functions = getFunctions();
// const storage = getStorage();
// if (typeof process.env.REACT_APP_FIREBASE_RECAPTCHA_PROVIDER === 'string' && app) {
//   initializeAppCheck(app, {
//     provider: new ReCaptchaV3Provider(process.env.REACT_APP_FIREBASE_RECAPTCHA_PROVIDER),
//     isTokenAutoRefreshEnabled: true,
//   });
// }
// if (process.env.enviroment === 'development') {
//   if (typeof window !== 'undefined' && window.location && window.location.hostname === 'localhost') {
//     connectAuthEmulator(authInstance, 'http://localhost:9099');
//     connectFirestoreEmulator(fireInstance, 'localhost', 8089);
//     connectFunctionsEmulator(functions, 'localhost', 5001);
//     connectStorageEmulator(storage, 'localhost', 9199);
//   }
// }
