export interface IRealStateItem {
  realStateName: string;
  urlPrefix: string;
  address: string;
  adminName: string;
  logoUrl: string;
}
