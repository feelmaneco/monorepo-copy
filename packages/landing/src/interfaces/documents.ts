export interface IRealStateDocumentsEntity {
  realStateName: string;
  url: string;
  name: string;
}
