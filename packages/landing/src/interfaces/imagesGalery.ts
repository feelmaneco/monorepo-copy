export interface IRealStateGaleryEntity {
  realStateName: string;
  url: string;
  name: string;
}
