export enum FirebaseCollections {
  REAL_STATE = 'real-state',
  POSTS = 'real-state-blog-posts',
  REAL_STATE_GALERY = 'real-state-galery',
  MESSAGES = 'real-state-messages',
  DOCUMENTS = 'real-state-documents',
  NEWS = 'user-news',
  NEW_REAL_STATE_REQUEST = 'real-state-new-request',
  CONTACT_MESSAGES = 'contact-messages',
}
