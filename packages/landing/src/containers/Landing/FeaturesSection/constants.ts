import { IconName, IconPrefix } from '@fortawesome/fontawesome-svg-core';

type fontAwesomeIcon = { custom: false; name: IconName; prefix: IconPrefix };
type customIcon = {
  custom: true;
  url: string;
  size: number;
};
type FeaturesText = {
  icon: fontAwesomeIcon | customIcon;
  title: string;
  description: string;
};

export const featuresText: FeaturesText[] = [
  {
    description:
      'No necesitas contratar personas para hacer la página, te invitamos a unirte a nuestra comunidad de conjuntos y tener su propia sección en la página, estaría ubicada en',
    icon: {
      custom: false,
      name: 'window-maximize',
      prefix: 'far',
    },
    title: 'Tiener una pagina para el conjunto',
  },
  {
    description:
      '¿No sabes como avisar a los propietarios sobre novedades de su conjunto? Ahora todas las noticias de su condomino en un solo lugar',
    icon: { custom: false, name: 'newspaper', prefix: 'fas' },
    title: 'Publicar noticias',
  },
  {
    description: 'Necesitas compartir documentos, DinDom te aydara compartirlos en un click',
    icon: { custom: false, name: 'file-alt', prefix: 'fas' },
    title: 'Compartir documentos',
  },
  {
    description: 'En tu página puedes ubicar la opción de pagar administración para poder realizar los pagos en línea',
    title: 'Pagar administración',
    icon: {
      custom: true,
      size: 112,
      url: '/pseGreen2.svg',
    },
  },
];
