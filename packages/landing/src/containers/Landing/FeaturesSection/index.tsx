import React from 'react';
import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSpring, animated } from 'react-spring';
import { featuresText } from './constants';

export const FeaturesSection: React.FC = () => {
  const imageAnimationsStyle = useSpring({
    from: {
      x: 1000,
    },
    to: {
      x: 100,
    },
  });
  return (
    <div className='flex min-h-screen flex-col items-center pl-8' style={{ minHeight: 800 }}>
      <h2 className='mt-8 items-start text-xl font-extrabold text-green-700 sm:text-2xl md:text-3xl xl:text-4xl 2xl:text-5xl'>
        ¿Porque usar DinDom?
      </h2>
      <div className='mt-8 flex'>
        <div className='flex w-full flex-col flex-wrap items-start justify-center md:flex-row 2xl:w-1/2'>
          {featuresText.map(item => (
            <div key={item.title} className='xl:flex-50% w-full items-center justify-around'>
              <div className='my-5 flex flex-col items-center'>
                {item.icon.custom === false ? (
                  <FontAwesomeIcon
                    icon={{
                      iconName: item.icon.name,
                      prefix: item.icon.prefix,
                    }}
                    size='7x'
                    className='text-green-500'
                  />
                ) : (
                  <img src={item.icon.url} width={item.icon.size} alt={item.icon.url} />
                )}
                <div className='mt-4 flex flex-col items-center text-center'>
                  <h3 className='text-xl font-semibold text-green-700'>{item.title}</h3>
                  <p className='w-9/12 text-sm'>{item.description}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
        <div className='hidden flex-1 2xl:block'>
          <animated.div style={{ ...imageAnimationsStyle }} className='relative z-0 h-full w-full border-2 border-green-500'>
            <Image src='/exampleconjuntos.png' className='' layout='fill' alt='dindom.co' />
          </animated.div>
        </div>
      </div>
    </div>
  );
};
