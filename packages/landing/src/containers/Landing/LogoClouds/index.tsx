import React from 'react';
import Link from 'next/link';

export const LogoClouds: React.FC = () => {
  return (
    <div id='inscribirse' className='bg-white'>
      <div className='max-w-7xl mx-auto px-4 py-12 sm:px-6 lg:px-8 lg:py-16'>
        <div className='lg:grid lg:grid-cols-2 lg:items-center lg:gap-8'>
          <div>
            <h2 className='text-3xl font-extrabold text-gray-800 sm:text-4xl'>Estamos creciendo exponencialmente</h2>
            <div className='mt-8 sm:flex'>
              <div className='rounded-md shadow'>
                <Link href='#registro-nuevo-conjunto'>
                  <a className='flex items-center justify-center rounded-md border border-transparent bg-green-600 px-5 py-3 text-base font-medium text-white hover:bg-green-700'>
                    Inscribir tu conjunto
                  </a>
                </Link>
              </div>
              <div className='mt-3 sm:ml-3 sm:mt-0'>
                <Link href='#contactar'>
                  <a className='flex items-center justify-center rounded-md border border-transparent bg-green-100 px-5 py-3 text-base font-medium text-green-700 hover:bg-green-200'>
                    Contáctenos
                  </a>
                </Link>
              </div>
            </div>
          </div>

          <div className='mt-8 flex flex-col items-center justify-center gap-0.5 md:grid-cols-3 lg:mt-0 lg:grid-cols-2'>
            <h3 className='text-7xl font-bold text-green-700'>50+</h3>
            <p className='text-2xl text-green-500'>Conjuntos conectados</p>
          </div>
        </div>
      </div>
      {/* <Modal
        isOpen={modalIsOpen}
        onRequestClose={toggleModal}
        contentLabel="Example Modal"
      >
        <h2>Hello</h2>
        <button onClick={toggleModal}>close</button>
      </Modal> */}
    </div>
  );
};
