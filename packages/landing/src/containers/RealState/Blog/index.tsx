import React from 'react';
import { Post } from '@components/RealState/Post';
import { IRealStateBlogPost } from '@interfaces/postsBlog';
import { RealStateInfo } from '@dindom/common/src/interfaces/realState/realState';

type RealStateBlogContainerProps = {
  realStateProp: RealStateInfo;
  posts: IRealStateBlogPost[];
};

export const RealStateBlogContainer: React.FC<RealStateBlogContainerProps> = ({ posts }) => {
  // const [posts, setPosts] = useState<IRealStateBlogPost[]>([]);
  const orderByDate = () => {
    return posts.sort((a, b) => (a.time < b.time ? 1 : -1));
  };
  // useEffect(() => {
  //   const getPostFunc = async (name: string) => {
  //     const resposne = await getPosts(name);
  //     setPosts(resposne.posts.map((post) => post.post));
  //   };
  //   if (posts.length === 0 && realState) {
  //     getPostFunc(realState.name);
  //   }
  // }, [posts, realState]);
  return (
    <ul role='list' className=''>
      <div className='container'>
        Post
        {orderByDate().map((post, index) => (
          <Post post={post} key={index} />
        ))}
      </div>
    </ul>
  );
};
