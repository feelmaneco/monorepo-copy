import React from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { IRealStateGaleryEntity } from '@interfaces/imagesGalery';
import { RealStateInfo } from '@dindom/common/build/interfaces/realState/realState';

type RealStateGaleryContainerProps = {
  realStateProp: RealStateInfo;
  galery: IRealStateGaleryEntity[];
};

export const RealStateGaleryContainer: React.FC<RealStateGaleryContainerProps> = ({ galery }) => {
  // useEffect(() => {
  //   if (galery.length === 0 && realState.name) {
  //     (async () => {
  //       const resposne = await getImages(realState.name);
  //       console.log("Resposne", resposne);
  //       setGalery(resposne.images);
  //     })();
  //   }
  // }, [galery, realState]);
  return (
    <div className='container'>
      <div className='container flex flex-wrap'>
        {Array.isArray(galery) &&
          galery.map((image, index) => (
            <div className='mx-2 rounded-lg border-2' key={index}>
              <img width='300px' height='200px' className='rounded-lg' src={image.url} />
            </div>
          ))}
      </div>
      {/* <Carousel
        showArrows={true}
        onChange={onChangeHandler}
        onClickItem={onClickItemHandler}
        onClickThumb={onClickThumbHandler}
      >
        <div>
          <img src="https://res.cloudinary.com/civico/image/upload/c_fit,f_auto,fl_lossy,h_1200,q_auto:low,w_1200/v1422034283/entity/image/file/551/000/520bdcc731e93cfe09000551.jpg" />
          <p className="legend">Legend 1</p>
        </div>
        <div>
          <img src="https://res.cloudinary.com/civico/image/upload/c_fit,f_auto,fl_lossy,h_1200,q_auto:low,w_1200/v1422034286/entity/image/file/552/000/520bdcca31e93cfe09000552.jpg" />
          <p className="legend">Legend 2</p>
        </div>
        <div>
          <img src="https://live.staticflickr.com/2557/5776597659_54b11faaa8_b.jpg" />
          <p className="legend">Legend 3</p>
        </div>
      </Carousel> */}
    </div>
  );
};
