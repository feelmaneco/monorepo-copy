export const descriptionData = {
  name: 'San Pedro Plaza III',
  photoUrl: 'https://img-co-1.trovit.com/pBa1R1hPHM/pBa1R1hPHM.1_11.jpg',
  phoneNumber: '123 3122',
  email: 'sanpedroplaza3@gmail.com',
  address: 'Cll 187 #55b 90',
  status: '3',
  aparmentsNumber: '280',
  administrator: {
    name: 'Jose Felipe Cardenas',
    photoUrl: 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
  },
};
