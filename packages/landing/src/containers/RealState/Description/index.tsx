import React from 'react';
import { AdministratorCard } from '@components/RealState/AdministratorCard';
import { EmailCard } from '@components/RealState/EmailCard';
import { PhoneCard } from '@components/RealState/PhoneCard';
import { DocumentsList } from '@components/RealState/Documents';
import { PayAdministrationContainer } from '@components/RealState/PayAdministrationContainer';
import { GetInTouchContainer } from '@components/RealState/GetInTouch';
import { ClipLoader } from 'react-spinners';
import { IRealStateEntity } from '@dindom/common/src/interfaces/realState/realState';
import { IRealStateDocumentsEntity } from '@interfaces/documents';

type RealStateDescriptionContainerProps = {
  realStateProp: IRealStateEntity;
  documents?: IRealStateDocumentsEntity[];
};

export const RealStateDescriptionContainer: React.FC<RealStateDescriptionContainerProps> = ({ realStateProp, documents }) => {
  return (
    <>
      {realStateProp && (
        <>
          {realStateProp.administrator.name ? (
            <section className='mt-3 flex flex-wrap justify-around'>
              {realStateProp.administrator.photoUrl && (
                <AdministratorCard name={realStateProp.administrator.name} photoUrl={realStateProp.administrator.photoUrl} />
              )}
              {realStateProp.email.administrator && <EmailCard email={realStateProp.email.administrator} />}
              {realStateProp.phoneNumbers && <PhoneCard phone={realStateProp.phoneNumbers.administrator} />}
            </section>
          ) : (
            <div className='mt-3 flex flex-wrap justify-around'>
              <ClipLoader color={'#047857'} size={100} />
            </div>
          )}
          {documents && <DocumentsList documents={documents} />}

          {realStateProp.PSEUrl && <PayAdministrationContainer url={realStateProp.PSEUrl} />}
          <GetInTouchContainer />
        </>
      )}
    </>
  );
};
