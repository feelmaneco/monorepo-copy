import React from 'react';
export default function custom404() {
  return (
    <>
      {
        <html className='h-full'>
          <body className='h-full'>
            <div className='h-screen bg-white px-4 py-16 sm:px-6 sm:py-24 md:grid md:place-items-center lg:px-8'>
              <div className='max-w-max mx-auto'>
                <main className='sm:flex'>
                  <p className='text-4xl font-extrabold text-green-500 sm:text-5xl'>404</p>
                  <div className='sm:ml-6'>
                    <div className='sm:border-l sm:border-gray-200 sm:pl-6'>
                      <h1 className='text-4xl font-extrabold tracking-tight text-gray-900 sm:text-5xl'>Página no encontrada</h1>
                      <p className='mt-1 text-base text-gray-500'>Por favor Revisa la dirección URL nuevamente</p>
                    </div>
                    <div className='mt-10 flex space-x-3 sm:border-l sm:border-transparent sm:pl-6'>
                      <a
                        href='/'
                        className='inline-flex items-center rounded-md border border-transparent bg-green-500 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
                      >
                        Regresar a DinDom
                      </a>
                      <a
                        href='mailto:info@dindom.co'
                        target='_blank'
                        rel='noreferrer'
                        className='inline-flex items-center rounded-md border border-transparent bg-indigo-100 px-4 py-2 text-sm font-medium text-green-500 hover:bg-green-200 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
                      >
                        Contacta a soporte
                      </a>
                    </div>
                  </div>
                </main>
              </div>
            </div>
          </body>
        </html>
      }
    </>
  );
}
