import React from 'react';
import { faUser, faHome, faEnvelope, faCreditCard, faVolumeUp, faGift } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTelegram } from '@fortawesome/free-brands-svg-icons';
import { Footer } from '@components/Footer';
import { Header } from '@components/Header';

export default function Home() {
  return (
    <>
      <Header />
      <div className='w-full bg-white font-man  md:h-max'>
        <div className='flex flex-col md:h-auto md:w-full md:justify-end'>
          <div className=' flex w-full justify-center md:h-80 md:w-50% md:flex-col md:justify-end'>
            <img
              src='https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/landing_static%2FGroup%2067.png?alt=media&token=41868182-7095-43fe-946e-0ac87fda0948'
              className='mt-16 w-50% md:ml-20 md:w-40% md:min-w-30%'
            />
          </div>
          <div className='md:flex md:h-80 md:flex-col'>
            <h1 className='md:h-25% mx-4 p-3 text-center text-3xl font-extrabold text-black-main md:ml-0 md:pl-14 md:text-left md:text-5xl'>
              Vecinos en Conexión
            </h1>
            <p className='mx-7 p-1 text-center text-sm font-medium md:ml-0 md:h-10% md:pl-14 md:text-left md:text-lg'>
              ¡Imaginate poder comunicarte con tus vecinos sin tocar a la puerta!
            </p>
            <div className='flex justify-center bg-gradient-to-b from-white to-green-main pt-7 md:h-60% md:justify-start md:pl-14'>
              <button
                type='button'
                className='h-10 w-40 items-center justify-center rounded-md border border-transparent bg-black-button px-8 text-sm font-extrabold text-white  focus:bg-white focus:text-black focus:outline-none focus:ring-1 focus:ring-black md:px-1 md:text-base'
              >
                Conocer más
              </button>
            </div>
          </div>
        </div>
        <div className='bg-green-main pt-14 md:h-auto md:w-full md:py-14'>
          <div className='mx-12 md:m-0 md:h-20% md:pl-14 md:pb-3'>
            <h4 className='text-center text-base font-extrabold text-white md:text-left md:text-4xl'>¿Qué hace DinDom?</h4>
          </div>
          <div className='mx-12 mt-3 flex flex-col justify-center divide-y text-sm font-semibold md:mx-16 md:grid md:h-80% md:w-35% md:grid-cols-4 md:justify-items-center md:gap-10 '>
            <div className='py-3 text-center transition delay-100 duration-300 ease-in-out hover:-translate-y-1 hover:scale-110 hover:bg-green-main md:col-span-2 md:flex md:h-100% md:w-full md:flex-col md:items-center md:rounded-3xl md:border-none md:bg-white md:p-8 md:hover:bg-white'>
              <div className='hidden md:flex md:flex-row md:pb-5'>
                <FontAwesomeIcon aria-hidden='true' className='w-6 flex-shrink-0 text-green-main' icon={faCreditCard} size='3x' />
              </div>
              <a href='' className='text-sm text-white md:text-lg md:font-medium md:text-gray-main '>
                Pagar tu administración de manera fácil y rápida
              </a>
            </div>
            <div className='py-3 text-center transition delay-100 duration-300 ease-in-out hover:-translate-y-1 hover:scale-110 hover:bg-green-main md:col-span-2 md:flex md:h-100% md:w-full md:flex-col md:items-center md:rounded-3xl md:border-none md:bg-white md:p-8 md:hover:bg-white'>
              <div className='hidden md:flex md:flex-row md:pb-5'>
                <FontAwesomeIcon aria-hidden='true' className='w-6 flex-shrink-0 text-green-main' icon={faVolumeUp} size='3x' />
              </div>
              <a href='' className='text-sm text-white md:text-lg md:font-medium md:text-gray-main'>
                Promocionar tu negocio y ofrecer tus servicios profesionales
              </a>
            </div>
            <div className='py-3 text-center transition delay-100 duration-300 ease-in-out hover:-translate-y-1 hover:scale-110 hover:bg-green-main md:col-span-2 md:flex md:h-100% md:w-full md:flex-col md:items-center md:rounded-3xl md:border-none md:bg-white md:p-8 md:hover:bg-white'>
              <div className='hidden md:flex md:flex-row md:pb-5'>
                <FontAwesomeIcon aria-hidden='true' className='w-6 flex-shrink-0 text-green-main' icon={faGift} size='3x' />
              </div>
              <a href='' className='text-sm text-white md:text-lg md:font-medium md:text-gray-main'>
                Estar actualizado con las últimas noticias de tu edificio
              </a>
            </div>
            <div className='py-3 text-center transition delay-100 duration-300 ease-in-out hover:-translate-y-1 hover:scale-110 hover:bg-green-main md:col-span-2 md:flex md:h-100% md:w-full md:flex-col md:items-center md:rounded-3xl md:border-none md:bg-white md:p-8 md:hover:bg-white'>
              <div className='hidden md:flex md:flex-row md:pb-5'>
                <FontAwesomeIcon aria-hidden='true' className='w-6 flex-shrink-0 text-green-main' icon={faTelegram} size='3x' />
              </div>
              <a href='' className='text-white md:text-lg md:font-medium md:text-gray-main'>
                Chatear con tus vecinos
              </a>
            </div>
          </div>
        </div>
        <div className='my-8 mt-0 sm:drop-shadow-md md:absolute md:top-36 md:right-0 md:rounded-l-3xl md:border md:border-gray-50 md:bg-white'>
          <div className='relative flex bg-gradient-to-b from-green-main to-white pb-6 pt-16 md:static md:rounded-3xl md:bg-white md:from-white'>
            <div className='absolute md:right-96 md:w-35%'>
              <img
                src='https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/landing_static%2F6.png?alt=media&token=206660db-0cfb-4c20-8ec1-497bfba7bac1'
                className='w-100% py-1 pl-10 '
              />
            </div>
            <div className='flex w-full justify-center text-2,5xl font-extrabold text-black-main md:text-5xl'>
              <h2 className=''>Registro</h2>
            </div>
          </div>
          <div className='font-sans mx-20 text-center text-sm font-medium text-black-main'>
            <p>Compartenos tus datos y te contactaremos pronto</p>
          </div>
          <div className='m-12 mt-8 '>
            <form className='flex flex-col items-center'>
              <div className='mb-2 flex w-full rounded-lg border bg-white shadow-sm'>
                <label htmlFor='status' className='sr-only'>
                  Rol
                </label>
                <div className='flex w-15% items-center justify-center'>
                  <a href=''>
                    <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0 text-gray-main' icon={faUser} size='lg' />
                  </a>
                </div>
                <select
                  id='status'
                  name='status'
                  className='h-12 w-85% rounded-lg bg-transparent px-1 text-sm font-normal text-black-main focus:border-2 focus:border-gray-100 focus:outline-none'
                >
                  <option>Soy administrador</option>
                  <option>Soy propietario</option>
                </select>
              </div>

              <div className='mb-2 flex w-full rounded-lg border bg-white shadow-sm'>
                <label className='sr-only'>Email address</label>
                <div className='flex w-15% items-center justify-center'>
                  <a href=''>
                    <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0 text-gray-main' icon={faHome} size='lg' />
                  </a>
                </div>
                <input
                  type='text'
                  className='h-12 w-85% rounded-lg bg-transparent px-1 text-sm font-normal placeholder-gray-main focus:border-2 focus:border-gray-100 focus:outline-none'
                  placeholder='Escribe el nombre de tu conjunto'
                />
              </div>
              <div className='mb-5 flex w-full rounded-lg border bg-white shadow-sm'>
                <label className='sr-only'>Email address</label>
                <div className='flex w-15% items-center justify-center'>
                  <a href=''>
                    <FontAwesomeIcon aria-hidden='true' className=' h-6 w-6 flex-shrink-0  text-gray-main' icon={faEnvelope} size='lg' />
                  </a>
                </div>
                <input
                  name='email'
                  type='email'
                  autoComplete='email'
                  className='h-12 w-85% rounded-lg bg-transparent px-1 text-sm font-normal placeholder-gray-main focus:border-2 focus:border-gray-100 focus:outline-none'
                  placeholder='Escribe tu email'
                />
              </div>
              <button
                type='button'
                className='bottom-1 right-1 h-10 w-60% items-center justify-center rounded-md border border-transparent bg-green-main text-sm font-extrabold text-white focus:bg-white focus:text-black-main focus:outline-none focus:ring-1 focus:ring-gray-main'
              >
                Enviar Registro
              </button>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
}
