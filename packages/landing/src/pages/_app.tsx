import '@styles/icons.css';
import config from 'react-reveal/globals';
import '@config/firebase';
import '@config/fontawesome';
import '@config/moment';
import Modal from 'react-modal';
import Head from 'next/head';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '@styles/toastyNotifications.css';
import '@fortawesome/fontawesome-svg-core/styles.css';
import React from 'react';
import { AppProps } from 'next/app';

config({ ssrFadeout: true });
Modal.setAppElement('#__next');
function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>DinDom.co</title>
        <meta name='viewport' content='initial-scale=1.0, width=device-width' />
        <link rel='apple-touch-icon' sizes='180x180' href='/apple-touch-icon.png' />
        <link rel='icon' type='image/png' sizes='32x32' href='/favicon-32x32.png' />
        <link rel='icon' type='image/png' sizes='16x16' href='/favicon-16x16.png' />
        <link rel='manifest' href='/site.webmanifest' />
        <link rel='mask-icon' href='/safari-pinned-tab.svg' color='#5bbad5' />
        <meta name='msapplication-TileColor' content='#da532c' />
        <meta name='theme-color' content='#ffffff'></meta>
        <meta name='description' content='Administra tu conjunto con más facilidad'></meta>
        <script
          dangerouslySetInnerHTML={{
            __html: `(function(h, o, t, j, a, r, ...args) {
            h.hj = h.hj || function(){ (h.hj.q = h.hj.q || []).push(args) };
            h._hjSettings = { hjid: 2872229, hjsv: 6 };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
          })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=')`,
          }}
        />
      </Head>
      <div className='min-h-screen bg-gray-100'>
        <div id='pdfDocumentModal'></div>
        <ToastContainer />

        <Component {...pageProps} />
      </div>
    </>
  );
}

export default MyApp;
