import { Card } from '@components/Blog/Card';
import { FirstPost } from '@components/Blog/FirstPost';
import { Footer } from '@components/Footer';
import { Header } from '@components/Header';
import { useBlog } from '@hooks/useBlog';
import React from 'react';

export default function blogdev() {
  const { posts } = useBlog();
  return (
    <>
      <Header />
      <div className='bg-white'>
        <div className='mx-auto mt-32'>
          <p className='mb-6 text-center text-5xl font-extrabold text-black'>Blog</p>
        </div>
        {posts[0] && <FirstPost post={posts[0]} />}
        <ul className='mx-auto flex w-80% flex-wrap justify-center align-middle'>
          {posts.map(post => (
            <Card key={post.title} post={post} />
          ))}
        </ul>
      </div>
      <Footer />
    </>
  );
}
