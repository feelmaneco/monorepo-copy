import { Footer } from '@components/Footer';
import { Header } from '@components/Header';
import { IBlog } from '@dindom/common/build/interfaces/blog/blog.public.interface';
import { findPostByUrl } from '@services/api/dindom-blog/findPostByUrl';
import { formatTime } from '@utils/formatTime';
import { GetStaticPaths, GetStaticProps } from 'next';
import Link from 'next/link';
import React from 'react';

type BlogPostPageProps = {
  post: IBlog;
};
export const BlogPostPage: React.FC<BlogPostPageProps> = ({ post }) => {
  return (
    <div className='min-h-screen'>
      <Header />

      <div
        className='mx-auto w-full bg-slate-600 py-32 px-20'
        style={{ backgroundImage: `url(${post.imageThubnailURL})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat' }}
      >
        <p className='textlg mt-2 font-extrabold text-white'>
          <Link href={`/blog`}>
            <a>Regresal al blog</a>
          </Link>
        </p>
        <h1 className='mt-2 text-5xl font-extrabold text-white'>{post.title}</h1>
        <p className='py-2 text-lg font-medium text-white'>{formatTime(post.createdAt)}</p>
        <p className='w-50% py-2 text-lg font-medium text-white'>{post.subdescription}</p>
      </div>
      <p className='font mx-16 my-8 text-lg font-medium text-black '>{post.description}</p>
      <div className='absolute bottom-0 left-0 right-0'>
        <Footer />
      </div>
    </div>
  );
};

export const getStaticPaths: GetStaticPaths<{ slug: string }> = async () => {
  return {
    paths: [], // indicates that no page needs be created at build time
    fallback: 'blocking', // indicates the type of fallback
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    if (typeof params.id !== 'string' || !params.id) {
      return {
        props: {},
        redirect: {
          destination: '/',
        },
      };
    }
    const response = await findPostByUrl(params.id);
    console.log('RESPONSE', response.data);
    const props: BlogPostPageProps = {
      post: response.data.data,
    };

    return {
      props,
      // redirect: {
      //   destination: "/conjuntos",
      // },
    };
  } catch (error) {
    return {
      redirect: {
        destination: '/conjuntos',
      },
      props: {},
    };
  }
};

export default BlogPostPage;
