import React from 'react';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
export const Header: React.FC = () => {
  return (
    <div className='fixed top-0 left-0 right-0 z-20 flex h-12 items-center bg-gradient-to-b from-white to-white/70 md:mr-9 md:justify-end md:pt-3'>
      <div className='flex-row-start ml-3 flex h-full w-20% items-center md:ml-10'>
        <img
          src='https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/landing_static%2Flogo.png?alt=media&token=f278ff45-b919-4c8b-9b68-674237d81b9f'
          alt=''
          className='flex-end'
        />
      </div>
      <div className='mr-4 flex h-full w-80% items-center justify-end md:hidden'>
        <Link href=''>
          <a>
            <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' color='#2B2B2B' icon={faBars} size='lg' />
          </a>
        </Link>
      </div>
      <div className='hidden w-80% items-center justify-end  font-bold md:flex'>
        <Link href='/quienes-somos'>
          <a key='' className='rounded-md p-2 px-6 text-center text-base text-black-main hover:bg-green-main hover:text-white'>
            Inicio
          </a>
        </Link>
        <a
          key=''
          href='que-ofrecemos'
          className='rounded-md p-2 px-6 text-center text-base text-black-main hover:bg-green-main hover:text-white'
        >
          ¿Qué hace DinDom?
        </a>
        <a key='' href='blog' className='rounded-md p-2 px-6 text-center text-base text-black-main hover:bg-green-main hover:text-white'>
          Blog
        </a>
        <a
          key=''
          href='/conjuntos'
          className='rounded-md p-2 px-6 text-center text-base text-black-main hover:bg-green-main hover:text-white'
        >
          Comenzar
        </a>
      </div>
    </div>
  );
};
