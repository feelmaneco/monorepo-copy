type NavigationItem = {
  name: string;
  href: string;
};

export const navigation: NavigationItem[] = [
  { name: 'Quienes somos', href: '/#quienes-somos' },
  { name: 'Qué ofrecemos', href: '/#que-ofrecemos' },
  { name: 'Registra tu conjunto', href: '/#inscribirse' },
  { name: 'Conjuntos', href: '/conjuntos' },
  // { name: "San Pedro plaza III", href: "/conjuntos/sanpedroplaza3" },
];
