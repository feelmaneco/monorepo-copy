import React from 'react';

type IconProps = {
  style?: React.CSSProperties;
  icon: string;
  className?: string;
};

export const Icon: React.FC<IconProps> = ({ style, icon, className }) => {
  return <i style={style} className={`${icon} ${className}`} />;
};
