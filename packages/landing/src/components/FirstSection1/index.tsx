import React from 'react';

export const FirstSection1: React.FC = () => {
  return (
    <div id='quienes-somos' className='max-w-7xl mx-auto mt-16 px-4 sm:mt-24 sm:px-6'>
      <div className='text-center'>
        <h1 className='text-4xl font-extrabold tracking-tight text-gray-900 sm:text-5xl md:text-6xl'>
          <span className='block'>Administra tu</span>
          <span className='text-green-500'>conjunto</span>
          <span className=''> con más facilidad</span>
        </h1>
        <p className='max-w-md md:max-w-3xl mx-auto mt-3 text-base text-gray-500 sm:text-lg md:mt-5 md:text-xl'>
          Ayudamos a los conjuntos los propietarios de propiedad horizontal a estar en contacto con su conjunto
        </p>
      </div>
    </div>
  );
};
