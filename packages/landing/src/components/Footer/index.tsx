import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { faFacebookF, faLinkedinIn, faInstagram } from '@fortawesome/free-brands-svg-icons';

export const Footer: React.FC = () => {
  return (
    <div className=' mb-14 flex flex-col font-bold md:mb-7 md:h-20 md:flex-row md:bg-green-main'>
      <div className='flex w-full md:w-66% '>
        <a href='' className='flex w-33% items-center justify-center text-center text-xxs text-gray-main md:w-25% md:text-sm md:text-white'>
          Términos y condiciones
        </a>
        <a
          href=''
          className='flex w-34% items-center justify-center text-center text-xxs text-gray-main md:w-25%  md:text-sm md:text-white'
        >
          Política de privacidad
        </a>
        <div className='flex h-5 w-33% items-center justify-around px-2 md:h-20 md:w-50% md:justify-center '>
          <a href=''>
            <FontAwesomeIcon
              aria-hidden='true'
              className='delay-50 h-6 w-6 flex-shrink-0 text-gray-main transition duration-200 ease-in-out hover:-translate-y-1 hover:scale-200 md:mx-3 md:w-33% md:text-white'
              icon={faFacebookF}
              size='sm'
            />
          </a>
          <a href=''>
            <FontAwesomeIcon
              aria-hidden='true'
              className='delay-50 h-6 w-6 flex-shrink-0 text-gray-main transition duration-200 ease-in-out hover:-translate-y-1 hover:scale-200 md:mx-3 md:w-33% md:text-white'
              icon={faLinkedinIn}
              size='sm'
            />
          </a>
          <a href=''>
            <FontAwesomeIcon
              aria-hidden='true'
              className=' delay-50 w-6 flex-shrink-0 text-gray-main transition duration-200 ease-in-out hover:-translate-y-1 hover:scale-200 md:mx-3 md:w-33% md:text-white'
              icon={faInstagram}
              size='sm'
            />
          </a>
        </div>
      </div>
      <div className='flex w-full justify-center md:h-20 md:w-33% '>
        <span className='flex h-5 items-center text-center  text-xxs text-gray-main md:h-full md:text-sm md:text-white'>© 2022 DinDom</span>
      </div>
    </div>
  );
};
