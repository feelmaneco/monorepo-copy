import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import React from 'react';

type LogoProps = {
  isBig?: true;
  color?: 'white' | 'green';
};

export const Logo: React.FC<LogoProps> = ({ isBig, color = 'green' }) => {
  return (
    <div className='mb-4 flex items-end justify-center'>
      <FontAwesomeIcon size={isBig ? '4x' : '2x'} className={`text-${color === 'green' ? 'green-500' : 'white'}`} icon={faHome} />
      <span className={`text-${color === 'green' ? 'green-500' : 'white'} ${isBig && 'text-4xl'} ml-1 font-extrabold`}>DinDom.co</span>
    </div>
  );
};
