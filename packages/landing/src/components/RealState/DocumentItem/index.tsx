import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFile } from '@fortawesome/free-solid-svg-icons';
import { IRealStateDocumentsEntity } from '@interfaces/documents';
type DocumentItemProps = {
  onClick: (document: IRealStateDocumentsEntity) => void;
  document: IRealStateDocumentsEntity;
};
export const DocumentItem: React.FC<DocumentItemProps> = ({ document, onClick }) => {
  return (
    <div
      onClick={() => {
        onClick(document);
      }}
      className='mx-1 my-2 flex h-40 w-96 cursor-pointer flex-col items-center justify-around rounded-md border-2 border-gray-200 bg-gray-50 p-3 duration-500 hover:scale-110 hover:border-green-200'
    >
      <div className='flex h-12 w-12 items-center justify-center rounded-md bg-green-500 text-white'>
        <FontAwesomeIcon icon={faFile} className='h-6 w-6' aria-hidden='true' />
      </div>
      <span className='font-semibold'>{document.name}</span>
    </div>
  );
};
