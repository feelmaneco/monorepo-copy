import React from 'react';
import { IRealStateBlogPost } from '@interfaces/postsBlog';
// import moment, { locale } from "moment";

// locale("es");

type PostProps = {
  post: IRealStateBlogPost;
};
export const Post: React.FC<PostProps> = ({ post }) => {
  return (
    <>
      <li className='mb-4 mt-4 w-fit overflow-hidden rounded-lg bg-white shadow-lg sm:flex'>
        <div className='mb-4 flex-shrink-0 sm:mb-0 sm:mr-4'>
          {post.thubnailPreviewImgUrl && (
            <img
              src={post.thubnailPreviewImgUrl}
              className='flex h-56 w-56 cursor-pointer flex-col items-center justify-center rounded-2xl shadow-md hover:border-gray-700'
              aria-hidden='true'
              alt={post.title}
            />
          )}
        </div>
        <div className='p-2'>
          {post.title && (
            <div className='flex justify-between'>
              <h3 className={`mb-4 text-gray-800 sm:text-2xl lg:text-3xl`}>{post.title}</h3>
            </div>
          )}
          <h3>Publicado por: Administración</h3>
          {/* <p className="ml-auto text-indigo-600">
            
            {moment(post.time.toDate()).locale("es-ES").fromNow()}
          </p> */}
        </div>
      </li>
    </>
  );
};
