import React from 'react';
import Image from 'next/image';

type AdministratorCardProps = {
  name: string;
  photoUrl: string;
};
const myLoader = ({ src }) => {
  return src;
};
export const AdministratorCard: React.FC<AdministratorCardProps> = ({ name, photoUrl }) => {
  return (
    <div className='h-40 w-96 rounded-md border-2 border-gray-200 bg-gray-50 p-3'>
      <h3 className='text-center font-bold'>Administrador</h3>
      <div className='flex items-center justify-around p-2'>
        <Image src={photoUrl} className='rounded-full' loader={myLoader} objectFit='cover' width='80px' height='80px' />
        <span className='font-semibold'>{name}</span>
      </div>
    </div>
  );
};
