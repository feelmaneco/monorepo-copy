import React from 'react';
import { useFormik } from 'formik';
import { useRouter } from 'next/router';
import { sendMessage } from '@services/sendMessage';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import Yup from '@utils/formValidation';
import classNames from 'classnames';

type FormProps = {
  text: string;
  from: string;
  realStateOwnerId: string;

  fromLastname: string;
};

export const GetInTouchContainer: React.FC = () => {
  const ProfileInfoSchema = Yup.object().shape({
    from: Yup.string().max(50).min(5),
    fromLastname: Yup.string().max(50).min(5),
    realStateOwnerId: Yup.string().max(50).min(5).required(),
    text: Yup.string().max(500).min(5).required(),
  });
  const router = useRouter();
  const formik = useFormik<FormProps>({
    initialValues: {
      from: '',
      fromLastname: '',

      realStateOwnerId: '',
      text: '',
    },
    onSubmit: values => {
      // realStateUrl: string;
      // realStateUrl;
      sendMessage({
        ...values,
        realStateUrl: Array.isArray(router.query.id) ? router.query.id.join('') : router.query.id,
      });
    },
    validationSchema: ProfileInfoSchema,
  });
  const { handleSubmit, values, handleChange, errors } = formik;
  return (
    <div className='overflow-hidden bg-gradient-to-r from-white via-gray-100 to-white px-4 py-16 sm:px-6 lg:px-8 lg:py-24'>
      <div className='max-w-xl relative mx-auto'>
        <div className='text-center'>
          <h2 className='text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl'>Contacte con administrador</h2>
        </div>
        <div className='mt-12'>
          <form onSubmit={handleSubmit} className='grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8'>
            <div>
              <label htmlFor='first-name' className='block text-sm font-medium text-gray-700'>
                Nombres
              </label>
              <div className='mt-1'>
                <input
                  type='text'
                  name='from'
                  onChange={handleChange}
                  value={values.from}
                  id='first-name'
                  autoComplete='given-name'
                  className='block w-full rounded-md border-gray-300 px-4 py-3 shadow-sm focus:border-indigo-500 focus:ring-indigo-500'
                />
                {errors.from && (
                  <div className='rounded m-2 flex items-center bg-red-400 p-2 text-white'>
                    <span>{errors.from}</span>
                    <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                  </div>
                )}
              </div>
            </div>
            <div>
              <label htmlFor='last-name' className='block text-sm font-medium text-gray-700'>
                Apellidos
              </label>
              <div className='mt-1'>
                <input
                  type='text'
                  name='fromLastname'
                  onChange={handleChange}
                  value={values.fromLastname}
                  id='last-name'
                  autoComplete='family-name'
                  className='block w-full rounded-md border-gray-300 px-4 py-3 shadow-sm focus:border-indigo-500 focus:ring-indigo-500'
                />
                {errors.fromLastname && (
                  <div className='rounded m-2 flex items-center bg-red-400 p-2 text-white'>
                    <span>{errors.fromLastname}</span>
                    <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                  </div>
                )}
              </div>
            </div>
            <div className='sm:col-span-2'>
              <label htmlFor='company' className='block text-sm font-medium text-gray-700'>
                Propietario de
              </label>
              <div className='mt-1'>
                <input
                  type='text'
                  name='realStateOwnerId'
                  onChange={handleChange}
                  value={values.realStateOwnerId}
                  id='company'
                  autoComplete='organization'
                  className='block w-full rounded-md border-gray-300 px-4 py-3 shadow-sm focus:border-indigo-500 focus:ring-indigo-500'
                />
                {errors.realStateOwnerId && (
                  <div className='rounded m-2 flex items-center bg-red-400 p-2 text-white'>
                    <span>{errors.realStateOwnerId}</span>
                    <FontAwesomeIcon aria-hidden='true' className='h-6 w-6 flex-shrink-0' icon={faInfoCircle} size='2x' />
                  </div>
                )}
              </div>
            </div>

            <div className='sm:col-span-2'>
              <label htmlFor='message' className='block text-sm font-medium text-gray-700'>
                Mensaje
              </label>
              <div className='mt-1'>
                <textarea
                  id='message'
                  name='text'
                  value={values.text}
                  onChange={handleChange}
                  rows={4}
                  className='block w-full rounded-md border border-gray-300 px-4 py-3 shadow-sm focus:border-green-500 focus:ring-green-500'
                  defaultValue={''}
                />
              </div>
            </div>
            <div className='sm:col-span-2'>
              <button
                type='submit'
                className={classNames(
                  {
                    ' cursor-pointer bg-green-600 hover:bg-green-700': values.from !== '' || values.realStateOwnerId !== '',
                    'cursor-default bg-gray-400': values.from === '' && values.realStateOwnerId === '',
                  },
                  'inline-flex w-full items-center justify-center rounded-md border border-transparent px-6 py-3 text-base font-medium text-white shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2',
                )}
              >
                Contactar
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
