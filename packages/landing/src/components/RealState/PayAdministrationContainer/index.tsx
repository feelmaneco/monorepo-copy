import Link from 'next/link';
import React from 'react';

type PayAdministrationContainerProps = {
  url: string;
};

export const PayAdministrationContainer: React.FC<PayAdministrationContainerProps> = ({ url }) => {
  return (
    <div className='h-60'>
      <h3 className='text-center text-lg font-semibold'>Pagar administracion</h3>
      <div className='mt-2 flex flex-row items-center justify-around'>
        <div className='h-40'>
          <img className='h-24' src='https://unoxuno.com.co/wp-content/uploads/2019/10/BotonPSE-300x225.png' />
          <Link href={url}>
            <div className='w-full cursor-pointer rounded-lg bg-green-500 p-2 text-center font-bold text-white hover:bg-green-700'>
              <a>Pagar</a>
            </div>
          </Link>
        </div>
        {/* <div className=" h-40">
          <img
            className="h-24"
            src="https://unoxuno.com.co/wp-content/uploads/2019/10/BotonPSE-300x225.png"
          />
          <button className="p-2 bg-green-500 rounded-lg text-white w-full hover:bg-green-700">
            Pagar
          </button>
        </div> */}
      </div>
    </div>
  );
};
