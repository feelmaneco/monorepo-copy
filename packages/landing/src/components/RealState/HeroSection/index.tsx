import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

type HeroDescriptionProps = {
  name: string;
  photoUrl: string;
  address: string;
};

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export const HeroDescription: React.FC<HeroDescriptionProps> = ({ address, name, photoUrl }) => {
  const router = useRouter();
  const getIsCurrent = (name?: string): boolean => {
    if (!router.query.section && !name) {
      return true;
    }
    return router.query.section === name;
  };
  const tabs = [
    {
      name: 'Descripcion',
      href: `/conjuntos/${router.query.id}`,
      current: getIsCurrent(),
    },
    {
      name: 'Blog/Noticias',
      href: `/conjuntos/${router.query.id}/blog`,
      current: getIsCurrent('blog'),
    },
    // {
    //   name: "Servicios",
    //   href: `/conjuntos/${router.query.id}/servicios`,
    //   current: getIsCurrent("servicios"),
    // },
    /*
    style={{
              background: `url(${Background})`,
            }}
    
    */
    {
      name: 'Galeria',
      href: `/conjuntos/${router.query.id}/galeria`,
      current: getIsCurrent('galeria'),
    },
  ];
  return (
    <>
      <section
        style={{
          background: `url(${photoUrl})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: '100%',
        }}
        className='mt-24 flex h-96 w-screen flex-col items-center justify-center text-white'
      >
        <div className='flex h-full w-full flex-col items-center justify-center bg-gray-700/50'>
          <h1 className='text-4xl font-extrabold'>{name}</h1>
          <h3>{address}</h3>
        </div>
      </section>
      <div className='block'>
        <div className='border-b border-gray-200'>
          <nav className='-mb-px flex space-x-8' aria-label='Tabs'>
            {tabs.map(tab => (
              <Link href={tab.href} key={tab.name}>
                <a
                  key={tab.name}
                  className={classNames(
                    tab.current
                      ? 'border-indigo-500 text-indigo-600'
                      : 'border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700',
                    'whitespace-nowrap border-b-2 px-1 py-4 text-sm font-medium',
                  )}
                  aria-current={tab.current ? 'page' : undefined}
                >
                  {tab.name}
                </a>
              </Link>
            ))}
          </nav>
        </div>
      </div>
    </>
  );
};
