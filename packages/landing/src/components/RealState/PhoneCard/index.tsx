import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
type PhoneCardProps = {
  phone: string;
};
export const PhoneCard: React.FC<PhoneCardProps> = ({ phone }) => {
  return (
    <div className='flex h-40 w-96 flex-col items-center justify-around rounded-md border-2 border-gray-200 bg-gray-50 p-3'>
      <div className='flex h-12 w-12 items-center justify-center rounded-md bg-green-500 text-white'>
        <FontAwesomeIcon icon={faPhone} className='h-6 w-6' aria-hidden='true' />
      </div>
      <span className='font-semibold'>{phone}</span>
    </div>
  );
};
