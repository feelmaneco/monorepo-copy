import React, { useState } from 'react';
import { DocumentItem } from '@components/RealState/DocumentItem';
import Modal from 'react-modal';
import dynamic from 'next/dynamic';
import { IRealStateDocumentsEntity } from '@interfaces/documents';

Modal.setAppElement('#pdfDocumentModal');
const PDFViewer = dynamic(import('../../PdfViewer'), { ssr: false });

type DocumentsListProps = {
  documents: IRealStateDocumentsEntity[];
};
export const DocumentsList: React.FC<DocumentsListProps> = ({ documents }) => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [activeDocument, setActiveDocument] = useState<IRealStateDocumentsEntity | null>();
  const onDocumentClickHandler = (document: IRealStateDocumentsEntity) => {
    setActiveDocument(document);
    toggleModal();
  };
  const toggleModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <>
      {activeDocument && (
        <Modal
          isOpen={isModalOpen}
          onRequestClose={() => {
            setIsModalOpen(!isModalOpen);
          }}
          className=''
          contentLabel={activeDocument.name}
        >
          <PDFViewer url={activeDocument.url} onClose={toggleModal} />
        </Modal>
      )}
      <section className='my-3 flex flex-col items-center justify-center'>
        <h3 className='text-xl font-semibold'>Documentos</h3>
        {documents.length > 0 && (
          <div className='mt-3 flex w-full flex-wrap justify-around'>
            {documents.map((document, index) => (
              <DocumentItem key={index} document={document} onClick={onDocumentClickHandler} />
            ))}
          </div>
        )}
      </section>
    </>
  );
};
