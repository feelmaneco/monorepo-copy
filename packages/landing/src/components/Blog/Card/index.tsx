import { IBlog } from '@dindom/common/build/interfaces/blog/blog.public.interface';
import { formatTime } from '@utils/formatTime';
import Link from 'next/link';
import React from 'react';

type CardProps = {
  post: IBlog;
};
export const Card: React.FC<CardProps> = ({ post: { title, createdAt, subDescription, imageThumbnailURL, publicURL } }) => {
  return (
    <li key={title} className='my-8 mx-8'>
      <div className=' w-80 rounded-lg'>
        <div
          className='h-44 w-80 rounded-lg'
          style={{ backgroundImage: `url(${imageThumbnailURL})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat' }}
        />
      </div>
      <div className='flex'>
        <p className='pointer-events-none mt-2 block truncate text-lg font-bold  text-gray-900'>{title}</p>
        <p className='pointer-events-none ml-auto mt-2 block truncate text-sm font-bold text-gray-900'>{formatTime(createdAt)}</p>
      </div>
      <p className='pointer-events-none block h-12 w-80 overflow-hidden text-base font-semibold text-black'>{subDescription}</p>
      <Link href={`blog/${publicURL}`}>
        <a className='mt-4 rounded-lg border-2 border-green-400 px-6 py-1 text-sm text-green-400'>Leer más</a>
      </Link>
    </li>
  );
};
