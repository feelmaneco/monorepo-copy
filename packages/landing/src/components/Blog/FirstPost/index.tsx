import { IBlog } from '@dindom/common/build/interfaces/blog/blog.public.interface';
import { formatTime } from '@utils/formatTime';
import Link from 'next/link';
import React from 'react';

type FirstPostProps = {
  post: IBlog;
};
export const FirstPost: React.FC<FirstPostProps> = ({ post: { subDescription, imageThumbnailURL, title, publicURL, createdAt } }) => {
  return (
    <div
      className='mx-auto w-full bg-slate-600 py-32 px-20'
      style={{ backgroundImage: `url(${imageThumbnailURL})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat' }}
    >
      <p className='mt-28 text-5xl font-extrabold text-white'>{title}</p>
      <p className='py-2 text-lg font-medium text-white'>{formatTime(createdAt)}</p>
      <p className='w-50% py-2 text-lg font-medium text-white'>{subDescription}</p>
      <Link href={`blog/${publicURL}`}>
        <a className='my-8 rounded-lg bg-green-400 px-10 py-3 font-bold text-white'>Leer más</a>
      </Link>
    </div>
  );
};
