import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faVideo, faEnvelope, faDollarSign, faBell } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
const features = [
  {
    name: 'Siempre en contacto',
    description: 'Comunícate directamente con la administración de tu conjunto.',
    icon: faEnvelope,
  },
  {
    name: 'Pague su administración por medio de una suscripción',
    description: 'No te preocupes de que no has pagado tu administración en la fecha necesaria, nosotros nos preocupamos por ti.',
    icon: faDollarSign,
  },
  {
    name: 'Pendiente de notificaciones',
    description: 'Llegaron nuevas facturas, un correo o una novedad en el conjunto? Te lo notificamos!',
    icon: faBell,
  },
  {
    name: 'Asambleas virtuales',
    description: 'Una nueva experiencia para asambleas virtuales.',
    icon: faVideo,
  },
];

export const Features: React.FC = () => {
  return (
    <div id='que-ofrecemos' className='bg-white py-12'>
      <div className='max-w-xl lg:max-w-7xl mx-auto px-4 sm:px-6 lg:px-8'>
        <h2 className='sr-only'>A better way to send money.</h2>
        <dl className='space-y-10 lg:grid lg:grid-cols-3 lg:gap-8 lg:space-y-0'>
          {features.map(feature => (
            <div key={feature.name}>
              <dt>
                <div className='flex h-12 w-12 items-center justify-center rounded-md bg-green-500 text-white'>
                  <FontAwesomeIcon icon={feature.icon} className='h-6 w-6' aria-hidden='true' />
                </div>
                <p className='mt-5 text-lg font-medium leading-6 text-gray-900'>{feature.name}</p>
              </dt>
              <dd className='mt-2 text-base text-gray-500'>{feature.description}</dd>
            </div>
          ))}
        </dl>
      </div>
    </div>
  );
};
