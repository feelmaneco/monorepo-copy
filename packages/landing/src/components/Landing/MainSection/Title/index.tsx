import React from 'react';
import { useSpring, animated } from 'react-spring';

export const MainSectionTitle: React.FC = () => {
  const props = useSpring({
    to: { opacity: 1 },
    from: { opacity: 0 },
    delay: 400,
  });
  return (
    <animated.div style={props} id='quienes-somos' className='mt-16 justify-self-start md:mt-8'>
      <div className='text-center'>
        <h1 className='text-2xl font-extrabold tracking-tight text-gray-900 sm:text-3xl md:text-4xl lg:text-5xl 2xl:text-6xl'>
          <span className='block'>Administra tu</span>
          <span className='text-green-500'>conjunto</span>
          <span className=''> con más facilidad</span>
        </h1>
        <p className='max-w-md md:max-w-3xl mx-auto mt-3 text-xs text-gray-500 sm:text-sm md:text-base'>
          Ayudamos a los conjuntos los propietarios de propiedad horizontal a estar en contacto con su conjunto
        </p>
      </div>
    </animated.div>
  );
};
