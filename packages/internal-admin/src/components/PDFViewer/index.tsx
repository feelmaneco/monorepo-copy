import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';

type PdfViewerProps = {
  url: string;
  onClose: () => void;
};
pdfjs.GlobalWorkerOptions.workerSrc = '/pdf.worker.min.js';

const PdfViewer: React.FC<PdfViewerProps> = ({ url, onClose }) => {
  const [numPages, setNumPages] = useState<number | null>(null);
  const [pageNumber, setPageNumber] = useState(1);
  const onDocumentLoadSuccess = ({ numPages }: { numPages: number }) => {
    setNumPages(numPages);
  };
  return (
    <div className='bg-white'>
      <div className='flex justify-end'>
        <FontAwesomeIcon onClick={onClose} className='cursor-pointer' icon={{ prefix: 'fas', iconName: 'times-circle' }} size={'2x'} />
      </div>
      <div className='flex flex-col items-center'>
        <Document file={url} onLoadSuccess={onDocumentLoadSuccess}>
          <Page pageNumber={pageNumber} />
        </Document>
        <p>
          Page {pageNumber} of {numPages}
        </p>
        {pageNumber !== 1 && (
          <button onClick={() => setPageNumber(pageNumber - 1)}>
            <FontAwesomeIcon icon={{ prefix: 'fas', iconName: 'arrow-alt-circle-left' }} size={'2x'} />
          </button>
        )}
        {pageNumber !== numPages && (
          <button onClick={() => setPageNumber(pageNumber + 1)}>
            <FontAwesomeIcon icon={{ prefix: 'fas', iconName: 'arrow-alt-circle-right' }} size={'2x'} />
          </button>
        )}
      </div>
    </div>
  );
};
export default PdfViewer;
