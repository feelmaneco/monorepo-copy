import { getAuth, signOut as fireSignOut } from 'firebase/auth';

export const signOut = async (): Promise<boolean> => {
  try {
    const auth = getAuth();
    await fireSignOut(auth);
    return true;
  } catch (e) {
    console.log('Errror login', e);
    return false;
  }
};
