import React, { Fragment } from 'react';
// import { navigation } from "./navigation";
import { classNames } from '../../../../utils/css/classNames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Dialog, Transition } from '@headlessui/react';
import { XIcon } from '@heroicons/react/outline';
import { navigation, NavigationItem } from '../../layouts/navigation';
import { Logo } from '../../../../components/Logo';
import { Link, useLocation } from 'react-router-dom';
type SideBarProps = {
  show: boolean;
  toggleSideBar: () => void;
  navigation: NavigationItem[];
};

export const SideBar: React.FC<SideBarProps> = ({ show, toggleSideBar }) => {
  const location = useLocation();
  return (
    <>
      <Transition.Root show={show} as={Fragment}>
        <Dialog as='div' className='fixed inset-0 z-40 flex md:hidden' onClose={toggleSideBar}>
          <Transition.Child
            as={Fragment}
            enter='transition-opacity ease-linear duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='transition-opacity ease-linear duration-300'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <Dialog.Overlay className='fixed inset-0 bg-gray-600 bg-opacity-75' />
          </Transition.Child>
          <Transition.Child
            as={Fragment}
            enter='transition ease-in-out duration-300 transform'
            enterFrom='-translate-x-full'
            enterTo='translate-x-0'
            leave='transition ease-in-out duration-300 transform'
            leaveFrom='translate-x-0'
            leaveTo='-translate-x-full'
          >
            <div className='relative flex w-full max-w-xs flex-1 flex-col bg-indigo-700 pb-4 pt-5'>
              <Transition.Child
                as={Fragment}
                enter='ease-in-out duration-300'
                enterFrom='opacity-0'
                enterTo='opacity-100'
                leave='ease-in-out duration-300'
                leaveFrom='opacity-100'
                leaveTo='opacity-0'
              >
                <div className='absolute right-0 top-0 -mr-12 pt-2'>
                  <button
                    type='button'
                    className='ml-1 flex h-10 w-10 items-center justify-center rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white'
                    onClick={toggleSideBar}
                  >
                    <span className='sr-only'>Close sidebar</span>
                    <XIcon className='h-6 w-6 text-white' aria-hidden='true' />
                  </button>
                </div>
              </Transition.Child>
              <div className='flex flex-shrink-0 items-center px-4'>
                <Logo />
              </div>
              <div className='mt-5 h-0 flex-1 overflow-y-auto'>
                <nav className='space-y-1 px-2'>
                  {navigation.map(item => (
                    <Link
                      key={item.name}
                      to={item.path}
                      className={classNames(
                        location.pathname.includes(item.path) ? 'bg-indigo-800 text-white' : 'text-indigo-100 hover:bg-indigo-600',
                        'group flex items-center rounded-md px-2 py-2 text-base font-medium',
                      )}
                    >
                      {/* <item.icon
                        className="mr-4 flex-shrink-0 h-6 w-6 text-indigo-300"
                        aria-hidden="true"
                      /> */}
                      {item.name}
                    </Link>
                  ))}
                </nav>
              </div>
            </div>
          </Transition.Child>
          <div className='w-14 flex-shrink-0' aria-hidden='true'>
            {/* Dummy element to force sidebar to shrink to fit close icon */}
          </div>
        </Dialog>
      </Transition.Root>

      {/* Static sidebar for desktop */}
      <div className='hidden md:fixed md:inset-y-0 md:flex md:w-64 md:flex-col'>
        {/* Sidebar component, swap this element with another sidebar if you like */}
        <div className='flex flex-grow flex-col overflow-y-auto bg-green-700 pt-5'>
          <div className='flex flex-shrink-0 items-center px-4'>
            <Logo color='white' />
          </div>
          <div className='mt-5 flex flex-1 flex-col'>
            <nav className='flex-1 space-y-1 px-2 pb-4'>
              {navigation.map(item => (
                <Link
                  key={item.name}
                  to={item.path}
                  className={classNames(
                    location.pathname.includes(item.path) ? 'bg-green-800 text-white' : 'text-green-100 hover:bg-green-600',
                    'group flex items-center rounded-md px-2 py-2 text-base font-medium ',
                  )}
                >
                  <FontAwesomeIcon aria-hidden='true' className='mr-3 h-6 w-6 flex-shrink-0 text-green-300' icon={item.icon} size='2x' />
                  {/* <item.icon
                   
                    
                  /> */}
                  <span className='self-end'>{item.name}</span>
                </Link>
              ))}
            </nav>
          </div>
        </div>
      </div>
    </>
  );
};
