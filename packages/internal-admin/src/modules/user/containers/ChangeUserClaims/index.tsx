import React, { useState } from 'react';
import { USER_ROLE } from '../../../../interfaces/Users.interface';
import { changeCustomClaims } from '../../../../services/firebase/users/changeCustomClaims';

export const ChangeUserClaims: React.FC = () => {
  const [formData, setFormData] = useState<{ uid: string; role: USER_ROLE }>({
    role: USER_ROLE.USER,
    uid: '',
  });
  const onApplyClickHandler = () => {
    changeCustomClaims(formData.uid, { role: formData.role });
  };
  return (
    <div>
      <h3 className='text-2xl'>Change User claims</h3>
      <div className='flex items-center'>
        <div className='mr-3'>
          <label htmlFor='uidUser' className='block text-sm font-medium text-gray-700'>
            UID user
          </label>
          <div className='mt-1'>
            <input
              type='text'
              name='uidUser'
              id='email'
              value={formData.uid}
              onChange={e => {
                setFormData(prev => ({
                  ...prev,
                  uid: e.target.value,
                }));
              }}
              className='block w-full rounded-md border-gray-300 p-2 text-xl shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
              placeholder='Z60kpka26j6Fr5ivuyj64qbNLkNR'
            />
          </div>
        </div>
        <div className='mr-2'>
          <label htmlFor='location' className='block text-sm font-medium text-gray-700'>
            UserRole
          </label>
          <select
            id='location'
            name='location'
            className='mt-1  block w-full rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm'
            defaultValue={formData.role}
            onChange={e => {
              setFormData(prev => ({
                ...prev,
                role: e.target.value as USER_ROLE,
              }));
            }}
          >
            <option value={USER_ROLE.ADMIN}>{USER_ROLE.ADMIN}</option>
            <option value={USER_ROLE.REAL_STATE_ADMIN}>{USER_ROLE.REAL_STATE_ADMIN}</option>
            <option value={USER_ROLE.USER}>{USER_ROLE.USER}</option>
            <option value={USER_ROLE.GUARDIAN}>{USER_ROLE.GUARDIAN}</option>
          </select>
        </div>
        <button
          type='button'
          onClick={onApplyClickHandler}
          className='inline-flex items-center self-end rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
        >
          Apply
        </button>
      </div>
    </div>
  );
};
