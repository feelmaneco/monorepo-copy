import React, { useState } from 'react';
import { USER_ROLE } from '../../../../interfaces/Users.interface';
import { changeCustomClaims } from '../../../../services/firebase/users/changeCustomClaims';

export const ChangeUserRealStateCalims: React.FC = () => {
  const [formData, setFormData] = useState<{ uid: string; id: string }>({
    id: '',
    uid: '',
  });
  const onApplyClickHandler = () => {
    changeCustomClaims(formData.uid, {
      realStateID: formData.id,
    });
  };
  return (
    <div>
      <h3 className='text-2xl'>Change User RealStateID</h3>
      <div className='flex items-center'>
        <div className='mr-3'>
          <label htmlFor='uidUser' className='block text-sm font-medium text-gray-700'>
            UID user
          </label>
          <div className='mt-1'>
            <input
              type='text'
              value={formData.uid}
              onChange={e => {
                setFormData(prev => ({
                  ...prev,
                  uid: e.target.value,
                }));
              }}
              className='block w-full rounded-md border-gray-300 p-2 text-xl shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
              placeholder='Z60kpka26j6Fr5ivuyj64qbNLkNR'
            />
          </div>
        </div>
        <div className='mr-2'>
          <label htmlFor='location' className='block text-sm font-medium text-gray-700'>
            RealStateID
          </label>
          <input
            onChange={e =>
              setFormData(prev => ({
                ...prev,
                id: e.target.value as USER_ROLE,
              }))
            }
            value={formData.id}
            className='block w-full rounded-md border-gray-300 p-2 text-xl shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
            placeholder='Z60kpka26j6Fr5ivuyj64qbNLkNR'
          />
        </div>
        <button
          type='button'
          onClick={onApplyClickHandler}
          className='inline-flex items-center self-end rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2'
        >
          Apply
        </button>
      </div>
    </div>
  );
};
