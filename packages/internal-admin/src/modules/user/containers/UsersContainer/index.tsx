import React, { useState } from 'react';
import { Button } from '../../../../components/Button';
import { createUser } from '../../../../services/firebase/users/createUser';
import { ChangeUserClaims } from '../ChangeUserClaims';
import { ChangeUserRealStateCalims } from '../ChangeUserClaims/changeRealStateID';
import { UsersTable } from '../UsersTable';

export const UsersContainer: React.FC = () => {
  const [createNewUserData, setCreateNewUserData] = useState<{
    name: string;
    email: string;
    password: string;
  }>({
    email: '',
    name: '',
    password: '',
  });
  const createUserHandler = () => {
    createUser(createNewUserData.name, createNewUserData.email, createNewUserData.password);
  };
  return (
    <>
      <div className='flex flex-col'>
        <input
          placeholder='name'
          value={createNewUserData.name}
          onChange={event => {
            setCreateNewUserData(prev => ({
              ...prev,
              name: event.target.value,
            }));
          }}
        />
        <input
          placeholder='email'
          onChange={event => {
            setCreateNewUserData(prev => ({
              ...prev,
              email: event.target.value,
            }));
          }}
        />
        <input
          placeholder='password'
          onChange={event => {
            setCreateNewUserData(prev => ({
              ...prev,
              password: event.target.value,
            }));
          }}
          type='password'
        />
        <Button data={{ text: 'create user', center: false, type: 'default' }} onClick={createUserHandler}>
          create user
        </Button>
      </div>
      <ChangeUserClaims />
      <ChangeUserRealStateCalims />
      <UsersTable />
    </>
  );
};
