import React, { useEffect, useState } from 'react';
import { GetUserResponse } from '../../../../interfaces/HttpResponses.interface';
import { getUsers } from '../../../../services/firebase/users/getUsers';
import { UsersTableItem } from '../../components/UserTableItem';

export const UsersTable: React.FC = () => {
  const [users, setUsers] = useState<GetUserResponse[]>([]);
  useEffect(() => {
    const getUsersFunc = async () => {
      console.log('running');
      try {
        const response = await getUsers();
        if (response.error) {
          throw response.message;
        }
        console.log('Response', response.data);
        setUsers(response.data);
      } catch (error) {
        console.log('error geting users', error);
      }
    };
    getUsersFunc();
    return () => {
      setUsers([]);
    };
  }, []);
  return (
    <div>
      Users Table
      <div className='overflow-hidden bg-white shadow sm:rounded-md'>
        <ul className='divide-y divide-gray-200'>
          {console.log('USSSER', users)}
          {users.map((user, index) => (
            <UsersTableItem user={user} key={index} />
          ))}
        </ul>
      </div>
    </div>
  );
};
