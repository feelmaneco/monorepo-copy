import { Listbox, Transition } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/outline';
import { useFormik } from 'formik';
import React, { Fragment, useEffect, useState } from 'react';
import { GetUserResponse } from '../../../../../interfaces/HttpResponses.interface';
import { createRealState } from '../../../../../services/firebase/realState/createRealState';
import { getRealStateAdmins } from '../../../../../services/firebase/users/getRealStateAdmins';

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ');
}
// address: string;
//   realStateName: string;
//   administrator: {
//     name: string;
//     phoneNumber: string;
//     photoUrl: string;
//     uid: string;
//     email: string;
//
type createRealStateFormProps = {
  address: string;
  realStateName: string;
  administratorPhone: string;
};
export const RealStateAddForm: React.FC = () => {
  const [users, setUsers] = useState<GetUserResponse[]>([]);
  const [selected, setSelected] = useState<GetUserResponse>();
  const { values, handleChange, handleSubmit } = useFormik<createRealStateFormProps>({
    initialValues: {
      address: '',
      administratorPhone: '',
      realStateName: '',
    },
    onSubmit: async values => {
      if (selected) {
        const resposne = await createRealState({
          address: values.address,
          administrator: {
            email: selected.email,
            name: selected.name,
            phoneNumber: values.administratorPhone,
            photoUrl: selected.photoUrl,
            uid: selected.id,
          },
          realStateName: values.realStateName,
        });
        console.log('values', resposne);
      }
    },
  });

  useEffect(() => {
    if (!selected && users.length > 0) {
      setSelected(users[0]);
    }
    console.log('SELECTED', selected);
  }, [users, selected]);
  useEffect(() => {
    const reqFunc = async () => {
      const response = await getRealStateAdmins();
      if (response.error === false) {
        setUsers(response.data);
      }
      console.log('request', response);
    };
    reqFunc();
  }, []);
  return (
    <div className='bg-white shadow sm:rounded-lg'>
      <div className='px-4 py-5 sm:p-6'>
        <form onSubmit={handleSubmit}>
          <h3 className='text-lg font-medium leading-6 text-gray-900'>Create Real State</h3>
          <div className='mt-2 max-w-xl text-sm text-gray-500'>
            <div>
              {selected && (
                <Listbox value={selected} onChange={setSelected}>
                  {({ open }) => (
                    <>
                      <Listbox.Label className='block text-sm font-medium text-gray-700'>Assigned to</Listbox.Label>
                      <div className='relative mt-1'>
                        <Listbox.Button className='relative w-full cursor-default rounded-md border border-gray-300 bg-white py-2 pl-3 pr-10 text-left shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-1 focus:ring-indigo-500 sm:text-sm'>
                          <span className='block truncate'>{selected.name}</span>
                          <span className='pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2'>
                            <SelectorIcon className='h-5 w-5 text-gray-400' aria-hidden='true' />
                          </span>
                        </Listbox.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          leave='transition ease-in duration-100'
                          leaveFrom='opacity-100'
                          leaveTo='opacity-0'
                        >
                          <Listbox.Options className='absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm'>
                            {users.map(user => (
                              <Listbox.Option
                                key={user.id}
                                className={({ active }) =>
                                  classNames(
                                    active ? 'bg-indigo-600 text-white' : 'text-gray-900',
                                    'relative cursor-default select-none py-2 pl-3 pr-9',
                                  )
                                }
                                value={user}
                              >
                                {({ selected, active }) => (
                                  <>
                                    <span className={classNames(selected ? 'font-semibold' : 'font-normal', 'block truncate')}>
                                      {user.name}
                                    </span>

                                    {selected ? (
                                      <span
                                        className={classNames(
                                          active ? 'text-white' : 'text-indigo-600',
                                          'absolute inset-y-0 right-0 flex items-center pr-4',
                                        )}
                                      >
                                        <CheckIcon className='h-5 w-5' aria-hidden='true' />
                                      </span>
                                    ) : null}
                                  </>
                                )}
                              </Listbox.Option>
                            ))}
                          </Listbox.Options>
                        </Transition>
                      </div>
                    </>
                  )}
                </Listbox>
              )}
            </div>
            <div>
              <h4 className='mt-10 text-lg font-medium leading-6 text-gray-900'>Main info</h4>

              <div>
                <label htmlFor='address' className='block text-sm font-medium text-gray-700'>
                  RealState Address
                </label>
                <div className='mt-1'>
                  <input
                    type='text'
                    name='address'
                    id='address'
                    value={values.address}
                    onChange={handleChange}
                    className='block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
                    placeholder=''
                  />
                </div>
              </div>
              <div>
                <label htmlFor='administratorPhone' className='block text-sm font-medium text-gray-700'>
                  realStateName
                </label>
                <div className='mt-1'>
                  <input
                    type='text'
                    name='realStateName'
                    id='realStateName'
                    value={values.realStateName}
                    onChange={handleChange}
                    className='block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
                    placeholder=''
                  />
                </div>
              </div>
              <div>
                <label htmlFor='administratorPhone' className='block text-sm font-medium text-gray-700'>
                  Administrator Phone
                </label>
                <div className='mt-1'>
                  <input
                    type='text'
                    name='administratorPhone'
                    id='administratorPhone'
                    value={values.administratorPhone}
                    onChange={handleChange}
                    className='block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm'
                    placeholder=''
                  />
                </div>
              </div>
            </div>
          </div>
          <div className='mt-5'>
            <button
              type='submit'
              className='inline-flex items-center justify-center rounded-md border border-transparent bg-green-100 px-4 py-2 font-medium text-green-700 hover:bg-green-200 focus:outline-none focus:ring-2 focus:ring-green-500 focus:ring-offset-2 sm:text-sm'
            >
              Create Realstate
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
