import React from 'react';
import { RealStateMainContainer } from '../../containers/RealState/RealStateMainContainer';

export const RealStatePage: React.FC = () => {
  return (
    <>
      <RealStateMainContainer />
    </>
  );
};
