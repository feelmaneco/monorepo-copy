import React from 'react';
import { BrowserRouter, Routes as ReactRoutes, Route } from 'react-router-dom';
import { UserLayout } from '../layouts';
import { UserDashBoardPage } from '../pages/dashboard';
import { RealStatePage } from '../pages/realstate';
import { UsersPage } from '../pages/users';

export const Routes: React.FC = () => {
  return (
    <BrowserRouter>
      <ReactRoutes>
        <Route path='/' element={<UserLayout />}>
          <Route path='/dashboard' element={<UserDashBoardPage />} />
          <Route path='users' element={<UsersPage />} />
          <Route path='realstate' element={<RealStatePage />} />
        </Route>
        {/* <Route path="/dashboard">
          <UserLayout>
            <UserDashBoardPage />
          </UserLayout>
        </Route>
        <Route path="/users">
          <UserLayout>
            <UsersPage />
          </UserLayout>
        </Route>
        <Route path="/realstate">
          <UserLayout>
            <RealStatePage />
          </UserLayout>
        </Route> */}
      </ReactRoutes>
      {/* <Route path="*" render={() => <Redirect to="/dashboard" />} /> */}
    </BrowserRouter>
  );
};
