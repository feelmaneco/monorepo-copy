import React, { useState, useEffect } from 'react';
import { useLocation, Outlet } from 'react-router-dom';
import { SideBar } from '../containers/SideBar';
// import { classNames } from "../../../../utils/css/classNames";

import { TopBar } from '../containers/TopBar';
import { navigation } from './navigation';

export const UserLayout: React.FC = () => {
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const location = useLocation();
  const [title, setTitle] = useState<string>('');
  useEffect(() => {
    setTitle(navigation.find(index => index.path === location.pathname)?.name || '');
  }, [location]);

  const toggleSideBar = () => {
    setSidebarOpen(!sidebarOpen);
  };
  return (
    <>
      <div>
        <SideBar navigation={navigation} show={sidebarOpen} toggleSideBar={toggleSideBar} />
        <div className='flex flex-1 flex-col md:pl-64'>
          <TopBar toggleSideBar={toggleSideBar} />
          <main>
            <div className='py-6'>
              <div className='mx-auto max-w-7xl px-4 sm:px-6 md:px-8'>
                <h1 className='text-2xl font-semibold text-gray-900'>{title}</h1>
              </div>
              <div className='mx-auto max-w-7xl px-4 sm:px-6 md:px-8'>
                <Outlet />
              </div>
            </div>
          </main>
        </div>
      </div>
    </>
  );
};
