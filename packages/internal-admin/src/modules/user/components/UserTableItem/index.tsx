import React from 'react';
import { CheckCircleIcon, ChevronRightIcon, MailIcon, HashtagIcon } from '@heroicons/react/solid';
import { GetUserResponse } from '../../../../interfaces/HttpResponses.interface';

type UsersTableItemProps = {
  user: GetUserResponse;
};

export const UsersTableItem: React.FC<UsersTableItemProps> = ({ user }) => {
  return (
    <li key={user.id}>
      <div className='block hover:bg-gray-50'>
        <div className='flex items-center px-4 py-4 sm:px-6'>
          <div className='flex min-w-0 flex-1 items-center'>
            <div className='flex-shrink-0'>
              <img
                className='h-12 w-12 rounded-full'
                src={
                  user.photoUrl ||
                  'https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/images.png?alt=media&token=67241575-57e9-41fc-85e8-00db6fb6b842'
                }
                alt=''
              />
            </div>
            <div className='min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4'>
              <div>
                <p className='truncate text-sm font-medium text-indigo-600'>{user.name}</p>
                <p className='mt-2 flex items-center text-sm text-gray-500'>
                  <MailIcon className='mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400' aria-hidden='true' />
                  <span className='truncate'>{user.email}</span>
                </p>
                <p className='mt-2 flex items-center text-sm text-gray-500'>
                  <HashtagIcon className='mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400' aria-hidden='true' />
                  <span className='truncate'>{user.id}</span>
                </p>
              </div>
              <div className='hidden md:block'>
                <div>
                  <p className='text-sm text-gray-900'>
                    Applied on <time dateTime={new Date().toString()}>{new Date().toString()}</time>
                  </p>
                  <p className='mt-2 flex items-center text-sm text-gray-500'>
                    <CheckCircleIcon className='mr-1.5 h-5 w-5 flex-shrink-0 text-green-400' aria-hidden='true' />
                    some state
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div>
            <ChevronRightIcon className='h-5 w-5 text-gray-400' aria-hidden='true' />
          </div>
        </div>
      </div>
    </li>
  );
};
