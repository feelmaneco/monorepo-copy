import { useFormik } from 'formik';
import React from 'react';
import { Link } from 'react-router-dom';
import { Logo } from '../../../../components/Logo';
import { Button } from '../../../../components/Button';
import { login } from '../../services/firebase/login';
interface FormValues {
  email: string;
  password: string;
}

export const SignInPage: React.FC = () => {
  const handleLogin = (email: string, password: string) => {
    login(email, password);
  };
  const formik = useFormik<FormValues>({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: values => {
      handleLogin(values.email, values.password);
    },
  });
  const { handleSubmit, handleChange, values } = formik;
  return (
    <div className='flex min-h-screen flex-col justify-center bg-green-700 py-12 sm:px-6 lg:px-8'>
      <div className='sm:mx-auto sm:w-full sm:max-w-md'>
        <Logo isBig color='white' />
      </div>

      <div className='mt-8 sm:mx-auto sm:w-full sm:max-w-md'>
        <div className='bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10'>
          <form className='space-y-6' onSubmit={handleSubmit}>
            <div>
              <label htmlFor='email' className='block text-sm font-medium text-gray-700'>
                Email address
              </label>
              <div className='mt-1'>
                <input
                  id='email'
                  name='email'
                  type='email'
                  onChange={handleChange}
                  value={values.email}
                  autoComplete='email'
                  required
                  className='block
                    w-full
                    appearance-none
                    rounded-md
                    border
                    border-gray-300
                    px-3
                    py-2
                    placeholder-gray-400
                    shadow-sm
                    focus:border-indigo-500
                    focus:outline-none
                    focus:ring-indigo-500
                    sm:text-sm'
                />
              </div>
            </div>

            <div>
              <label htmlFor='password' className='block text-sm font-medium text-gray-700'>
                Password
              </label>
              <div className='mt-1'>
                <input
                  id='password'
                  name='password'
                  type='password'
                  onChange={handleChange}
                  value={values.password}
                  autoComplete='current-password'
                  required
                  className='block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm'
                />
              </div>
            </div>

            <div className='flex items-center justify-between'>
              <div className='flex items-center'>
                <input
                  id='remember-me'
                  name='remember-me'
                  type='checkbox'
                  className='h-4 w-4 rounded border-gray-300 text-green-600 focus:ring-green-500'
                />
                <label htmlFor='remember-me' className='ml-2 block text-sm text-gray-900'>
                  Recordarme
                </label>
              </div>

              <div className='text-sm'>
                <Link to='/courses?sort=name' className='font-medium text-indigo-600 hover:text-indigo-500'>
                  Olvidaste el password?
                </Link>
              </div>
            </div>

            <div>
              <button
                type='submit'
                className='flex
                    w-full
                    justify-center
                    rounded-md
                    border
                    border-transparent
                    bg-green-600
                    py-2
                    px-4
                    text-sm
                    font-medium
                    text-white
                    shadow-sm
                    hover:bg-indigo-700
                    focus:outline-none
                    focus:ring-2
                    focus:ring-indigo-500
                    focus:ring-offset-2'
              >
                Entrar
              </button>
            </div>
          </form>

          <div className='mt-6'>
            <div className='relative'>
              <div className='absolute inset-0 flex items-center'>
                <div className='w-full border-t border-gray-300' />
              </div>
              <div className='relative flex justify-center text-sm'>
                <span className='bg-white px-2 text-gray-500'>O entrar usando</span>
              </div>
            </div>

            <div className='mt-6 grid grid-cols-3 gap-3'>
              <div>
                <Button
                  onClick={() => null}
                  data={{
                    type: 'icon',
                    center: true,
                    icon: {
                      iconName: 'facebook-f',
                      prefix: 'fab',
                    },
                    iconSize: 'lg',
                    srLabel: 'Sign in with google',
                  }}
                />
              </div>

              <div>
                <Button
                  onClick={() => null}
                  data={{
                    type: 'icon',
                    center: true,
                    icon: {
                      iconName: 'google',
                      prefix: 'fab',
                    },
                    iconSize: 'lg',
                    srLabel: 'Sign in with google',
                  }}
                />
              </div>

              <div>
                <Button
                  onClick={() => null}
                  data={{
                    type: 'icon',
                    center: true,
                    icon: {
                      iconName: 'google',
                      prefix: 'fab',
                    },
                    iconSize: 'lg',
                    srLabel: 'Sign in with google',
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
