import React from 'react';
import { BrowserRouter, Routes as ReactRoutes, Route } from 'react-router-dom';
import { SignInPage } from '../pages/signIn';
import { SignUpPage } from '../pages/signUp';

export const Routes: React.FC = () => {
  return (
    <BrowserRouter>
      <ReactRoutes>
        <Route path='/' element={<SignInPage />} />
        <Route path='/signup' element={<SignUpPage />} />
        {/* <Route path="*" exact render={() => <Redirect to="/signin" />} /> */}
      </ReactRoutes>
    </BrowserRouter>
  );
};
