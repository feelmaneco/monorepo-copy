import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';

export const login = async (email: string, password: string): Promise<boolean> => {
  try {
    const auth = getAuth();
    await signInWithEmailAndPassword(auth, email, password);
    return true;
  } catch (e) {
    console.log('Errror login', e);
    return false;
  }
};
