import React from 'react';
import Providers from './providers';
import Modal from 'react-modal';

Modal.setAppElement('#document-viewer');
export const AppModule: React.FC = () => {
  return <Providers />;
};
