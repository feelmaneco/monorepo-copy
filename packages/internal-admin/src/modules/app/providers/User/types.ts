import { User } from 'firebase/auth';
import { USER_ROLE } from '../../../../interfaces/Users.interface';

export interface IUserContext {
  user: User | null | false;
  role: USER_ROLE;
}
