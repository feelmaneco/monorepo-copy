import React, { useState, createContext, useEffect } from 'react';
import { IUserContext } from './types';
import { User, getIdTokenResult } from 'firebase/auth';
import { USER_ROLE } from '../../../../interfaces/Users.interface';
import { useSigninCheck } from 'reactfire';

export const UserContext = createContext<IUserContext>({
  user: null,
  role: USER_ROLE.USER,
});

export const UserProvider: React.FC = ({ children }) => {
  const [role, setUserRole] = useState<USER_ROLE>(USER_ROLE.USER);
  const [user, setUser] = useState<User | null | false>(null);
  const { data: signInCheckResult } = useSigninCheck();
  const verifyUser = async (unverifiedUser: User) => {
    const token = await getIdTokenResult(unverifiedUser);
    if (!token.claims.role || typeof token.claims.role !== 'string' || token.claims.role !== 'admin') {
      setUser(false);
    } else {
      setUser(unverifiedUser);
      setUserRole(token.claims.role as USER_ROLE.ADMIN);
    }
  };
  useEffect(() => {
    if (signInCheckResult && !signInCheckResult.signedIn) {
      setUser(false);
    }
    if (signInCheckResult && signInCheckResult.signedIn) {
      verifyUser(signInCheckResult.user);
    }
  }, [signInCheckResult]);
  return <UserContext.Provider value={{ user, role }}>{children}</UserContext.Provider>;
};
