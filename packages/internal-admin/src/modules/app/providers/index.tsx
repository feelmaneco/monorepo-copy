import { initializeAppCheck, ReCaptchaV3Provider } from 'firebase/app-check';
import { connectAuthEmulator, getAuth } from 'firebase/auth';
import { connectFirestoreEmulator, getFirestore } from 'firebase/firestore';
import { connectFunctionsEmulator, getFunctions } from 'firebase/functions';
import { connectStorageEmulator, getStorage } from 'firebase/storage';
import React from 'react';
import { AuthProvider, FirestoreProvider, StorageProvider, useFirebaseApp } from 'reactfire';
import { Routes } from '../routes';
import { AppProvider } from './App';
import { UserProvider } from './User';

const Providers: React.FC = () => {
  const app = useFirebaseApp();
  const firestore = getFirestore(app);
  const auth = getAuth(app);
  const functions = getFunctions(app);
  const storage = getStorage(app);
  if (typeof process.env.REACT_APP_FIREBASE_RECAPTCHA_PROVIDER === 'string' && app) {
    initializeAppCheck(app, {
      provider: new ReCaptchaV3Provider(process.env.REACT_APP_FIREBASE_RECAPTCHA_PROVIDER),
      isTokenAutoRefreshEnabled: true,
    });
  }
  console.log(window.location.hostname === 'localhost');
  if (window.location.hostname === 'localhost') {
    console.log('IS RUNNING LOCAL');
    connectAuthEmulator(auth, 'http://localhost:9099');
    connectFirestoreEmulator(firestore, 'localhost', 8089);
    connectFunctionsEmulator(functions, 'localhost', 5001);
    connectStorageEmulator(storage, 'localhost', 9199);
  }

  return (
    <AuthProvider sdk={auth}>
      <FirestoreProvider sdk={firestore}>
        <StorageProvider sdk={storage}>
          <AppProvider>
            <UserProvider>
              <Routes />
            </UserProvider>
          </AppProvider>
        </StorageProvider>
      </FirestoreProvider>
    </AuthProvider>
  );
};
export default Providers;
