import React, { useContext } from 'react';
import { AuthModule } from '../../auth';
import { UserModule } from '../../user';
import { FullPageLoader } from '../components/FullPageLoader';
import { UserContext } from '../providers/User';

export const Routes: React.FC = () => {
  const { user } = useContext(UserContext);
  if (user === null) {
    return <FullPageLoader />;
  }
  if (user === false) {
    return <AuthModule />;
  }
  if (user) {
    return <UserModule />;
  }
  return null;
};
