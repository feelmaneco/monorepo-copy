import { collection, getDocs, getFirestore, query } from 'firebase/firestore';
import { FirebaseCollections } from '../../../interfaces/firebaseCollections';
import { IHttpResponseError, IHttpResponseSuccess } from '../../../interfaces/HttpResponses.interface';
import { IUserEntity } from '../../../interfaces/Users.interface';
import { formatResponseError } from '../../../utils/formatError';

export const getUsers = async (): Promise<IHttpResponseSuccess<'GetUsers'> | IHttpResponseError> => {
  try {
    const db = getFirestore();
    const q = query(collection(db, FirebaseCollections.USERS));
    const response = await getDocs(q);
    if (response.empty) {
      return {
        error: false,
        data: [],
      };
    }
    const responseData = response.docs.map(doc => ({
      ...(doc.data() as IUserEntity),
      id: doc.id,
    }));
    return {
      error: false,
      data: responseData,
    };
  } catch (error) {
    return formatResponseError('getUsers', error);
  }
};
