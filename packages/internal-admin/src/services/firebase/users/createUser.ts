import { getFunctions, httpsCallable } from 'firebase/functions';

export const createUser = async (name: string, email: string, password: string): Promise<true | string> => {
  try {
    const fireFunctions = getFunctions();
    const createUserReq = httpsCallable(fireFunctions, 'createUser');
    const resposne = await createUserReq({ name, email, password });
    console.log('Response', resposne);
    return true;
  } catch (error) {
    if (error instanceof Error) {
      return error.message;
    }
    return 'unhandled error';
  }
};
