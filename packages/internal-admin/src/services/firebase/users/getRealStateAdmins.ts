import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { FirebaseCollections } from '../../../interfaces/firebaseCollections';
import { IHttpResponseError, IHttpResponseSuccess } from '../../../interfaces/HttpResponses.interface';
import { IUserEntity, USER_ROLE } from '../../../interfaces/Users.interface';
import { formatResponseError } from '../../../utils/formatError';

export const getRealStateAdmins = async (): Promise<IHttpResponseSuccess<'GetUsers'> | IHttpResponseError> => {
  try {
    const db = getFirestore();
    const q = query(collection(db, FirebaseCollections.USERS), where('role', '==', USER_ROLE.REAL_STATE_ADMIN));
    const response = await getDocs(q);
    if (response.empty) {
      return {
        error: false,
        data: [],
      };
    }
    const responseData = response.docs.map(doc => ({
      ...(doc.data() as IUserEntity),
      id: doc.id,
    }));
    return {
      error: false,
      data: responseData,
    };
  } catch (error) {
    return formatResponseError('getUsers', error);
  }
};
