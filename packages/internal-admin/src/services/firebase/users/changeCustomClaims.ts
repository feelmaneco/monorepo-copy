import { getFunctions, httpsCallable } from 'firebase/functions';

import { USER_ROLE } from '../../../interfaces/Users.interface';

export const changeCustomClaims = async (
  uid: string,
  {
    role,
    realStateID,
  }: {
    role?: USER_ROLE;
    realStateID?: string;
  },
): Promise<true | string> => {
  try {
    const fireFunctions = getFunctions();
    const createUserReq = httpsCallable(fireFunctions, 'changeUserClaims');
    const dataToUpdate: {
      role?: USER_ROLE;
      realStateID?: string;
    } = {};
    if (role) {
      dataToUpdate.role = role;
    }
    if (realStateID) {
      dataToUpdate.realStateID = realStateID;
    }
    console.log({ uid, ...dataToUpdate });
    const resposne = await createUserReq({ uid, ...dataToUpdate });
    console.log('Response', resposne);
    return true;
  } catch (error) {
    if (error instanceof Error) {
      return error.message;
    }
    return 'unhandled error';
  }
};
