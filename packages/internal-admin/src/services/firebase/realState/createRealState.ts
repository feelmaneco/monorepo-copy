import { getFunctions, httpsCallable } from 'firebase/functions';

type createRealStateProps = {
  address: string;
  realStateName: string;
  administrator: {
    name: string;
    phoneNumber: string;
    photoUrl: string;
    uid: string;
    email: string;
  };
};
export const createRealState = async (data: createRealStateProps) => {
  try {
    const fireFunctions = getFunctions();
    const createReq = httpsCallable(fireFunctions, 'createRealState');
    const response = await createReq(data);
    console.log('response', response);
  } catch (error) {
    console.log('ErrorcreateRealState', error);
  }
};
