import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { AppModule } from './modules/app';
import reportWebVitals from './reportWebVitals';
import './config/fontawesome';
import './config/enviroment';
import 'tailwindcss/tailwind.css';
import { FirebaseAppProvider } from 'reactfire';
import { FullPageLoader } from './modules/app/components/FullPageLoader';
import { getFirebaseConfig } from './config/variables';

ReactDOM.render(
  <Suspense fallback={<FullPageLoader />}>
    <FirebaseAppProvider suspense={true} firebaseConfig={getFirebaseConfig()}>
      <AppModule />
    </FirebaseAppProvider>
  </Suspense>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
