export const getFirebaseConfig = () => {
  const isProd = process.env.REACT_APP_ENV === 'production';
  if (isProd) {
    return {
      apiKey: 'AIzaSyDNTjH8JmKZZa5dsvZwTJK6Wo6vBo8kLH4',
      authDomain: 'donde-vivo-prod.firebaseapp.com',
      databaseURL: 'https://donde-vivo-prod-default-rtdb.firebaseio.com',
      projectId: 'donde-vivo-prod',
      storageBucket: 'donde-vivo-prod.appspot.com',
      messagingSenderId: '956769231426',
      appId: '1:956769231426:web:f352ca2937140f9e7d43db',
      measurementId: 'G-2LWYZM8VGT',
    };
  }
  return {
    apiKey: 'AIzaSyA4NM8G2Mj_bn6n_i9bX7hQJxvj2SOJS4A',
    authDomain: 'donde-vivo-dev.firebaseapp.com',
    databaseURL: 'https://donde-vivo-dev-default-rtdb.firebaseio.com',
    projectId: 'donde-vivo-dev',
    storageBucket: 'donde-vivo-dev.appspot.com',
    messagingSenderId: '425582762536',
    appId: '1:425582762536:web:2031070948cda33c3ab31d',
    measurementId: 'G-87JBJNJXVL',
  };
};
