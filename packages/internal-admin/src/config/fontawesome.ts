import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUser,
  faHome,
  faHouseUser,
  faImages,
  faPlusCircle,
  faNewspaper,
  faArrowRight,
  faArrowCircleDown,
  faArrowCircleRight,
  faEnvelope,
  faFileAlt,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faTimesCircle,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faTwitter, faGoogle, faFacebookF } from '@fortawesome/free-brands-svg-icons';

library.add(faUser);
library.add(faUsers);
library.add(faHome);
library.add(faHouseUser);
library.add(faFacebook);
library.add(faFacebookF);
library.add(faTwitter);
library.add(faGoogle);
library.add(faImages);
library.add(faPlusCircle);
library.add(faNewspaper);
library.add(faArrowRight);
library.add(faArrowCircleDown);
library.add(faArrowCircleRight);
library.add(faEnvelope);
library.add(faFileAlt);
library.add(faArrowAltCircleLeft);
library.add(faArrowAltCircleRight);
library.add(faTimesCircle);
