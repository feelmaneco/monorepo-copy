import { Timestamp } from 'firebase/firestore';

export interface IMessageEntity {
  text: string;
  from: string;
  realStateOwnerId: string;
  date?: Timestamp;
  realStateName: string;
}
