import { FindRealStateInfo } from './FindRealStateInfo.interface';

import { IUserEntity } from './Users.interface';

export interface GetUserResponse extends IUserEntity {
  id: string;
}
interface IHttpResponseSuccessList {
  GetUsers: GetUserResponse[];
  UpdateRealStateInfo: FindRealStateInfo;
}

export interface IHttpResponseSuccess<T extends keyof IHttpResponseSuccessList> {
  error: false;
  data: IHttpResponseSuccessList[T];
}

export interface IHttpResponseError {
  error: true;
  message: string;
}
