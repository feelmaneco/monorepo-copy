export enum USER_ROLE {
  ADMIN = 'admin',
  REAL_STATE_ADMIN = 'real-state-admin',
  USER = 'user',
  GUARDIAN = 'guardian',
}

export interface IUserEntity {
  name: string;
  email: string;
  role: USER_ROLE;
  photoUrl: string;
}
