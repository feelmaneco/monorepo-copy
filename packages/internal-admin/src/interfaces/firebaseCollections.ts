export enum FirebaseCollections {
  REAL_STATE = 'real-state',
  POSTS = 'real-state-blog-posts',
  REAL_STATE_GALERY = 'real-state-galery',
  MESSAGES = 'real-state-messages',
  DOCUMENTS = 'real-sate-documents',
  USERS = 'real-state-users',
}
