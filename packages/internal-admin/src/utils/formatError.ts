import { IHttpResponseError } from '../interfaces/HttpResponses.interface';

export const formatResponseError = (fun: string, error: unknown): IHttpResponseError => {
  if (error instanceof Error) {
    return {
      error: true,
      message: error.message,
    };
  } else {
    return {
      error: true,
      message: `${fun}: unknown error`,
    };
  }
};
