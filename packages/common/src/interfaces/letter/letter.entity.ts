import { LetterType, StatusLetter } from '@dindom/common/src/interfaces/letter/letter.type';

export interface ILetterEntity {
  realStateID: string;
  letterType: LetterType;
  for: {
    userId: string;
    apartment: string;
    entrance: string;
  };
  status: StatusLetter;
  isNew: boolean;
  photoUrl: string;
}