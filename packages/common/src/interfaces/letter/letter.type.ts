export type LetterType= 'gas' | 'water' | 'energy' | 'bank' | 'other' ;

export type StatusLetter = 'inbox' | 'delivered';