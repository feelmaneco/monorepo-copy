import { BlogStatus } from '../blogStatus.type';
import { BlogAuthor } from '../blogAuthor.type';

export type CreateNewPostInBlogProps = {
  title: string;
  subDescription: string;
  description: string;
  authorId: string;
  imageThumbnailURL: string;
  author?: BlogAuthor;
  status: BlogStatus;
  publicURL?: string;
};
