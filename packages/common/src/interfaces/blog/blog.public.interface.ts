import { BlogAuthor } from './blogAuthor.type';
import { BlogStatus } from './blogStatus.type';

export interface IBlog {
  title: string;
  subDescription: string;
  description: string;
  author?: BlogAuthor;
  authorId: string;
  imageThumbnailURL: string;
  createdAt: { _nanoseconds: number; _seconds: number };
  updatedAt: { _nanoseconds: number; _seconds: number };
  status: BlogStatus;
  publicURL?: string;
}
