import { USER_ROLE } from './roles.enum';

type UserStatus = 'active' | 'offline' | 'banned' | 'deleted';
export interface IUserBaseEntity {
  name: string;
  email: string;
  photoUrl: string;
  status: UserStatus;
}
export interface IUserAdminEntity extends IUserBaseEntity {
  role: USER_ROLE.ADMIN;
}
export interface IUserRealAdminEntity extends IUserBaseEntity {
  role: USER_ROLE.REAL_STATE_ADMIN;
  relaStateId: string;
}
export interface IUserRealStateUserEntity extends IUserBaseEntity {
  role: USER_ROLE.USER;
  relaStateId: string;
  entrance: string;
  aparment: string;
}
export interface IUserRealStateGuardianEntity extends IUserBaseEntity {
  role: USER_ROLE.GUARDIAN;
  relaStateId: string;
}
export type IUserEntity = IUserRealAdminEntity | IUserRealStateUserEntity | IUserRealStateGuardianEntity | IUserAdminEntity;
