export type MailType = 'package' | 'other';

export type StatusMail = 'inbox' | 'delivered';
