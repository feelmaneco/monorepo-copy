import { MailType, StatusMail } from '@dindom/common/src/interfaces/mail/mailReceive.type';

export type onReceiveMailProps = {
  realStateID: string;
  mailType: MailType;
  for: {
    userId: string;
    apartment: string;
    entrance: string;
  };
  status: StatusMail;
  isNew: boolean;
  photoUrl: string;
};
