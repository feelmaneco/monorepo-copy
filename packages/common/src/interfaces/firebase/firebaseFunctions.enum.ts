export enum FirebaseOnCallFunction {
  CHANGE_USRNAME = 'changeUsername',
  ADD_NEW_USER_BY_REAL_STATE_AMDIN = 'APIAddNewUserByRealStateAdmin',
  CREATE_NEW_POST_IN_BLOG = 'createNewPostInBlog',
  GENERATE_CHAT_TOKEN = 'generateUserToken',
}
