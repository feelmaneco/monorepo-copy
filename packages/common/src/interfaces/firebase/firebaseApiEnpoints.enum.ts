export enum FirebaseApiEnpoints {
  CHANGE_USERNAME = '/APIChangeUsername',
  FIND_REAL_STATE_LIST = '/APIFindRealStateList',
  FIND_REAL_STATE_BY_URL = '/APIFindRealStateByUrl',
  FIND_POSTS_BY_REAL_STATE_NAME = '/APIFindPostsByRealStateName',
  FIND_GALERY_BY_REAL_STATE_NAME = '/APIFindGaleryByRealStateName',
  FIND_DOCUMENTS_BY_REAL_STATE_NAME = '/APIFindDocumentsByRealStateName',
  FIND_DINDOM_POST_BY_URL = '/APIFindDinDomPostByURL',
  FIND_ALL_POSTS = '/APIFindAllBlogPosts',
}
