export enum FirebaseStorage {
  LANDING_STATIC = 'landing_static',
  POST_IMAGES = 'postImages',
  PROFILE_IMAGES = 'profileImages',
  REAL_STATE_DOCUMENTS = 'realStateDocuments',
  REAL_STATE_GALERY = 'realStateGalery',
  CHAT_PHOTOS = 'chat/photos',
  CHAT_FILES = 'chat/files',
}
