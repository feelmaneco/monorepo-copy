import { MessageContent, MessageStatus, MessageType } from '../chat/message.type';
import { FieldValue, Timestamp } from 'firebase/firestore';

export interface ChatMessageEntity {
  from: {
    id: string;
    name: string;
    aparment?: string;
    entrance?: string;
    photoUrl: string;
  };
  content: {
    type: MessageType;
    content: MessageContent;
  };
  likes: string[];
  dislikes: string[];
  createdAt: Timestamp;
  updatedAt: Timestamp;
  payload?: any;
  status: MessageStatus;
}
