import { MessageContent, MessageStatus, MessageType } from '../chat/message.type';
import { FieldValue} from 'firebase/firestore';

export interface IChatMessageEntity {
  from: {
    id: string;
    name: string;
    aparment?: string;
    entrance?: string;
    photoUrl: string;
  };
  content: {
    type: MessageType;
    content: MessageContent;
  };
  likes: string[];
  dislikes: string[];
  createdAt: FieldValue;
  updatedAt: FieldValue;
  payload?: any;
  status: MessageStatus;
}
