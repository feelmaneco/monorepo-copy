import { MessageStatus } from "../message.type";

export type onBlockProps = {
  messageid: string;
  realstateId: string;
  newStatus: MessageStatus;
};
