export type onSendFileProps = {
  file: string;
  realStateID: string;
  from: {
    id: string;
    name: string;
    apartment?: string;
    entrance?: string;
    photoUrl: string;
  };
};
