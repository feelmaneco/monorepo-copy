import express from 'express';
import { PORT } from './config/variables';
import './config/telegram.config.ts';
const app = express();

app.listen(PORT, () => {
  console.log(`listening on ${PORT}`);
});
