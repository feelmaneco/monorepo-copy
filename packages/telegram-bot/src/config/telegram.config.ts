import { Telegraf, Telegram } from 'telegraf';
import { TELEGRAM_API_TOKEN } from './variables';
const TelegramBot = new Telegraf(TELEGRAM_API_TOKEN);

TelegramBot.start(ctx => ctx.reply('HELLO'));
TelegramBot.launch();

export { TelegramBot };
