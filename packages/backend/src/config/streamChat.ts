import { DefaultGenerics, StreamChat } from 'stream-chat';

let instance: StreamChat<DefaultGenerics> | null;

export const chatServerClient = (): StreamChat<DefaultGenerics> => {
  if (!instance) {
    instance = StreamChat.getInstance('t7kw7pb5a899', 'b7cynfmxkkkv7hjqzwqffy2n9p732bmjysgmqauguxr3sm7g8bkk35vk7fauw9jy');
  }
  return instance;
};
