/* eslint-disable operator-linebreak */
import * as fireAdmin from "firebase-admin";
import * as functions from "firebase-functions";
// import * as adminDecCreds from '';
// import * as adminProdCred from '';
let instance: fireAdmin.app.App | null = null;
export const admin = (): fireAdmin.app.App => {
  if (!instance) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    let firebaseEnv: boolean =
      functions.config() &&
      functions.config().creds &&
      functions.config().creds.env === "dev";

    if (process.env.ENV === "local") {
      firebaseEnv = true;
    }
    // if(process.env.ENV === "local"){
    //   firebaseEnv =
    // }
    const fireConfig = firebaseEnv
      ? require("./devCreds.json")
      : require("./prodCreds.json");

    instance = fireAdmin.initializeApp({
      credential: fireAdmin.credential.cert(fireConfig),
      databaseURL: firebaseEnv
        ? "https://donde-vivo-dev-default-rtdb.firebaseio.com"
        : "https://donde-vivo-prod-default-rtdb.firebaseio.com",
    });
  }
  return instance;
};

export const firestore = admin().firestore();
