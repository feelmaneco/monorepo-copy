export interface IGetPubliRealStateLanding {
  logoUrl: string;
  realStateName: string;
  urlPrefix: string;
  address: string;
  adminName: string;
}
