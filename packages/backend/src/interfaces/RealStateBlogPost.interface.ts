export interface IRealStateBlogPost {
  title: string;
  subTitle?: string;
  text: string;
  thubnailPreviewImgUrl?: string;
  dateTime?: FirebaseFirestore.Timestamp;
  url: string;
}
