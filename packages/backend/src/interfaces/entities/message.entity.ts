export interface IMessageEntity {
  text: string;
  from: string;
  realStateOwnerId: string;
  date?: FirebaseFirestore.Timestamp;
  realStateName: string;
}
