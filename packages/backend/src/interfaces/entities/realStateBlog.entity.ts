import {IRealStateBlogPost} from "../RealStateBlogPost.interface";

export interface IRealStateBlogPostEntity {
  realStateName: string;
  post: IRealStateBlogPost;
}
