export interface IHttpResponseError {
  error: true;
  message: string;
}
