import {IGetPubliRealStateLanding} from "../getPublicRealState.interface";
import {CreatePostResponse} from "./onCall/CreatePost.response";
import {FindRealStateInfo} from "./onCall/FindRealStateInfo.interface";
import {GetPublicInfoRealStateResponse} from "./onCall/GetPublicInfoRealState.response";
import {IUpdateRealStateGaleryResponse} from "./onCall/UpdateRealStateGalery.response";

interface IHttpResponseSuccessList {
  FindRealStateInfo: FindRealStateInfo;
  UpdateRealStateInfo: FindRealStateInfo;
  UpdateRealStateGalery: IUpdateRealStateGaleryResponse;
  CreatePost: CreatePostResponse;
  CreateMessage: true;
  GetPublicRealState: GetPublicInfoRealStateResponse;
  VerifyAccessCode: true;
  CreateUser: true;
  ChangeUserClaims: true;
  TruthyResponse: true;
  onGetRealStateList: IGetPubliRealStateLanding[];
  unCreateUserByRealAdmin: true;
}

export interface IHttpResponseSuccess<
  T extends keyof IHttpResponseSuccessList
> {
  error: false;
  data: IHttpResponseSuccessList[T];
}
