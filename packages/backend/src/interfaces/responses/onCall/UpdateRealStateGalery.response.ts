export interface IUpdateRealStateGaleryResponse {
  image: {
    url: string;
    name: string;
    order: number;
  };
}
