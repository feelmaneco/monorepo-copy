export interface GetPublicInfoRealStateResponse {
  name: string;
  administratorName: string;
  administratorPhotoUrl: string;
  address: string;
  email: string;
  phoneNumber: string;
  PSEUrl: string;
}
