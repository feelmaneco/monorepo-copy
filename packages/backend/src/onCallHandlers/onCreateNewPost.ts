import {IRealStateBlogPost} from "../interfaces/RealStateBlogPost.interface";
import * as functions from "firebase-functions";
import {IHttpResponseSuccess} from "../interfaces/responses/HttpResponseSuccess.interface";
import {IHttpResponseError} from "../interfaces/responses/HttpResponseError.interface";
import {formatResponseError} from "../utils/formatResponseError";
import {admin} from "../config/firebase";
import {FirebaseCollections} from "@dindom/common/src/interfaces/firebase/firebaseCollections.enum";
import {IRealStateBlogPostEntity} from "../interfaces/entities/realStateBlog.entity";

type onCreateNewPostHandlerProps = {
  data: IRealStateBlogPost;
  realStateName: string;
};

export const onCreateNewPostHandler = async (
  data: onCreateNewPostHandlerProps,
  context: functions.https.CallableContext
): Promise<IHttpResponseSuccess<"CreatePost"> | IHttpResponseError> => {
  try {
    console.log("DATA", data);
    if (!context || !context.auth) throw new Error("Unauthorized user");
    if (!data || !data.data || !data.realStateName) {
      throw new Error("No data provided");
    }
    const dataToInsert: IRealStateBlogPostEntity = {
      post: data.data,
      realStateName: data.realStateName,
    };
    await admin()
      .firestore()
      .collection(FirebaseCollections.POSTS)
      .doc()
      .set(dataToInsert);
    return {
      error: false,
      data: {
        isPublished: true,
      },
    };
  } catch (error) {
    return formatResponseError("onCreateNewPostHandler", error);
  }
};
