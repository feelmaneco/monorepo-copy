import {admin} from "../config/firebase";
import {FirebaseCollections} from "@dindom/common/src/interfaces/firebase/firebaseCollections.enum";
import {IHttpResponseError} from "../interfaces/responses/HttpResponseError.interface";
import {IHttpResponseSuccess} from "../interfaces/responses/HttpResponseSuccess.interface";
import {formatResponseError} from "../utils/formatResponseError";
import * as functions from "firebase-functions";
import {IRealStateGaleryEntity} from "../interfaces/entities/realStateGalery.entity";

type onFindRealStateInfoHandlerProps = {
  image: {
    url: string;
    name: string;
  };
  realStateName: string;
  type: "ADD" | "UPDATE_ORDER";
};

export const onUploadRealStateDocumentsHandler = async (
  data: onFindRealStateInfoHandlerProps,
  context: functions.https.CallableContext
): Promise<
  IHttpResponseSuccess<"UpdateRealStateGalery"> | IHttpResponseError
> => {
  console.log("RUNNING Func");
  try {
    if (!context || !context.auth) throw new Error("Unauthorized user");
    if (!data || !data.image || !data.type || !data.realStateName) {
      throw new Error("no data provided");
    }
    if (data.type === "ADD") {
      console.log("ADD", data);
      const dataToAdd: IRealStateGaleryEntity = {
        name: data.image.name,
        realStateName: data.realStateName,
        url: data.image.url,
      };
      await admin()
        .firestore()
        .collection(FirebaseCollections.DOCUMENTS)
        .doc()
        .create(dataToAdd);
      // .set();
    }
    console.log("done");
    return {
      error: false,
      data: {
        image: {
          name: data.image.name,
          order: 1,
          url: data.image.url,
        },
      },
    };
  } catch (error) {
    console.log("ERROR", error);
    return formatResponseError("onUploadRealStateDocumentsHandler", error);
  }
};
