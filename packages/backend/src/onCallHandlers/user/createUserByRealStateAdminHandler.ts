import { https } from 'firebase-functions';
import { formatResponseError } from '../../utils/formatResponseError';
import { admin } from '../../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { auth } from 'firebase-admin';
import { USER_ROLE } from '@dindom/common/src/interfaces/user/roles.enum';
import { HttpSimpleErrorResponse, HttpSimpleSuccessResposne } from '@dindom/common/src/interfaces/firebase/HttpResponses.interface';

type ChangeUsernameHanlderProps = {
  name: string;
  id: string;
};

export const createUserByRealStateAdminHandler = async (
  input: ChangeUsernameHanlderProps,
  context: https.CallableContext,
): Promise<HttpSimpleSuccessResposne | HttpSimpleErrorResponse> => {
  try {
    console.log(context.auth);
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('changeUsernameHanlder', error);
  }
};
