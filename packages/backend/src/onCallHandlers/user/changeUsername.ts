import { https } from 'firebase-functions';
import { formatResponseError } from '../../utils/formatResponseError';
import { admin } from '../../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { auth } from 'firebase-admin';
import { USER_ROLE } from '@dindom/common/src/interfaces/user/roles.enum';

type ChangeUsernameHanlderProps = {
  name: string;
  id: string;
};

export const changeUsernameHanlder = async ({ id, name }: ChangeUsernameHanlderProps, context: https.CallableContext): Promise<any> => {
  try {
    await admin().firestore().collection(FirebaseCollections.USERS).doc(id).set(
      {
        name,
      },
      { merge: true },
    );
    const user = await auth().getUser(id);
    // console.log('user', user);
    if (user.customClaims && user.customClaims.role === USER_ROLE.REAL_STATE_ADMIN) {
      const response = await admin()
        .firestore()
        .collection(FirebaseCollections.REAL_STATE)
        .where('administrator.uid', '==', user.uid)
        .get();
      console.log('response', response.empty);
      if (!response.empty) {
        await admin()
          .firestore()
          .collection(FirebaseCollections.REAL_STATE)
          .doc(response.docs[0].id)
          .set(
            {
              ...response.docs[0].data(),
              administrator: {
                ...response.docs[0].data().administrator,
                name,
              },
            },
            { merge: true },
          );
      }
    }
    return {
      error: false,
      data: '',
    };
  } catch (error) {
    return formatResponseError('changeUsernameHanlder', error);
  }
};
