import { HttpSimpleErrorResponse, HttpSuccessResponse } from '@dindom/common/build/interfaces/firebase/HttpResponses.interface';
import { https } from 'firebase-functions';
import { chatServerClient } from '../../config/streamChat';
import { formatResponseError } from '../../utils/formatResponseError';
export const generateUserChatTokenHandler = async (
  _input: unknown,
  context: https.CallableContext,
): Promise<HttpSimpleErrorResponse | HttpSuccessResponse<'generateNewToken'>> => {
  try {
    if (!context.auth) throw new Error('unathorized user');
    const token = chatServerClient().createToken(context.auth.uid);
    return {
      error: false,
      data: token,
    };
  } catch (error) {
    return formatResponseError('generateUserChatToken', error);
  }
};
