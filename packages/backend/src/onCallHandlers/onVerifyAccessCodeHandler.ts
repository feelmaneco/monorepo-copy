import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IRealStateEntity } from '@dindom/common/src/interfaces/realState/realState';
import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';

type onVerifyAccessCodeHandlerProps = {
  code: string;
  realStateName: string;
};
export const onVerifyAccessCodeHandler = async (
  data: onVerifyAccessCodeHandlerProps,
): Promise<IHttpResponseSuccess<'VerifyAccessCode'> | IHttpResponseError> => {
  try {
    if (!data || !data.code) {
      throw new Error('No code provided');
    }
    if (!data || !data.realStateName) {
      throw new Error('No realStateName provided');
    }

    const realStateRequest = await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .where('name', '==', data.realStateName)
      .get();
    if (realStateRequest.empty) {
      throw new Error('No realStateName was found');
    }
    const realState = realStateRequest.docs[0].data() as IRealStateEntity;
    if (realState.accessCode !== data.code) {
      throw new Error('Invalid code');
    }
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onVerifyAccessCodeHandler', error);
  }
};
