import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';
import * as functions from 'firebase-functions';
import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IRealStateEntity } from '@dindom/common/src/interfaces/realState/realState';

type onFindRealStateInfoHandlerProps = {
  public: boolean;
  name: string;
};

export const onFindRealStateInfoHandler = async (
  data: onFindRealStateInfoHandlerProps,
  context: functions.https.CallableContext,
): Promise<IHttpResponseSuccess<'FindRealStateInfo'> | IHttpResponseError> => {
  try {
    console.log('functions.config()', functions.config());
    if (context.app == undefined && process.env.ENV !== 'local') {
      throw new functions.https.HttpsError('failed-precondition', 'The function must be called from an App Check verified app.');
    }
    if (data && data.public && data.name) {
      if (!context || !context.auth) throw new Error('Unauthorized user');
      const realStateRequest = await admin().firestore().collection(FirebaseCollections.REAL_STATE).where('name', '==', data.name).get();
      if (realStateRequest.empty) throw new Error('No RealState was found');
      const realStateResponse = realStateRequest.docs[0].data() as IRealStateEntity;
      return {
        error: false,
        data: {
          address: realStateResponse.address,
          administrator: {
            name: realStateResponse.administrator.name,
            photoUrl: realStateResponse.administrator.photoUrl,
          },
          aparmentsNumber: realStateResponse.aparmentsNumber,
          email: realStateResponse.email.administrator,
          name: realStateResponse.name,
          phoneNumber: realStateResponse.phoneNumbers.administrator,
          photoUrl: realStateResponse.photoURL,
          status: realStateResponse.goverment.status,
          uid: (realStateRequest.docs[0] && realStateRequest.docs[0].id) || '',
        },
      };
    } else {
      if (!context || !context.auth) throw new Error('Unauthorized user');
      const realStateRequest = await admin()
        .firestore()
        .collection(FirebaseCollections.REAL_STATE)
        .where('administrator.uid', '==', context.auth.uid)
        .get();
      if (realStateRequest.empty) throw new Error('No RealState was found');
      const realStateResponse = realStateRequest.docs[0].data() as IRealStateEntity;
      return {
        error: false,
        data: {
          address: realStateResponse.address,
          administrator: {
            name: realStateResponse.administrator.name,
            photoUrl: realStateResponse.administrator.photoUrl,
          },
          aparmentsNumber: realStateResponse.aparmentsNumber,
          email: realStateResponse.email.administrator,
          name: realStateResponse.name,
          phoneNumber: realStateResponse.phoneNumbers.administrator,
          photoUrl: realStateResponse.photoURL,
          status: realStateResponse.goverment.status,
          uid: (realStateRequest.docs[0] && realStateRequest.docs[0].id) || '',
        },
      };
    }
  } catch (error) {
    return formatResponseError('onFindRealStateInfoHandler', error);
  }
};
