import {formatResponseError} from "../utils/formatResponseError";
import * as functions from "firebase-functions";
import {IHttpResponseSuccess} from "../interfaces/responses/HttpResponseSuccess.interface";
import {IHttpResponseError} from "../interfaces/responses/HttpResponseError.interface";
import {admin} from "../config/firebase";
import {FirebaseCollections} from "@dindom/common/src/interfaces/firebase/firebaseCollections.enum";
type changeProfileImageHandlerProps = {
  filePath: string;
};

export const changeProfileImageHandler = async (
  data: changeProfileImageHandlerProps,
  context: functions.https.CallableContext
): Promise<IHttpResponseSuccess<"TruthyResponse"> | IHttpResponseError> => {
  try {
    if (context.app == undefined) {
      //   throw new functions.https.HttpsError(
      //     "failed-precondition",
      //     "The function must be called from an App Check verified app."
      //   );
    }
    if (!context || !context.auth || !context.auth.uid) {
      throw new Error("unathorized user");
    }
    if (!data || !data.filePath) {
      throw new Error("No image path Provided");
    }

    await admin()
      .firestore()
      .collection(FirebaseCollections.USERS)
      .doc(context.auth.uid)
      .update({administrator: {photoUrl: data.filePath}});
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError("changeProfileImageHandler", error);
  }
};
