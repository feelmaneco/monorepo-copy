import { admin } from '../config/firebase';
import * as AdminFire from 'firebase-admin';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IMessageEntity } from '../interfaces/entities/message.entity';
import { IRealStateEntity } from '@dindom/common/src/interfaces/realState/realState';
import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';

type onCreateMessageHandlerProps = {
  text: string;
  from: string;
  realStateOwnerId: string;
  realStateUrl: string;
  fromLastname: string;
};

export const onCreateMessageHandler = async (
  data: onCreateMessageHandlerProps,
): Promise<IHttpResponseSuccess<'CreateMessage'> | IHttpResponseError> => {
  try {
    console.log('DATA', data);
    if ( !data || !data.from || !data.realStateUrl || !data.text || !data.realStateOwnerId) {
      throw new Error('No data provided');
    }
    const realStateResponce = await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .where('publicUrl', '==', data.realStateUrl)
      .get();
    if (realStateResponce.empty) {
      throw new Error('no real state was found');
    }
    const realState = realStateResponce.docs[0].data() as IRealStateEntity;
    const dataToSend: IMessageEntity = {
      from: `${data.from} ${data.fromLastname}`,
      realStateName: realState.name,
      text: data.text,
      realStateOwnerId: data.realStateOwnerId,
      date: AdminFire.firestore.Timestamp.now(),
    };
    admin().firestore().collection(FirebaseCollections.MESSAGES).doc().set(dataToSend);
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onCreateMessageHandler', error);
  }
};
