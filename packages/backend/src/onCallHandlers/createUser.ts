import {IHttpResponseError} from "../interfaces/responses/HttpResponseError.interface";
import {IHttpResponseSuccess} from "../interfaces/responses/HttpResponseSuccess.interface";
import {formatResponseError} from "../utils/formatResponseError";
import * as functions from "firebase-functions";
import {admin} from "../config/firebase";

type onCreateUserHandlerProps = {
  email: string;
  password: string;
  name: string;
};

export const createUserHandler = async (
  data: onCreateUserHandlerProps,
  context: functions.https.CallableContext
): Promise<IHttpResponseSuccess<"CreateUser"> | IHttpResponseError> => {
  try {
    if (context.app == undefined) {
      // throw new functions.https.HttpsError(
      //   "failed-precondition",
      //   "The function must be called from an App Check verified app."
      // );
    }
    if (
      !data ||
      !data.email ||
      !data.password ||
      !data.name ||
      !context ||
      !context.auth
    ) {
      throw new Error("Unauthorized user");
    }
    await admin().auth().createUser({
      displayName: data.name,
      email: data.email,
      password: data.password,
      photoURL:
        // eslint-disable-next-line max-len
        "https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/images.png?alt=media&token=67241575-57e9-41fc-85e8-00db6fb6b842",
    });
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError("onCreateUserHandler", error);
  }
};
