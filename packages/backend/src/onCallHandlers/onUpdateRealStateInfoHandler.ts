import {IHttpResponseError} from "../interfaces/responses/HttpResponseError.interface";
import {IHttpResponseSuccess} from "../interfaces/responses/HttpResponseSuccess.interface";
import {formatResponseError} from "../utils/formatResponseError";
import * as functions from "firebase-functions";
import {admin} from "../config/firebase";
import {FirebaseCollections} from "@dindom/common/src/interfaces/firebase/firebaseCollections.enum";
import {FindRealStateInfo} from "../interfaces/responses/onCall/FindRealStateInfo.interface";

type onFindRealStateInfoHandlerProps = {
  data: FindRealStateInfo;
};

export const onUpdateRealStateInfoHandler = async (
  data: onFindRealStateInfoHandlerProps,
  context: functions.https.CallableContext
): Promise<
  IHttpResponseSuccess<"UpdateRealStateInfo"> | IHttpResponseError
> => {
  try {
    if (!data || !data.data) {
      throw new Error("No data provided");
    }
    if (!context || !context.auth) throw new Error("Unauthorized user");
    const realStateRequest = await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .where("administrator.uid", "==", context.auth.uid)
      .get();
    if (realStateRequest.empty) {
      throw new Error("Realstate wasn't found");
    }
    // address: realStateResponse.address,
    //       administrator: {
    //         name: realStateResponse.administrator.name,
    //         photoUrl: realStateResponse.administrator.photoUrl,
    //       },
    //       aparmentsNumber: realStateResponse.aparmentsNumber,
    //       email: realStateResponse.email.administrator,
    //       name: realStateResponse.name,
    //       phoneNumber: realStateResponse.phoneNumbers.administrator,
    //       photoUrl: realStateResponse.photoURL,
    //       status: realStateResponse.goverment.status,
    // const dataToSet: IRealStateEntity = {
    //   address: data.data.address,
    //   email: {
    //     administrator: data.data.email,
    //   },
    //   aparmentsNumber: data.data.aparmentsNumber,
    //   phoneNumbers: {
    //     administrator: data.data.phoneNumber,
    //   },
    //   name: data.data.name,
    //   photoURL: data.data.photoUrl,
    // };
    await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .doc(realStateRequest.docs[0].id)
      .set(
        {
          address: data.data.address,
          email: {
            administrator: data.data.email,
          },
          aparmentsNumber: data.data.aparmentsNumber,
          phoneNumbers: {
            administrator: data.data.phoneNumber,
          },
          name: data.data.name,
          photoURL: data.data.photoUrl,
        },
        {merge: true}
      );
    return {
      error: false,
      data: data.data,
    };
  } catch (error) {
    return formatResponseError("onFindRealStateInfoHandler", error);
  }
};
