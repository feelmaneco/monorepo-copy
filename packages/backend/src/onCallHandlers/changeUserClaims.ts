import { auth } from 'firebase-admin';
import * as functions from 'firebase-functions';
import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { USER_ROLE } from '@dindom/common/build/interfaces/user/roles.enum';
import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';

type chageUserClaimsHandlerProps = {
  uid: string;
  role?: USER_ROLE;
  realStateID?: string;
};

export const chageUserClaimsHandler = async (
  { uid, role, realStateID }: chageUserClaimsHandlerProps,
  context: functions.https.CallableContext,
): Promise<IHttpResponseSuccess<'ChangeUserClaims'> | IHttpResponseError> => {
  try {
    if (context.app == undefined) {
      // throw new functions.https.HttpsError(
      //     'failed-precondition',
      //     'The function must be called from an App Check verified app.')
    }
    if (!uid) {
      throw new Error('no uid provided');
    }
    const user = await auth().getUser(uid);

    if (role) {
      await auth().setCustomUserClaims(uid, {
        ...user.customClaims,
        role,
      });
    }
    if (realStateID) {
      await auth().setCustomUserClaims(uid, {
        ...user.customClaims,
        realStateID,
      });
    }
    const dataToUpdate: { role?: string; realStateID?: string } = {};
    if (role || realStateID) {
      if (role) {
        dataToUpdate.role = role;
      }
      if (realStateID) {
        dataToUpdate.realStateID = realStateID;
      }
      await admin().firestore().collection(FirebaseCollections.USERS).doc(uid).set(dataToUpdate, { merge: true });
    }

    return {
      data: true,
      error: false,
    };
  } catch (error) {
    return formatResponseError('chageUserClaimsHandler', error);
  }
};
