import { HttpSuccessResponse, HttpSimpleErrorResponse } from '@dindom/common/build/interfaces/firebase/HttpResponses.interface';
import * as functions from 'firebase-functions';
// import {  } from 'firebase-admin/database';
import { FieldValue } from 'firebase-admin/firestore';
import { formatResponseError } from '../../utils/formatResponseError';
import { IBlogEntity } from '../../../../common/src/interfaces/blog/blog.entity';
import { admin } from '../../config/firebase';
import * as AdminFire from 'firebase-admin';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { CreateNewPostInBlogProps } from '../../../../common/src/interfaces/blog/requestProps/createNewPostInBlog';

export const createNewPostInBlogHandler = async (
  data: CreateNewPostInBlogProps,
  context: functions.https.CallableContext,
): Promise<HttpSuccessResponse<'createNewPostInBlog'> | HttpSimpleErrorResponse> => {
  try {
    // if (!context.auth || !context.auth.uid) {
    //   throw new Error('Unauthorized User');
    // }
    // const user = await admin().auth().getUser(context.auth.uid);
    // if (!user.customClaims || !user.customClaims.role || user.customClaims.role !== 'admin') {
    //   throw new Error('Unauthorized User');
    // }
    if (!data || !data.title || !data.subDescription || !data.description || !data.authorId) {
      throw new Error('no data provided');
    }

    const dataToSend: IBlogEntity = {
      title: data.title,
      subDescription: data.subDescription,
      description: data.description,
      authorId: data.authorId,
      imageThumbnailURL: data.imageThumbnailURL,
      status: data.status,
      createdAt: FieldValue.serverTimestamp(),
      updatedAt: FieldValue.serverTimestamp(),
    };
    await admin().firestore().collection(FirebaseCollections.BLOG).doc().set(dataToSend);
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('createNewPostInBlogHandler', error);
  }
};
