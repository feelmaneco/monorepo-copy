import { CITY } from '../../interfaces/City.enum';
import { IRealStateEntity } from '@dindom/common/src/interfaces/realState/realState';
import * as functions from 'firebase-functions';
import { REAL_STATE_GOVEMENT_STATUS } from '@dindom/common/src/interfaces/realState/realState';
import { IHttpResponseError } from '../../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../../utils/formatResponseError';
import { USER_ROLE } from '@dindom/common/build/interfaces/user/roles.enum';
import { makeid } from '../../utils/makeId';
import { admin } from '../../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { GroupChatEntity } from '../../../../common/build/interfaces/entities/groupChat.entity';
import { IChatMessageEntity } from '../../../../common/build/interfaces/entities/groupChatMessages.entity';
import { FieldValue } from 'firebase-admin/firestore';
import { chatServerClient } from '../../config/streamChat';

type createRealStateHandlerProps = {
  address: string;
  realStateName: string;
  administrator: {
    name: string;
    phoneNumber: string;
    photoUrl: string;
    uid: string;
    email: string;
  };
};

export const createRealStateHandler = async (
  data: createRealStateHandlerProps,
  context: functions.https.CallableContext,
): Promise<IHttpResponseSuccess<'TruthyResponse'> | IHttpResponseError> => {
  try {
    if (!context || !context.auth || !context.auth.token || !context.auth.token.role || context.auth.token.role !== USER_ROLE.ADMIN) {
      throw new Error('unathorized user');
    }

    const dataToInsert: IRealStateEntity = {
      accessCode: makeid(5),
      address: {
        address: data.address,
        city: CITY.BOGOTA,
      },
      administrator: {
        name: data.administrator.name,
        phone: data.administrator.phoneNumber,
        photoUrl: data.administrator.photoUrl,
        uid: data.administrator.uid,
      },
      aparmentsNumber: 32,
      email: {
        administrator: data.administrator.email,
      },
      galery: [],
      goverment: {
        status: REAL_STATE_GOVEMENT_STATUS.four,
      },
      name: data.realStateName,
      phoneNumbers: {
        administrator: data.administrator.phoneNumber,
        control: '',
      },
      PSEUrl: '',
      photoURL:
        // eslint-disable-next-line max-len
        'https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/realstatedefaultbackground.jpeg?alt=media&token=adb4b4b5-30f4-40bb-8f7e-7a9563f3ec48',
      publicUrl: data.realStateName.trim().replace(/\s/g, '').toLowerCase(),
    };

    const realStateId = makeid(21);
    const channel = chatServerClient().channel('messaging', realStateId, {
      name: data.realStateName,
      created_by_id: context.auth.uid,
    });
    await channel.create();
    const dataChatToSend: GroupChatEntity = {
      realStateId,
      administrators: [{ name: 'AdminChat', email: '', photoUrl: '', status: 'active', role: USER_ROLE.ADMIN }],
      realStateName: data.realStateName,
      realStateAdministrator: {
        name: data.administrator.name,
        email: data.administrator.email,
        photoUrl: data.administrator.photoUrl,
        status: 'active',
        role: USER_ROLE.ADMIN,
      },
      bannedIds: [],
    };

    const dataMessageToSend: IChatMessageEntity = {
      from: {
        id: '',
        name: '',
        aparment: '',
        entrance: '',
        photoUrl: '',
      },
      content: {
        type: 'text',
        content: { text: '' },
      },
      likes: [],
      dislikes: [],
      createdAt: FieldValue.serverTimestamp(),
      updatedAt: FieldValue.serverTimestamp(),
      payload: '',
      status: 'public',
    };

    await admin().firestore().collection(FirebaseCollections.REAL_STATE).doc(realStateId).create(dataToInsert);
    await admin().firestore().collection(FirebaseCollections.REAL_STATE_CHATS).doc(realStateId).create(dataChatToSend);
    await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE_CHATS)
      .doc(realStateId)
      .collection(FirebaseCollections.REAL_STATE_CHAT_MESSAGES)
      .doc()
      .create(dataMessageToSend);

    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('createRealStateHandler', error);
  }
};
