import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';
import { IMailEntity } from '@dindom/common/build/interfaces/mail/mail.entity';
import { onReceiveMailProps } from '@dindom/common/src/interfaces/mail/requestProps/onReceiveMailProps';

export const onReceiveMailHandler = async (data: onReceiveMailProps): Promise<IHttpResponseSuccess<'TruthyResponse'> | IHttpResponseError> => {
  try {
    if (!data || !data.for) {
      throw new Error('No data provided');
    }

    const dataMail: IMailEntity = {
      realStateID: data.realStateID,
      mailType: data.mailType,
      for: {
        userId: data.for.userId,
        apartment: data.for.apartment,
        entrance: data.for.entrance,
      },
      status: data.status,
      isNew: data.isNew,
      photoUrl: data.photoUrl,
    };

    await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .doc(data.realStateID)
      .collection(FirebaseCollections.REAL_STATE_RECEIVED)
      .doc()
      .collection(FirebaseCollections.REAL_STATE_MAILS)
      .doc()
      .create(dataMail);

    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onRecibeCorrespondenceHandler', error);
  }
};
