import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';
import { admin } from '../config/firebase';
import { UserRecord } from 'firebase-admin/lib/auth/user-record';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { USER_ROLE } from '@dindom/common/build/interfaces/user/roles.enum';
import { IUserEntity } from '@dindom/common/build/interfaces/user/user.entity';
export const onCreateUserHandler = async (user: UserRecord): Promise<IHttpResponseSuccess<'CreateUser'> | IHttpResponseError> => {
  try {
    const dataToSet: IUserEntity = {
      email: user.email || 'sample@dondevivo.co',
      name: user.displayName || '',
      photoUrl: user.photoURL || '',
      role: (user.customClaims && user.customClaims.role) || USER_ROLE.REAL_STATE_ADMIN,
      relaStateId: '',
      status: 'active',
    };
    await admin().firestore().collection(FirebaseCollections.USERS).doc(user.uid).set(dataToSet, { merge: true });
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onCreateUserHandler', error);
  }
};
