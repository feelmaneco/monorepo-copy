import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';
import { FieldValue } from 'firebase-admin/firestore';
import { IChatMessageEntity } from '@dindom/common/build/interfaces/entities/groupChatMessages.entity';
import { onSendPhotoProps } from '@dindom/common/src/interfaces/chat/requestProps/onSendPhotoProps';  

export const onSendPhotoHandler = async (data: onSendPhotoProps): Promise<IHttpResponseSuccess<'TruthyResponse'> | IHttpResponseError> => {
  try {
    if (!data || !data.from) {
      throw new Error('No data provided');
    }

    const dataPhotoToSend: IChatMessageEntity = {
      from: {
        id: data.from.id,
        name: data.from.name,
        aparment: data.from.apartment,
        entrance: data.from.entrance,
        photoUrl: data.from.photoUrl,
      },
      content: {
        type: 'photo',
        content: { url: data.file },
      },
      likes: [],
      dislikes: [],
      createdAt: FieldValue.serverTimestamp(),
      updatedAt: FieldValue.serverTimestamp(),
      payload: '',
      status: 'public',
    };

    await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE_CHATS)
      .doc(data.realStateID)
      .collection(FirebaseCollections.REAL_STATE_CHAT_MESSAGES)
      .doc()
      .create(dataPhotoToSend);
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onSendPhotoHandler', error);
  }
};
