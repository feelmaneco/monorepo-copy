import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';
import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IRealStateEntity } from '@dindom/common/src/interfaces/realState/realState';
type onGetPublicRealStateHandlerProps = {
  publicUrl: string;
};

export const onGetPublicRealStateHandler = async (
  data: onGetPublicRealStateHandlerProps,
): Promise<IHttpResponseSuccess<'GetPublicRealState'> | IHttpResponseError> => {
  try {
    if (!data || !data.publicUrl) {
      throw new Error('no data provided');
    }
    const response = await admin().firestore().collection(FirebaseCollections.REAL_STATE).where('publicUrl', '==', data.publicUrl).get();
    if (response.empty) {
      throw new Error('No realState provided');
    }
    const realState = response.docs[0].data() as IRealStateEntity;
    // console.log("Response");
    return {
      error: false,
      data: {
        PSEUrl: 'https://www.pagosvirtualesavvillas.com.co/personal/pagos/292',
        address: realState.address.address,
        administratorName: realState.administrator.name,
        email: realState.email.administrator,
        name: realState.name,
        phoneNumber: realState.phoneNumbers.administrator,
        administratorPhotoUrl: realState.administrator.photoUrl,
      },
    };
  } catch (error) {
    return formatResponseError('onFindRealStateInfoHandler', error);
  }
};
