import { auth } from 'firebase-admin';
import { admin, firestore } from '../../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { USER_ROLE } from '@dindom/common/src/interfaces/user/roles.enum';
import { IHttpResponseError } from '../../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../../utils/formatResponseError';
import { chatServerClient } from '../../config/streamChat';

type onCreateNewUserByRealAdminProps = {
  user: {
    name: string;
    lastname: string;
    apartment: string;
    entrance: string;
    status: string;
    email: string;
  };
  realStateId: string;
};

export const onCreateNewUserByRealAdminHandler = async (
  data: onCreateNewUserByRealAdminProps,
): Promise<IHttpResponseSuccess<'CreateMessage'> | IHttpResponseError> => {
  try {
    console.log('DATA', data);
    if (
      !data ||
      !data.user ||
      typeof data.realStateId !== 'string' ||
      typeof data.user.apartment !== 'string' ||
      typeof data.user.email !== 'string' ||
      typeof data.user.entrance !== 'string' ||
      typeof data.user.lastname !== 'string' ||
      typeof data.user.name !== 'string' ||
      typeof data.user.status !== 'string'
    ) {
      throw new Error('invalid data');
    }
    const user = await admin()
      .auth()
      .createUser({
        displayName: `${data.user.name} ${data.user.lastname}`,
        email: data.user.email,
        password: '123123',
        emailVerified: true,
        photoURL:
          // eslint-disable-next-line max-len
          'https://firebasestorage.googleapis.com/v0/b/donde-vivo-dev.appspot.com/o/images.png?alt=media&token=67241575-57e9-41fc-85e8-00db6fb6b842',
      });
    await auth().setCustomUserClaims(user.uid, {
      role: USER_ROLE.USER,
      realStateID: data.realStateId,
    });
    const realStateCHat = await chatServerClient().queryChannels({
      type: 'messaging',
      id: data.realStateId,
    });
    console.log('realStateCHat', user.uid);
    const chatUser = await chatServerClient().upsertUser({
      id: user.uid,
      role: 'admin',
    });
    console.log('sdasd', chatUser);
    await realStateCHat[0].addMembers([
      {
        user_id: user.uid,
      },
    ]);
    await firestore
      .collection(FirebaseCollections.USERS)
      .doc(user.uid)
      .set(
        {
          realStateInfo: {
            aparment: data.user.apartment,
            entrance: data.user.entrance,
            realstateID: data.realStateId,
          },
        },
        { merge: true },
      );
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onCreateMessageHandler', error);
  }
};
