import { firestore } from 'firebase-admin';
import { EmailService } from '../config/nodemailer';

export const onCreateNewRegistrationRequestHanlder = async (snapshot: firestore.QueryDocumentSnapshot): Promise<void> => {
  const data = snapshot.data();
  console.log(data);
  if (data.email && data.name) {
    const mailOptions = {
      from: 'info@dindom.co', // sender address
      to: data.email,
      subject: 'Some subject', // Subject line
      html: `<p>HELLO DEAR  ${data.name}</p>`, // plain text body
    };
    const mailOptionsAdmin = {
      from: 'info@dindom.co', // sender address
      to: 'info@dindom.co',
      subject: 'NEW REQUEST', // Subject line
      html: `<p>FROM:${data.email},  REAL_STATE:${data.name}</p>`, // plain text body
    };
    EmailService().sendMail(mailOptions, (error: any, info: any) => {
      console.log('error', error);
      console.log('info', info);
    });
    EmailService().sendMail(mailOptionsAdmin, (error: any, info: any) => {
      console.log('error', error);
      console.log('info', info);
    });
  }
};
