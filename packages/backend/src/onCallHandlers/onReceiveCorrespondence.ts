import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';
import { ILetterEntity } from '@dindom/common/build/interfaces/letter/letter.entity';
import { onReceiveCorrespondenceProps } from '@dindom/common/src/interfaces/letter/requestProps/onCorrespondenceProps';

export const onReceiveCorrespondenceHandler = async (
  data: onReceiveCorrespondenceProps,
): Promise<IHttpResponseSuccess<'TruthyResponse'> | IHttpResponseError> => {
  try {
    if (!data || !data.for) {
      throw new Error('No data provided');
    }

    const dataCorrespondence: ILetterEntity = {
      realStateID: data.realStateID,
      letterType: data.letterType,
      for: {
        userId: data.for.userId,
        apartment: data.for.apartment,
        entrance: data.for.entrance,
      },
      status: data.status,
      isNew: data.isNew,
      photoUrl: data.photoUrl,
    };

    await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .doc(data.realStateID)
      .collection(FirebaseCollections.REAL_STATE_RECEIVED)
      .doc()
      .collection(FirebaseCollections.REAL_STATE_LETTERS)
      .doc()
      .create(dataCorrespondence);

    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onRecibeCorrespondenceHandler', error);
  }
};
