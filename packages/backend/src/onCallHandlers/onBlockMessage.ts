import { admin } from '../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { IHttpResponseError } from '../interfaces/responses/HttpResponseError.interface';
import { IHttpResponseSuccess } from '../interfaces/responses/HttpResponseSuccess.interface';
import { formatResponseError } from '../utils/formatResponseError';
import * as functions from 'firebase-functions';
import {onBlockProps} from '@dindom/common/src/interfaces/chat/requestProps/onBlockProps'


export const onBlockMessageHandler = async (
  data: onBlockProps,
  context: functions.https.CallableContext,
): Promise<IHttpResponseSuccess<'TruthyResponse'> | IHttpResponseError> => {
  try {
    console.log('DATA', data);
    if (!data || !data.messageid || !data.realstateId || !data.newStatus) {
      throw new Error('No data provided');
    }

    const realStateChatRequest = await admin().firestore().collection(FirebaseCollections.REAL_STATE_CHATS).doc(data.realstateId).get();

    const chatMessageRequest = await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE_CHATS)
      .doc(data.realstateId)
      .collection(FirebaseCollections.REAL_STATE_CHAT_MESSAGES)
      .doc(data.messageid)
      .get();

    if (!realStateChatRequest.exists) {
      throw new Error("RealState Chat wasn't found");
    } else if (!chatMessageRequest.exists) {
      throw new Error("Message wasn't found");
    }

    await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE_CHATS)
      .doc(data.realstateId)
      .collection(FirebaseCollections.REAL_STATE_CHAT_MESSAGES)
      .doc(data.messageid)
      .set(
        {
          status: data.newStatus,
        },
        { merge: true },
      );
    return {
      error: false,
      data: true,
    };
  } catch (error) {
    return formatResponseError('onBlockMessageHandler', error);
  }
};
