import {formatResponseError} from "../../utils/formatResponseError";
import {https, Response} from "firebase-functions";
import {admin} from "../../config/firebase";
import {FirebaseCollections} from "@dindom/common/src/interfaces/firebase/firebaseCollections.enum";

export const findPostByRealStateNameAPIHandler = async (
  req: https.Request,
  res: Response<unknown>
): Promise<void> => {
  try {
    if (!req.query.realStateName) {
      throw new Error("no name provided");
    }
    const findRealStateFromDB = await admin()
      .firestore()
      .collection(FirebaseCollections.POSTS)
      .where("realStateName", "==", req.query.realStateName)
      .get();
    if (findRealStateFromDB.empty) {
      throw Error("not found");
    }
    res.send({
      error: false,
      data: findRealStateFromDB.docs.map((item) => {
        return item.data();
      }),
    });
  } catch (error) {
    res.send(formatResponseError("findAllRealStateAPIHandler", error));
  }
};
