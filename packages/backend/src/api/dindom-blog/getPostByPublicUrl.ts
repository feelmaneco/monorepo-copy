import { formatResponseError } from '../../utils/formatResponseError';
import { https, Response } from 'firebase-functions';
import { admin } from '../../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';

export const getPostByPublicUrlAPIHandler = async (req: https.Request, res: Response<unknown>): Promise<void> => {
  try {
    if (!req.body.postUrl) {
      throw new Error('no postUrl provided');
    }
    const findPostInDB = await admin().firestore().collection(FirebaseCollections.BLOG).where('publicURL', '==', req.body.postUrl).get();
    if (findPostInDB.empty) {
      throw Error('not found');
    }
    res.send({
      error: false,
      data: findPostInDB.docs[0].data(),
    });
  } catch (error) {
    res.send(formatResponseError('getPostByPublicUrlAPIHandler', error));
  }
};
