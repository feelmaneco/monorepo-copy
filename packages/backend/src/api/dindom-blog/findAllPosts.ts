import { https, Response } from 'firebase-functions';
import { FirebaseCollections } from '@dindom/common/build/interfaces/firebase/firebaseCollections.enum';
import { HttpSimpleErrorResponse, HttpSuccessResponseGeneric } from '@dindom/common/build/interfaces/firebase/HttpResponses.interface';
import { IBlog } from '@dindom/common/build/interfaces/blog/blog.public.interface';
import Cors from 'cors';
import { admin } from '../../config/firebase';
import { formatResponseError } from '../../utils/formatResponseError';

const cors = Cors({ origin: true });
export const findAllPostsHandler = async (req: https.Request, res: Response<unknown>): Promise<void> => {
  cors(req, res, async () => {
    try {
      const findPostInDB = await admin().firestore().collection(FirebaseCollections.BLOG).get();
      if (findPostInDB.empty) {
        throw Error('not found');
      }
      res.send({
        error: false,
        data: findPostInDB.docs.map(item => item.data() as unknown as IBlog),
      });
    } catch (error) {
      res.send(formatResponseError('getPostByPublicUrlAPIHandler', error));
    }
  });
};
