import {formatResponseError} from "../../utils/formatResponseError";
import {https, Response} from "firebase-functions";
import {admin} from "../../config/firebase";
import {FirebaseCollections} from "@dindom/common/src/interfaces/firebase/firebaseCollections.enum";
import {IGetPubliRealStateLanding} from "../../interfaces/getPublicRealState.interface";

export const findAllRealStateAPIHandler = async (
  req: https.Request,
  res: Response<unknown>
): Promise<void> => {
  try {
    const dbResponse = await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .get();
    if (dbResponse.empty) {
      res.send({
        error: false,
        data: [],
      });
    }
    const response = dbResponse.docs.map((item) => {
      const {
        photoURL,
        name,
        publicUrl,
        address: {address},
        administrator,
      } = item.data();

      return {
        logoUrl: photoURL || "",
        realStateName: name || "",
        urlPrefix: publicUrl || "",
        address: address || "",
        adminName: administrator.name || "",
      } as IGetPubliRealStateLanding;
    });
    res.send({
      error: false,
      data: response,
    });
  } catch (error) {
    res.send(formatResponseError("findAllRealStateAPIHandler", error));
  }
};
