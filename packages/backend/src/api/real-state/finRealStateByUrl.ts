import { formatResponseError } from '../../utils/formatResponseError';
import { https, Response } from 'firebase-functions';
import { admin } from '../../config/firebase';
import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import { FindRealStateInfo } from '../../interfaces/responses/onCall/FindRealStateInfo.interface';
import { CITY } from '../../interfaces/City.enum';
import { REAL_STATE_GOVEMENT_STATUS } from '@dindom/common/src/interfaces/realState/realState';

export const findAllRealStateByURLAPIHandler = async (req: https.Request, res: Response<unknown>): Promise<void> => {
  try {
    const repsonse: FindRealStateInfo = {
      address: {
        address: '',
        city: CITY.BOGOTA,
      },
      administrator: {
        name: '',
        photoUrl: '',
      },
      aparmentsNumber: 0,
      email: '',
      name: '',
      phoneNumber: '',
      photoUrl: '',
      status: REAL_STATE_GOVEMENT_STATUS.one,
      uid: '',
    };
    if (!req.query.urlPrefix) {
      res.send({
        error: false,
        data: repsonse,
      });
    }
    const findRealStateFromDB = await admin()
      .firestore()
      .collection(FirebaseCollections.REAL_STATE)
      .where('publicUrl', '==', req.query.urlPrefix)
      .get();
    if (findRealStateFromDB.empty) {
      throw Error('not found');
    }
    res.send({
      error: false,
      data: findRealStateFromDB.docs.map(item => {
        return item.data();
      })[0],
    });
  } catch (error) {
    res.send(formatResponseError('findAllRealStateAPIHandler', error));
  }
};
