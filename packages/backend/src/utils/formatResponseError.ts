import {IHttpResponseError} from "../interfaces/responses/HttpResponseError.interface";

export const formatResponseError = (
  fun: string,
  error: unknown
): IHttpResponseError => {
  if (error instanceof Error) {
    return {
      error: true,
      message: error.message,
    };
  }
  return {
    error: true,
    message: `${fun}: unknown error`,
  };
};
