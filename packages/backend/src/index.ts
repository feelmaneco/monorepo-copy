import { FirebaseCollections } from '@dindom/common/src/interfaces/firebase/firebaseCollections.enum';
import * as functions from 'firebase-functions';
import { findAllPostsHandler } from './api/dindom-blog/findAllPosts';
import { getPostByPublicUrlAPIHandler } from './api/dindom-blog/getPostByPublicUrl';
import { findDocumentsByRealStateNameAPIHandler } from './api/documents/findDocumentsByName';
import { findGaleryByRealStateNameAPIHandler } from './api/galery/findGaleryByName';
import { findPostByRealStateNameAPIHandler } from './api/post/findPostByRealStateName';
import { findAllRealStateAPIHandler } from './api/real-state/findAllRealState';
import { findAllRealStateByURLAPIHandler } from './api/real-state/finRealStateByUrl';
import { createNewPostInBlogHandler } from './onCallHandlers/admin/createNewPostInBlogHandler';
import { createRealStateHandler } from './onCallHandlers/admin/createRealStateHandler';
import { chageUserClaimsHandler } from './onCallHandlers/changeUserClaims';
import { changeProfileImageHandler } from './onCallHandlers/changeUserProfileImage';
import { createUserHandler } from './onCallHandlers/createUser';
import { onCreateMessageHandler } from './onCallHandlers/onCreateMessage';
import { onCreateNewPostHandler } from './onCallHandlers/onCreateNewPost';
import { onCreateNewRegistrationRequestHanlder } from './onCallHandlers/onCreateNewRegistrationRequest';
import { onCreateUserHandler } from './onCallHandlers/onCreateUser';
import { onFindRealStateInfoHandler } from './onCallHandlers/onFindRealStateInfoHandler';
import { onGetPublicRealStateHandler } from './onCallHandlers/onGetPublicInfoRealState';
import { onRealStateGaleryHandler } from './onCallHandlers/onUpdateRealStateGalery';
import { onUpdateRealStateInfoHandler } from './onCallHandlers/onUpdateRealStateInfoHandler';
import { onUploadRealStateDocumentsHandler } from './onCallHandlers/onUploadRealStateDocuments';
import { onVerifyAccessCodeHandler } from './onCallHandlers/onVerifyAccessCodeHandler';
import { onCreateNewUserByRealAdminHandler } from './onCallHandlers/real-admin/createNewUser';
import { changeUsernameHanlder } from './onCallHandlers/user/changeUsername';
import { onSendFileHandler } from './onCallHandlers/onSendFile';
import { onSendPhotoHandler } from './onCallHandlers/onSendPhoto';
import { onBlockMessageHandler } from './onCallHandlers/onBlockMessage';
import { onReceiveCorrespondenceHandler } from './onCallHandlers/onReceiveCorrespondence';
import { onReceiveMailHandler } from './onCallHandlers/onReceiveMail';
import { generateUserChatTokenHandler } from './onCallHandlers/user/generateUserChatTokenHandler';

// import { findRealStateHandler } from "./onCallHandlers/real-admin/";
// import { onGetRealStateListHandler } from "./onCallHandlers/real-state/onGetRealStateListHandler";

export const findRealStateInfo = functions.https.onCall(onFindRealStateInfoHandler);
export const findRealState = functions.https.onCall(() => {
  return null;
});
export const updateRealStateInfo = functions.https.onCall(onUpdateRealStateInfoHandler);
export const onRealStateGalery = functions.https.onCall(onRealStateGaleryHandler);
export const onCreateNewPost = functions.https.onCall(onCreateNewPostHandler);
export const onCreateMessage = functions.https.onCall(onCreateMessageHandler);
export const onGetPublicRealState = functions.https.onCall(onGetPublicRealStateHandler);
export const onUploadRealStateDocuments = functions.https.onCall(onUploadRealStateDocumentsHandler);
export const onVerifyAccessCode = functions.https.onCall(onVerifyAccessCodeHandler);
export const createUser = functions.https.onCall(createUserHandler);
export const changeUserClaims = functions.https.onCall(chageUserClaimsHandler);
export const createRealState = functions.https.onCall(createRealStateHandler);

export const changeProfileImage = functions.https.onCall(changeProfileImageHandler);

export const onGetRealStateList = functions.https.onCall(() => {
  return null;
});
export const onCreateNewUserByRealAdmin = functions.https.onCall(onCreateNewUserByRealAdminHandler);
export const createNewPostInBlog = functions.https.onCall(createNewPostInBlogHandler);
export const onSendFile = functions.https.onCall(onSendFileHandler);
export const onSendPhoto = functions.https.onCall(onSendPhotoHandler);
export const onBlockMessage = functions.https.onCall(onBlockMessageHandler);
export const onReceiveMail = functions.https.onCall(onReceiveMailHandler);
export const onReceiveCorrespondence = functions.https.onCall(onReceiveCorrespondenceHandler);
export const generateUserToken = functions.https.onCall(generateUserChatTokenHandler);

// Triggers
export const onCreateUser = functions.auth.user().onCreate(onCreateUserHandler);
export const onCreateNewRegistrationRequest = functions.firestore
  .document(`${FirebaseCollections.NEW_REAL_STATE_REQUEST}/{docId}`)
  .onCreate(onCreateNewRegistrationRequestHanlder);
// API
export const APIFindRealStateList = functions.https.onRequest(findAllRealStateAPIHandler);
export const APIFindRealStateByUrl = functions.https.onRequest(findAllRealStateByURLAPIHandler);
export const APIFindPostsByRealStateName = functions.https.onRequest(findPostByRealStateNameAPIHandler);

export const APIFindGaleryByRealStateName = functions.https.onRequest(findGaleryByRealStateNameAPIHandler);

export const APIFindDocumentsByRealStateName = functions.https.onRequest(findDocumentsByRealStateNameAPIHandler);

export const APIFindDinDomPostByURL = functions.https.onRequest(getPostByPublicUrlAPIHandler);
export const APIHEllo = functions.https.onRequest((_req, res) => {
  // FirebaseCollections.DOCUMENTS
  res.send(`ok: `);
});
export const APIFindAllBlogPosts = functions.https.onRequest(findAllPostsHandler);
export const changeUsername = functions.https.onCall(changeUsernameHanlder);
