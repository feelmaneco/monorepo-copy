export enum USER_ROLE {
  ADMIN = 'admin',
  REAL_STATE_ADMIN = 'real-state-admin',
  USER = 'user',
  GUARDIAN = 'guardian',
}
