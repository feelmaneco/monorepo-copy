export interface NewRealStateRequest {
  realStateName: string;
  email: string;
  name: string;
  status: string;
}
