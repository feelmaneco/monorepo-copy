import { CITY, REAL_STATE_GOVEMENT_STATUS } from '@dindom/common/src/interfaces/realState/realState';

export interface FindRealStateInfo {
  name: string;
  photoUrl: string;
  phoneNumber: string;
  email: string;
  address: {
    city: CITY;
    address: string;
  };
  status: REAL_STATE_GOVEMENT_STATUS;
  aparmentsNumber: number;
  administrator: {
    name: string;
    photoUrl: string;
  };
  uid: string;
}
