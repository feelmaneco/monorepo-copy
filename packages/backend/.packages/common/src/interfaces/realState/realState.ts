import { IRealStateGalery } from '../galery/RealStateGalery.interface';
export enum REAL_STATE_GOVEMENT_STATUS {
  one = '1',
  two = '2',
  three = '3',
  four = '4',
  five = '5',
  six = '6',
}
export enum CITY {
  BOGOTA = 'Bogota',
}

export type RealStateInfo = {
  name: string;
  photoUrl: string;
  phoneNumber: string;
  email: string;
  address: {
    city: CITY;
    address: string;
  };
  status: REAL_STATE_GOVEMENT_STATUS;
  aparmentsNumber: number;
  administrator: {
    name: string;
    photoUrl: string;
  };
  PSEUrl: string;
};

export interface IRealStateEntity {
  name: string;
  address: {
    city: CITY;
    address: string;
  };
  photoURL: string;
  phoneNumbers: {
    administrator: string;
    control: string;
  };
  email: {
    administrator: string;
  };
  goverment: {
    status: REAL_STATE_GOVEMENT_STATUS;
  };
  administrator: {
    name: string;
    phone: string;
    uid: string;
    photoUrl: string;
  };
  aparmentsNumber: number;
  galery: IRealStateGalery[];
  publicUrl: string;
  accessCode: string;
  PSEUrl: string;
}
export type realStatePublicList = Pick<IRealStateEntity, 'publicUrl' | 'photoURL' | 'name'>;
