export type MessageType = 'text' | 'document' | 'photo';

export type MessageTextContent = {
  text: string;
};
export type MessageFile = {
  url: string;
  name: string;
};
export type MessagePhoto = {
  url: string;
};
export type MessageStatus = 'public' | 'hidden' | 'blocked';

export type MessageContent = MessageTextContent | MessageFile | MessagePhoto;
