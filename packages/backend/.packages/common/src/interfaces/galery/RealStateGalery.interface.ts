export interface IRealStateGalery {
  url: string;
  name: string;
  order: number;
}
