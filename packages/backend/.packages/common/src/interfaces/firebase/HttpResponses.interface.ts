export interface HttpSimpleSuccessResposne {
  error: false;
  data: true;
}
export interface HttpSimpleErrorResponse {
  error: true;
  message: string;
}
interface HttpSuccessResponseList {
  createNewPostInBlog: true;
}
export interface HttpSuccessResponse<T extends keyof HttpSuccessResponseList> {
  error: false;
  data: HttpSuccessResponseList[T];
}

export interface HttpSuccessResponseGeneric<T> {
  error: false;
  data: T;
}
