export type BlogStatus= 'created' | 'pendingVerification' | 'hidden' | 'published';
