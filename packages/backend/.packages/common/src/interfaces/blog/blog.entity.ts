// import { Timestamp } from 'firebase/firestore';
import { FieldValue, Timestamp } from 'firebase/firestore';
import { BlogAuthor } from './blogAuthor.type';
import { BlogStatus } from './blogStatus.type';

export interface IBlogEntity {
  title: string;
  subDescription: string;
  description: string;
  author?: BlogAuthor;
  authorId: string;
  imageThumbnailURL: string;
  createdAt: FieldValue;
  updatedAt: FieldValue;
  status: BlogStatus;
  publicURL?: string;
}
