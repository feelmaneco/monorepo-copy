import { IUserEntity } from '../user/user.entity';

export interface GroupChatEntity {
  realStateId: string;
  administrators: IUserEntity[];
  realStateName: string;
  realStateAdministrator: IUserEntity;
  bannedIds: string[];
}
