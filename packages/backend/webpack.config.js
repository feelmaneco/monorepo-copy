const path = require('path')
const nodeExternals = require('webpack-node-externals')
const IS_WATCHING = process.env.IS_WATCHING && process.env.IS_WATCHING ==="true"? true:false;
module.exports = {
  target: 'node',
  mode: 'production',
  entry: './src/index.ts',
  watch: IS_WATCHING,
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  optimization: {
    minimize: false,
  },
  performance: {
    hints: false,
  },
  devtool: 'nosources-source-map',
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
  },
  externals: [
    /^firebase.+$/,
    /^@google.+$/,
    // nodeExternals({
    //   allowlist: [/^@dindom/],
    // }),
  ],
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, './lib'),
    libraryTarget: 'commonjs',
  },
}